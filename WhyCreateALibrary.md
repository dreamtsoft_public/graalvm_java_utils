# Why create a Library and not use what is there?

There are 2 reasons to create an Abstract library that hides the complexity.
1. This creates an API which will have less changes and makes it easier for Developers to move between different frameworks, or even different versions of the framework.
2. Simplifying the API that developers use.  This will hide a lot of duplicate code.

To give a little more context, the Dreamtsoft platform will execute JavaScript on the server side (as well as client side). On the server side the application can bounce back and forth between JavaScript and Java without the JavaScript developer needing to know they have a Java Object.  Everything in JavaScript is treated as JavaScript Objects. Also, we have the ability to execute other scripts from within a script.

As an example, let's start with these steps:
1. Client calls REST API, which in turn executes Java Code
2. First Java Code creates Context
3. First Java Code retrieves First Script
4. First Java Code executes First Script in Context
5. First Script calls a Second Java Object
6. Second Java Object retrieves a Second Script
7. Second Java Object executes Second Script in the same Context as they are part of the same Thread.
8. Second Java Object retrieves Value from Second Script
9. Second Java Object processes information in Value
10. First Script processes information passed back from Second Java Object
11. First Java Object retrieves Value from First Script
12. First Java Object returns information on REST API response


**NOTE: The code in this example is a very basic and completely made up.**

## Step 1
A Client calls a REST API (JavaScript executed in a browser making a REST call).  The client also passes a JSON string that represents a map of information about what to process (which table, filter criteria, etc).  The First Java Code will transform that JSON string to a java.util.Map in order to figure out what needs to be executed from this point.

## Step 2
This is fairly straight forward because it is just the Context being created.  However, there are 2 possibilities here:
A. All Context are created equal.  Meaning they use the same configurations no matter what.
B. All Context are created based on information passed in. This allows for Dynamic creation of Contexts, which may be useful during development.
This is what the GraalVMContextFactory does.  It allows you to use configurations to create the Context in its entirety.  This can simplify how the Context can be created.  For the most part, I think Developers will use a default configuration something along the lines of:
```
Context context = Context.newBuilder().allowHostAccess(HostAccess.EXPLICIT).build();

-- OR --

HostAccess hostAccess = HostAccess.newBuilder()
                .allowAccessAnnotatedBy(SomeProprietaryAnnotation.class)
                .allowListAccess(true)
                .allowMapAccess(true)
                .allowArrayAccess(true)
                .allowIterableAccess(true)
                .allowIteratorAccess(true)
                .build();
Context context = Context.newBuilder().allowHostAccess(hostAccess).build();
```
A nice to have enhancement to Context.Builder would be to add another `newBuilder` method that takes a Map and configures the Context based on the Map. `Context.newBuilder(Map<String, Object)).build()` This would allow applications to have a Configuration file instead of hard coding the creation of the Context. But for now, GraalVMContextFactory does the job.

For this example we will use the GraalVMContextFactory and the GraalVMScriptEnvrionmentFactory.

```java
public class FirstObject {

    private final Map<String, Object> contextConfig;
    private final GraalVMScriptEnvironmentFactory scriptEnvironmentFactory;

    public FirstObject(final Map<String, Object> contextConfig) {
        this.contextConfig = contextConfig;
        scriptEnvironmentFactory = new GraalVMScriptEnvironmentFactory();
    }

    public Response callApi(final Map<String, Object> parameters) {
        final GraalVMScriptEnvironment scriptEnvironment = scriptEnvironmentFactory.createScriptEnvironment(GraalVMScriptConstants.SCRIPT_TYPE_JS, contextConfig);
    }

}
```

## Step 3
There is nothing special here.  This is getting the String to `eval`, or create a Source. This will be different for each application.

```java
public class FirstObject {

    private final Map<String, Object> contextConfig;
    private final GraalVMScriptEnvironmentFactory scriptEnvironmentFactory;

    public FirstObject(final Map<String, Object> contextConfig) {

        this.contextConfig = contextConfig;
        scriptEnvironmentFactory = new GraalVMScriptEnvironmentFactory();
    }

    public Response callApi(final Map<String, Object> parameters) {

        final GraalVMScriptEnvironment scriptEnvironment = scriptEnvironmentFactory.createScriptEnvironment(GraalVMScriptConstants.SCRIPT_TYPE_JS, contextConfig);
        final String script = getScript(parameters);
    }

    private String getScript(final Map<String, Object> parameters) {

        // Somehow a String that is the JavaScript is created here.
    }

}
```

## Step 4
Step 4 can have a couple of different scenarios.
A. The Value passed back from `eval` is executable; so Value.execute(Object...) can be called.
B. There is a function in the Value passed back from `eval` is what is to be executed.
In both of these scenarios, the Java code will need to create boilerplate code to execute either the Value or a function in the Value. That is one of the functions of GraalVMWrappedObject.

```java
public class FirstObject {

    private final Map<String, Object> contextConfig;
    private final GraalVMScriptEnvironmentFactory scriptEnvironmentFactory;

    public FirstObject(final Map<String, Object> contextConfig) {
        this.contextConfig = contextConfig;
        scriptEnvironmentFactory = new GraalVMScriptEnvironmentFactory();
    }

    public Response callApi(final Map<String, Object> parameters) {
        final GraalVMScriptEnvironment scriptEnvironment = scriptEnvironmentFactory.createScriptEnvironment(GraalVMScriptConstants.SCRIPT_TYPE_JS, contextConfig);
        final String script = getScript(parameters);
        final GraalVMWrappedObject wrappedScript = scriptEnvironment.scriptEvaluate(script);
        final Object result = wrappedScript.call(parameters);
    }

    private String getScript(final Map<String, Object> parameters) {

        // Somehow a String that is the JavaScript is created here.
    }

}
```

## Step 5
In the script, a Java Object is instantiated by creating a Prototype and then doing `new` on the Prototype.
In the Dreamtsoft platform, but not in this library, we have the ability to create an object and the developer doesn't care if it is Java or JavaScript.
All Objects are treated like JavaScript objects. This will most likely be different for each application. 
Although, we could add in some functionality to the library that tries to figure out if the developer wanted a Java Object, or a JavaScript Object.

For the sake of this example, let's assume the Prototype is created and `new` is called.

When creating the Prototype of the second Java Object, wrap it inside a GraalVMProxyObject. This needs to be added to the library.  The reason why it should be wrapped will be explained in Step 10.

Example:
```javascript
(function(parameters) {
    // Somehow the `SomeObject` Prototype is created here.
    var someObject = new SomeObject();
    someObject.search(parameters);
})
```

In the above example, `SomeObject` is a Java Object that has a method `search` that takes a java.util.Map as a parameter.
The developer doesn't necessarily know that the `parameters` must be a java.util.Map, or that `SomeObject` is really a Java Object.
The Dreamtsoft platform handles it for the developer.  The developer thinks they are just using JavaScript.

## Step 6
Similar to Step 3, Step 6 will retrieve a second script. This script may come from a file, it may come from the java.util.Map that was passed into the `search` method.

```java
public class SomeObject {

    public void search(final Map<String, Object> parameters) {
        final GraalVMScriptEnvironment scriptEnvironment = scriptEnvironmentFactory.createScriptEnvironment(GraalVMScriptConstants.SCRIPT_TYPE_JS, contextConfig);
        final String script = parameters.get("SubScript");
    }
    
}
```
## Step 7
Similar to Step 4, Step 7 will execute the script.  This script is passed in via the `parameters` under the key `calculateFilters`.

```java
public class SomeObject {

    public void search(final Map<String, Object> parameters) {

        final GraalVMScriptEnvironment scriptEnvironment = scriptEnvironmentFactory.createScriptEnvironment(GraalVMScriptConstants.SCRIPT_TYPE_JS, contextConfig);
        final String script = parameters.get("calculateFilters");
        
        if (StringUtils.isNotBlank(script)) {
            final GraalVMWrappedObject wrappedScript = scriptEnvironment.scriptEvaluate(script);
            final Object result = wrappedScript.call(parameters);
        }
        
    }
    
}
```


## Step 8
Assuming there is a script passed in from the `parameters`.  The Second Object will retrieve the resulting Value from the execution of the script.

In the script it will create a String

```javascript
(function(parameters) {
    var filters = parameters.filters;
    
    if (typeof filters === 'undefined') {
        return "";
    }

    // In order to save time/space, we are expecting filters to be an Map.
    // Imagine this could be a much more complex script sifting through the parameters and creating a JavaScript Object that is a map for the filters. 
    
    return filters;
})
```

```java
public class SomeObject {
    
    @HostAccess.Export
    public void search(final Map<String, Object> parameters) {

        final GraalVMScriptEnvironment scriptEnvironment = scriptEnvironmentFactory.createScriptEnvironment(GraalVMScriptConstants.SCRIPT_TYPE_JS, contextConfig);
        final String script = parameters.get("calculateFilters");
        String filters;

        if (StringUtils.isNotBlank(script)) {
            final GraalVMWrappedObject wrappedScript = scriptEnvironment.scriptEvaluate(script);
            final GraalVMWrappedObject result = wrappedScript.call(parameters);
        }
        
    }

}
```

## Step 9
In this step, the search is executed with the filters from Step 8.  As well as, either the results are stored for later retrieval; or an error message is stored.

```java
import java.util.ArrayList;
import java.util.HashMap;

public class SomeObject {

    private final Map<String, Object> propertyMap = new HashMap<>();
    
    @HostAccess.Export
    public void search(final Map<String, Object> parameters) {

        final GraalVMScriptEnvironment scriptEnvironment = scriptEnvironmentFactory.createScriptEnvironment(GraalVMScriptConstants.SCRIPT_TYPE_JS, contextConfig);
        final String script = parameters.get("calculateFilters");
        Map<String, String> filters = new HashMap<>();

        if (StringUtils.isNotBlank(script)) {
            final GraalVMWrappedObject wrappedScript = scriptEnvironment.scriptEvaluate(script);
            final GraalVMWrappedObject result = wrappedScript.call(parameters);
            final Object result = result.toObject();

            if (result instanceof final Map filterMap) {
                filters = filterMap;
            } else {
                // Throw an Exception, or Log an Error here
            }

        }

        search(filters);
    }
    
    void search(final Map<String, String> parameters) {
        booelan error = false;
        String errorMessage;
        
        // Do the actual search with the filters.
        // Set error and errorMessage if need be.
        
        if (error) {
            propertyMap.put("error", true);
            propertyMap.put("error_message", errorMessage);
        } else {
           resultList = new ArrayList<>();
           
           // Add results here
           
            rowNum = 0;
        }
        
    }

}
```

## Step 10

Let's expand on the First Script.

```javascript
(function(parameters) {
    // Somehow the `SomeObject` Prototype is created here.
    var someObject = new SomeObject();
    someObject.search(parameters);
    
    if (someObject.error == true) {
        return new BadStatus(someObject.error_message);
    } 
    
    var result = [];
    
    while (someObject.next()) {
        var rowMap = {
            user_id: someObject.user_id,
            username: someObject.username
        }
        
        result.push(rowMap);
    }
    
    return result;
})
```

Now let's expand the Second Java Object.

```java
import java.util.ArrayList;
import java.util.HashMap;

import org.graalvm.GraalVMScriptEnvironment;
import org.graalvm.polyglot.HostAccess;

public class SomeObject implements GraalVMScriptable {

    private final Map<String, Object> propertyMap = new HashMap<>();

    private GraalVMScriptEnvironment scriptEnvironment;

    private List<Map<String, Object>> resultList;

    private int rowNum = -1;

    public Object getMember(final String key) {

        if (propertyMap.contains(key)) {
            return propertyMap.get(key);
        }

        if (rowNum > -1 && rowNum < resultList.size()) {
            return scriptEnvironment.createUndefined();
        }

        final Map<Sting, Object> rowMap = resultList.get(rowNum);
        
        return rowMap.get(key);
    }

    public List<String> getMemberKeys() {

        final List<String> memKeys = new ArrayList<>(propertyMap.keySet());
        
        if (rowNum > -1 && rowNum < resultList.size()) {
            final Map<Sting, Object> rowMap = resultList.get(rowNum);
            memKeys.addAll(rowMap.keySet());
        }
        
        return memKeys;
    }

    public void putMember(final String key, final Object value) {

        propertyMap.put(key, value);
    }

    public boolean removeMember(final String key) {

        final Object value = propertyMap.get(key);
        return propertyMap.remove(key, value);
    }

    @HostAccess.Export
    public void search(final Map<String, Object> parameters) {

        scriptEnvironment = scriptEnvironmentFactory.createScriptEnvironment(GraalVMScriptConstants.SCRIPT_TYPE_JS, contextConfig);
        final String script = parameters.get("calculateFilters");
        Map<String, String> filters = new HashMap<>();

        if (StringUtils.isNotBlank(script)) {
            final GraalVMWrappedObject wrappedScript = scriptEnvironment.scriptEvaluate(script);
            final GraalVMWrappedObject result = wrappedScript.call(parameters);
            final Object result = result.toObject();

            if (result instanceof final Map filterMap) {
                filters = filterMap;
            }

        }

        search(filters);
    }

    @HostAccess.Export
    public List<Map<String, Object>> getResult() {

        return resultList;
    }

    @HostAccess.Export
    public boolean next() {
    
        rowNum++;
        
        return (rowNum < resultList.size());
    }
    
    void search(final Map<String, String> parameters) {

        booelan error = false;
        String errorMessage;

        // Do the actual search with the filters.
        // Set error and errorMessage if need be.

        if (error) {
            propertyMap.put("error", true);
            propertyMap.put("error_message", errorMessage);
        } else {
            resultList = new ArrayList<>();

            // Add results here

            rowNum = 0;
        }

    }

    GraalVMScriptEnvironment getScriptEnvironment() {

        if (scriptEnvironment == null) {
            scriptEnvironment = scriptEnvironmentFactory.createScriptEnvironment(GraalVMScriptConstants.SCRIPT_TYPE_JS, contextConfig);
        }

        return scriptEnvironment;
    }

}
```

Notice, the Java Object has dynamic properties added to it like (`error` and `error_message`).
In JavaScript, this is allowed. In Java, you can do this by creating a variable `public boolean error = false;`.
However, this is generally a code smell as it breaks encapsulation.  Not too mention it hard codes the name and you loose some of the dynamic aspects to it.
So in order to allow Java Objects to act like JavaScript Objects, GraalVMProxyObject wraps any Java Object that implements GraalVMScriptable. (Fully aware this may be replaced with ProxyObject later)
The GraalVMProxyObject, when created, will look for any method on the target Object that has @HostAccess.Export attached to it.
Currently, this is hard coded, but it would be an easy change to make it configurable.
What this does is create boilerplate code that looks for the methods allowed to be visible and also allows for dynamic properties to be placed on the object.
Also, this allows the Second Java Object to be able to reach down into the Result List and fetch out information from that list without having to write a lot of boilerplate code.
This allows the Java Objects to be treated like JavaScript Objects.

This could be even more complex with the `dot` notation expanding down into further Objects (ex - someObject.group.roles.permissions).  This becomes extremely complicated in GraalVM without something like this library to wrap all of the boilerplate code.

## Step 11

Nothing special in this Step, we are back out of the Context and have a Java Object that represents the result.  Initially, this will be the GraalVMWrappedObject.

```java
public class FirstObject {

    private final Map<String, Object> contextConfig;
    private final GraalVMScriptEnvironmentFactory scriptEnvironmentFactory;

    public FirstObject(final Map<String, Object> contextConfig) {
        this.contextConfig = contextConfig;
        scriptEnvironmentFactory = new GraalVMScriptEnvironmentFactory();
    }

    public Response callApi(final Map<String, Object> parameters) {
        final GraalVMScriptEnvironment scriptEnvironment = scriptEnvironmentFactory.createScriptEnvironment(GraalVMScriptConstants.SCRIPT_TYPE_JS, contextConfig);
        final String script = getScript(parameters);
        final GraalVMWrappedObject wrappedScript = scriptEnvironment.scriptEvaluate(script);
        final GraalVMWrappedObject wrappedResult = wrappedScript.call(parameters);
    }

    private String getScript(final Map<String, Object> parameters) {

        // Somehow a String that is the JavaScript is created here.
    }

}
```

## Step 12

Here in Step 12, we need to take the Result (whatever Java Object that might be) and marshall it into a JSON String. We can now pass that information back to the client.
Remember from Step 11, the Result is a GraalVMWrappedObject which contains the Value.  The requirement is to get whatever Java Object is represented in that Value.

```java
public class FirstObject {

    private final Map<String, Object> contextConfig;
    private final GraalVMScriptEnvironmentFactory scriptEnvironmentFactory;

    public FirstObject(final Map<String, Object> contextConfig) {
        this.contextConfig = contextConfig;
        scriptEnvironmentFactory = new GraalVMScriptEnvironmentFactory();
    }

    public Response callApi(final Map<String, Object> parameters) {
        final GraalVMScriptEnvironment scriptEnvironment = scriptEnvironmentFactory.createScriptEnvironment(GraalVMScriptConstants.SCRIPT_TYPE_JS, contextConfig);
        final String script = getScript(parameters);
        final GraalVMWrappedObject wrappedScript = scriptEnvironment.scriptEvaluate(script);
        final GraalVMWrappedObject wrappedResult = wrappedScript.call(parameters);
        final String jsonString = JSONUtil.marshall(wrappedResult.toObject());
        
        return new Response(jsonString);
    }

    private String getScript(final Map<String, Object> parameters) {

        // Somehow a String that is the JavaScript is created here.
    }

}
```

# Conclusion

Dreamtsoft's platform is a lot more complex than this example.  However, this example starts to show how bouncing back and forth between Java and JavaScript with generic code can be simple with a library like this. 
One of the areas of complexity that is simplified is in _org.graalvm.utilities.GraalVMGuestLanguageToHostUtil_.  When using generic code, there is a lot of possibilities of what is returned from the Guest Language.
So, having a utility that wraps all of this complexity makes life a little easier.

Another area is having the ability to mimic `dot` notation in JavaScript without having to hard code everything. A lot of the examples (understandably so) about ProxyObject show methods and properties being hard coded.  This library can make that process more dynamic and reduce the risk of missing a method/property not being added.  

A benefit of this library is also it is extensible.  If the library doesn't quite have the functionality needed, it can be extended and modified to what the developer needs.

Agreeably, there is some overlap with GraalVM (ex - GraalVMScriptable), that does need to be flushed out.  And there is a lot of work to do in this library, but the foundation is there.  A lot of fine-tuning and seeing how other developers are using GraalVM will help make this library viable.

# Suggestions for GraalVM

Having a utility like _org.graalvm.utilities.GraalVMGuestLanguageToHostUtil_ would be beneficial.  Most developers are going to want to get whatever Java Object is in the Polyglot Value.  Having a utility that figures out what type of Object it is and returns it would be a nice addition to GraalVM.  This utility should be used for any of the supported languages.

A second nice to have would be having a DefaultProxyObject that takes in a Java Object and figures out which methods to use based on the _@HostAccess.Export_ annotation (or it can be configured to use any annotation).  As well as giving the ability to have a Map that holds onto any properties (even methods) dynamically.

A third nice to have would be a funciton (similar to `require` in node.js) that allows the developer to import a Prototype.  There is the Java.type() function, but that is limited to Java Objects. The developer needs to do something different for JavaScript Objects (or whatever Guest Language is being used).  What would be nice is a more generic and smarter function.  Dreamtsoft has the ability to allow a developer to `require` something; and it will figure out if it is a JavaScript object or a Java Object.  This type of behavior would be great across all scripting languages supported by GraalVM.  