/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.examples;

import static org.assertj.core.api.Assertions.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.graalvm.GraalVMConfigConstants;
import org.graalvm.GraalVMContextFactory;
import org.graalvm.GraalVMScriptEnvironment;
import org.graalvm.GraalVMScriptEnvironmentFactory;
import org.graalvm.GraalVMWrappedObject;
import org.graalvm.polyglot.Context;
import org.graalvm.utilities.GraalVMHostToGuestLanguageUtil;
import org.graalvm.utilities.GraalVMScriptConstants;
import org.graalvm.utilities.GraalVMUtil;
import org.junit.jupiter.api.Test;

public class ExecuteJavaScriptIT {

    private final GraalVMUtil util = new GraalVMUtil();

    private final GraalVMHostToGuestLanguageUtil hostToGuestUtil = new GraalVMHostToGuestLanguageUtil();

    @Test
    public void testScriptEvaluate() {

        final Map<String, Object> contextConfigMap = new HashMap<>();
        contextConfigMap.put(GraalVMConfigConstants.ALLOW_ALL_ACCESS, true);

        final GraalVMContextFactory contextFactory = new GraalVMContextFactory(contextConfigMap);
        final Context context = contextFactory.buildContext(GraalVMScriptConstants.SCRIPT_TYPE_JS);

        final GraalVMScriptEnvironmentFactory scriptEnvironmentFactory = new GraalVMScriptEnvironmentFactory();
        final GraalVMScriptEnvironment scriptEnvironment = scriptEnvironmentFactory.createScriptEnvironment(context, GraalVMScriptConstants.SCRIPT_TYPE_JS);

        final Map<String, Object> argumentMap = new HashMap<>();
        argumentMap.put("id", "ABCD1234");
        argumentMap.put("name", "Some Cool Name");


        final String wrappedScript = util.moduleWrap(SCRIPT, argumentMap);
        final List<Object> arguments = util.createArgumentsWithModuleAndExports(scriptEnvironment);

        for (final String arg : argumentMap.keySet()) {
            final Object obj = argumentMap.get(arg);
            final Object guestObj = hostToGuestUtil.createGuestLanguageObject(scriptEnvironment, obj);
            arguments.add(guestObj);
        }

        final GraalVMWrappedObject wrappedObject = scriptEnvironment.scriptEvaluate(wrappedScript, arguments.toArray());

        final Object result = wrappedObject.callFunction("getDisplay");

        assertThat(result).as("Result").isEqualTo("ABCD1234 -- Some Cool Name");
    }

    @Test
    public void testScriptEvaluate2() {

        final Map<String, Object> contextConfigMap = new HashMap<>();
        contextConfigMap.put(GraalVMConfigConstants.ALLOW_ALL_ACCESS, true);

        final GraalVMContextFactory contextFactory = new GraalVMContextFactory();
        final Context context = contextFactory.buildContext(GraalVMScriptConstants.SCRIPT_TYPE_JS, contextConfigMap);

        final GraalVMScriptEnvironmentFactory scriptEnvironmentFactory = new GraalVMScriptEnvironmentFactory();
        final GraalVMScriptEnvironment scriptEnvironment = scriptEnvironmentFactory.createScriptEnvironment(context, GraalVMScriptConstants.SCRIPT_TYPE_JS);

        final Map<String, Object> argumentMap = new HashMap<>();
        argumentMap.put("id", "ABCD1234");
        argumentMap.put("name", "Some Cool Name");


        final String wrappedScript = util.moduleWrap(SCRIPT, argumentMap);
        final List<Object> arguments = util.createArgumentsWithModuleAndExports(scriptEnvironment);

        for (final String arg : argumentMap.keySet()) {
            final Object obj = argumentMap.get(arg);
            final Object guestObj = hostToGuestUtil.createGuestLanguageObject(scriptEnvironment, obj);
            arguments.add(guestObj);
        }

        final GraalVMWrappedObject wrappedObject = scriptEnvironment.scriptEvaluate(wrappedScript, arguments.toArray());

        final Object result = wrappedObject.callFunction("getDisplay");

        assertThat(result).as("Result").isEqualTo("ABCD1234 -- Some Cool Name");
    }


    private static final String SCRIPT = """
            var jsObj = {
                objId: id,
                objName: name,
                getDisplay: function() {
                    return id + ' -- ' + name;
                }
            };
              
            return jsObj;
            """;

}