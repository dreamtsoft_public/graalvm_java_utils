/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.examples;

import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.graalvm.GraalVMWrappedObject;
import org.graalvm.javascript.GraalVMJavaScriptEnvironment;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyIterable;
import org.junit.jupiter.api.Test;

public class IterableAsArgumentsIT {

    private static final String SCRIPT = """
            (function (name, options) {
            	console.log("Name: " + name);
            	console.log("Options Type: " + (typeof options));
            	console.log("Options: " + JSON.stringify(options));
            	
                for (var index = 0; index < options.length; index++) {
                    console.log("Options[" + index + "] = " + options[index]);
                }
              
                return 'Finished Successfully';
            })
            """;

    @Test
    public void testArrayArguments() {

        final Context context = Context.newBuilder().allowAllAccess(true).build();
        final GraalVMJavaScriptEnvironment scriptEnvironment = new GraalVMJavaScriptEnvironment(context);

        final GraalVMWrappedObject wrappedObjectResult = scriptEnvironment.scriptEvaluate(SCRIPT, createArgArray());
        final Object hostObjectResult = wrappedObjectResult.toObject();

        assertThat(hostObjectResult).as("Host Object Result").isInstanceOf(String.class);
        assertThat(hostObjectResult).as("Host Object Result value").isEqualTo("Finished Successfully");
    }

    @Test
    public void testValueArrayArguments() {

        final Context context = Context.newBuilder().allowAllAccess(true).build();
        final GraalVMJavaScriptEnvironment scriptEnvironment = new GraalVMJavaScriptEnvironment(context);

        final GraalVMWrappedObject wrappedObjectResult = scriptEnvironment.scriptEvaluate(SCRIPT, createArgValueArray(context));
        final Object hostObjectResult = wrappedObjectResult.toObject();

        assertThat(hostObjectResult).as("Host Object Result").isInstanceOf(String.class);
        assertThat(hostObjectResult).as("Host Object Result value").isEqualTo("Finished Successfully");
    }

    @Test
    public void testValueArrayAndProxyArrayArguments() {

        final Context context = Context.newBuilder().allowAllAccess(true).build();
        final GraalVMJavaScriptEnvironment scriptEnvironment = new GraalVMJavaScriptEnvironment(context);

        final GraalVMWrappedObject wrappedObjectResult = scriptEnvironment.scriptEvaluate(SCRIPT, createArgValueAndProxyArray(context));
        final Object hostObjectResult = wrappedObjectResult.toObject();

        assertThat(hostObjectResult).as("Host Object Result").isInstanceOf(String.class);
        assertThat(hostObjectResult).as("Host Object Result value").isEqualTo("Finished Successfully");
    }

    @Test
    public void testValueArgAndProxyArrayArguments() {

        final Context context = Context.newBuilder().allowAllAccess(true).build();
        final GraalVMJavaScriptEnvironment scriptEnvironment = new GraalVMJavaScriptEnvironment(context);

        final GraalVMWrappedObject wrappedObjectResult = scriptEnvironment.scriptEvaluate(SCRIPT, createArgValueWithProxyArray(context));
        final Object hostObjectResult = wrappedObjectResult.toObject();

        assertThat(hostObjectResult).as("Host Object Result").isInstanceOf(String.class);
        assertThat(hostObjectResult).as("Host Object Result value").isEqualTo("Finished Successfully");
    }


    private Object[] createArgArray() {

        final List<Object> options = new ArrayList<>();
        options.add("hasFooBar");
        options.add(Boolean.TRUE);

        Object[] args = new Object[2];
        args[0] = "FooBar Script";
        args[1] = options;

        return args;
    }

    private Object[] createArgValueArray(final Context context) {

        final Value name = context.asValue("FooBar Script");

        final List<Object> options = new ArrayList<>();
        options.add("hasFooBar");
        options.add(Boolean.TRUE);

        final Value valueIterable = context.asValue(options);

        Object[] args = new Object[2];
        args[0] = name;
        args[1] = valueIterable;

        return args;
    }

    private Object[] createArgValueAndProxyArray(final Context context) {

        final Value name = context.asValue("FooBar Script");

        final List<Object> options = new ArrayList<>();
        options.add("hasFooBar");
        options.add(Boolean.TRUE);

        final ProxyIterable proxyIterable = ProxyIterable.from(options);

        Object[] args = new Object[2];
        args[0] = name;
        args[1] = proxyIterable;

        return args;
    }

    private Object[] createArgValueWithProxyArray(final Context context) {

        final Value name = context.asValue("FooBar Script");

        final List<Object> options = new ArrayList<>();
        options.add("hasFooBar");
        options.add(Boolean.TRUE);

        final Value valueIterable = context.asValue(ProxyIterable.from(options));

        Object[] args = new Object[2];
        args[0] = name;
        args[1] = valueIterable;

        return args;
    }

}