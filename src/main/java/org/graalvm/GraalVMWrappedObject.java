/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyObject;
import org.graalvm.utilities.GraalVMDebugLogUtil;
import org.graalvm.utilities.GraalVMExecutableUtil;
import org.graalvm.utilities.GraalVMGuestLanguageToHostUtil;
import org.graalvm.utilities.GraalVMScriptConstants;
import org.graalvm.utilities.GraalVMUtil;
import org.graalvm.utilities.GraalVMValueUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GraalVMWrappedObject implements ProxyObject {

    private final Logger logger = LoggerFactory.getLogger(GraalVMWrappedObject.class);
    private final GraalVMUtil graalVmUtil = new GraalVMUtil();
    private final GraalVMValueUtil valueUtil = new GraalVMValueUtil();

    private final GraalVMExecutableUtil executableUtil = new GraalVMExecutableUtil();
    private final GraalVMGuestLanguageToHostUtil guestToHostUtil = new GraalVMGuestLanguageToHostUtil();
    private final GraalVMDebugLogUtil debugLogUtil = new GraalVMDebugLogUtil();

    private final GraalVMScriptEnvironment scriptEnvironment;
    private final Context context;
    private final Value value;

    public GraalVMWrappedObject(final GraalVMScriptEnvironment scriptEnvironment, final Value value) {

        if (scriptEnvironment == null) {
            throw new GraalVMRuntimeException("The Script Environment can not be null when instantiating a GraalVMWrappedObject");
        }

        if (value == null) {
            throw new GraalVMRuntimeException("The Polyglot Value can not be null when instantiating a GraalVMWrappedObject");
        }

        this.scriptEnvironment = scriptEnvironment;
        this.context = this.scriptEnvironment.getContext();
        this.value = value;
    }

    // *******************
    // ProxyObject methods
    //

    @Override
    public Object getMember(final String key) {

        Value member;

        if (value.hasMembers() && value.hasMember(key)) {
            member = value.getMember(key);
        } else {
            member = valueUtil.createUndefined(context);
        }

        return member;
    }

    @Override
    public Object getMemberKeys() {

        Set<String> keys;

        if (value.hasMembers()) {
            keys = value.getMemberKeys();
        } else {
            //noinspection unchecked
            keys = Collections.EMPTY_SET;
        }

        return keys.toArray();
    }

    @Override
    public boolean hasMember(final String key) {

        boolean hasMem = false;

        if (value.hasMembers()) {
            hasMem = value.hasMember(key);
        }

        return hasMem;
    }

    @Override
    public void putMember(final String key, final Value value) {

        if (this.value.hasMembers()) {
            this.value.putMember(key, value);
        }

    }

    //
    // *******************

    public GraalVMWrappedObject call(final Object... args) {

        final Value result = graalVmUtil.callFunction(scriptEnvironment, value, args);

        // We always get a Value back unless the 'obj' was an instance of 'Function'.
        return new GraalVMWrappedObject(scriptEnvironment, result);
    }

    public Object newObject(final Object... args) {

        Value value = executableUtil.instantiateValue(scriptEnvironment, this.value, args);

        if (value == null) {
            value = this.value;
        }

        return new GraalVMWrappedObject(scriptEnvironment, value);
    }

    public Object callFunction(final String funcName, final Object... args) {

        Object returnObj;

        try {
            final Object obj = graalVmUtil.callFunction(scriptEnvironment, value, funcName, args);

            Value value = null;

            if (obj instanceof final Value valueObj) {
                value = valueObj;
            } else if (obj instanceof final GraalVMWrappedObject wrappedObject) {
                value = wrappedObject.getValue();
            }

            if (valueUtil.isValueNullOrUndefined(value)) {
                returnObj = obj;
            } else {
                returnObj = guestToHostUtil.getHostValue(scriptEnvironment, value);
            }

        } catch (final Exception e) {
            logger.error("***** Start GraalVMWrappedObject.callFunction() *****");
            logger.error("Could not execute function: {}", funcName);
            logger.error("With Args: {}", Arrays.toString(args));
            logger.error("On 'Value' {}", value);
            debugLogUtil.displayValue(logger, value);
            logger.error("Bindings: {}", context.getBindings(GraalVMScriptConstants.SCRIPT_TYPE_JS).getMemberKeys());
            logger.error(e.getMessage(), e);
            logger.error("***** End GraalVMWrappedObject.callFunction() *****");
            throw new GraalVMRuntimeException(e.getMessage(), e);
        }

        return returnObj;
    }

    public boolean hasObject(final String name) {

        final Value value = graalVmUtil.getMember(this.value, name);

        return !valueUtil.isValueNullOrUndefined(value);
    }

    public Object getObject(final String name) {

        final Value value = graalVmUtil.getMember(this.value, name);

        return guestToHostUtil.getHostValue(scriptEnvironment, value);
    }

    public void setObject(final String name, final Object value) {

        graalVmUtil.setMember(this.value, name, value);
    }

    public Object unwrap() {

        return toObject();
    }

    public Object toObject() {

        return guestToHostUtil.getHostValue(scriptEnvironment, value);
    }

    public String unwrapToString() {

        return toString();
    }

    public boolean hasFunction(final String functionName) {

        return graalVmUtil.hasFunction(value, functionName);
    }

    public boolean isArray() {

        return valueUtil.isArray(value);
    }

    public boolean isFunction() {

        return valueUtil.isFunction(value);
    }

    @Override
    public String toString() {

        return toObject().toString();
    }

    @Override
    public int hashCode() {

        return value.hashCode();
    }

    @Override
    public boolean equals(final Object o) {

        if (this == o) {
            return true;
        }

        if (!(o instanceof final GraalVMWrappedObject that)) {
            return false;
        }

        return new EqualsBuilder().append(value, that.value).isEquals();
    }

    public Value getValue() {

        return value;
    }


}