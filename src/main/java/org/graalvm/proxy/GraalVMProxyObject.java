/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.proxy;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.graalvm.GraalVMRuntimeException;
import org.graalvm.GraalVMScriptEnvironment;
import org.graalvm.polyglot.HostAccess;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.Proxy;
import org.graalvm.polyglot.proxy.ProxyArray;
import org.graalvm.polyglot.proxy.ProxyObject;
import org.graalvm.utilities.GraalVMHostToGuestLanguageUtil;

public class GraalVMProxyObject implements ProxyObject {

    private final GraalVMHostToGuestLanguageUtil hostToGuestUtil = new GraalVMHostToGuestLanguageUtil();

    private final Map<String, Method> methodMap = new HashMap<>();

    private final GraalVMScriptEnvironment scriptEnvironment;
    private final ProxyObject target;

    public GraalVMProxyObject(final GraalVMScriptEnvironment scriptEnvironment, final ProxyObject target) {

        this.scriptEnvironment = scriptEnvironment;
        this.target = target;

        populateMethodMap();
    }

    @Override
    public Object getMember(final String key) {

        if (StringUtils.isBlank(key)) {
            throw new GraalVMRuntimeException("Can not find a member with a blank name");
        }

        Object member = methodMap.get(key);

        if (member == null) {
            member = target.getMember(key);
        }

        return switch (member) {
            case final ProxyObject proxyObject -> new GraalVMProxyObject(scriptEnvironment, proxyObject);
            case final Proxy proxy -> proxy;
            case final Value value -> value;
            case final Method method -> hostToGuestUtil.getExecutable(scriptEnvironment, target, method);
            default -> hostToGuestUtil.createGuestLanguageObject(scriptEnvironment, member);
        };

    }

    @Override
    public Object getMemberKeys() {

        final List<String> memberKeys = new ArrayList<>();
        memberKeys.addAll(methodMap.keySet());
        memberKeys.addAll(getTargetMemberKeys());

        return memberKeys;
    }

    @Override
    public boolean hasMember(final String key) {

        if (StringUtils.isBlank(key)) {
            return false;
        }

        if (methodMap.containsKey(key)) {
            return true;
        }

        final List<String> memKeys = getTargetMemberKeys();

        if (memKeys == null) {
            return false;
        }

        return memKeys.contains(key);

    }

    @Override
    public void putMember(final String key, final Value value) {

        target.putMember(key, value);
    }

    @Override
    public boolean removeMember(final String key) {

        return target.removeMember(key);
    }

    void populateMethodMap() {

        final Method[] methods = target.getClass().getMethods();

        for (Method method : methods) {
            final String methodName = method.getName();

            if (method.isAnnotationPresent(HostAccess.Export.class)) {
                methodMap.put(methodName, method);
            }

        }

    }

    @SuppressWarnings("unchecked")
    List<String> getTargetMemberKeys() {

        final Object memKeys = target.getMemberKeys();

        List<String> returnList;

        // https://www.graalvm.org/sdk/javadoc/org/graalvm/polyglot/proxy/ProxyObject.html#getMemberKeys--
        if (memKeys == null) {
            returnList = new ArrayList<>();
        } else if (memKeys instanceof final ProxyArray proxyArray) {
            returnList = new ArrayList<>();

            for (int index = 0; index < proxyArray.getSize(); index++) {
                returnList.add((String) proxyArray.get(index));
            }

        } else if (memKeys instanceof final List memKeyList) {
            returnList = memKeyList;
        } else if (memKeys instanceof final Set memKeySet) {
            returnList = memKeySet.stream().toList();
        } else if (memKeys.getClass().isArray() && memKeys.getClass().arrayType() == String.class) {
            returnList = Arrays.asList((String[]) memKeys);
        } else if (memKeys instanceof final Value value) {
            returnList = value.getMemberKeys().stream().toList();
        } else {
            returnList = new ArrayList<>();
        }

        return returnList;
    }

}