/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.proxy;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.graalvm.GraalVMRuntimeException;
import org.graalvm.GraalVMScriptEnvironment;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyExecutable;
import org.graalvm.utilities.GraalVMExecutableArgumentsUtil;
import org.graalvm.utilities.GraalVMHostArguments;
import org.graalvm.utilities.GraalVMHostToGuestLanguageUtil;
import org.graalvm.utilities.GraalVMValueUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GraalVMProxyExecutable implements ProxyExecutable {

    private static final Logger LOGGER = LoggerFactory.getLogger(GraalVMProxyExecutable.class);

    private final GraalVMExecutableArgumentsUtil argUtl = new GraalVMExecutableArgumentsUtil();
    private final GraalVMHostToGuestLanguageUtil hostToGuestUtil = new GraalVMHostToGuestLanguageUtil();
    private final GraalVMValueUtil valueUtil = new GraalVMValueUtil();

    private GraalVMScriptEnvironment scriptEnvironment;
    private final Context context;
    private final Object hostObject;

    private Method method;

    public GraalVMProxyExecutable(final GraalVMScriptEnvironment scriptEnvironment, final Object hostObject, final Method method) {

        this.scriptEnvironment = scriptEnvironment;
        this.context = this.scriptEnvironment.getContext();
        this.hostObject = hostObject;
        this.method = method;

        if (context == null) {
            throw new GraalVMRuntimeException("The GraalVMScriptEnvironment can not be null when instantiating a GraalVMProxyExecutable");
        }

        if (this.hostObject == null) {
            throw new GraalVMRuntimeException("The Host Object which contains the method can not be null when instantiating a GraalVMProxyExecutable");
        }

        if (this.method == null) {
            throw new GraalVMRuntimeException("The Method to be called can not be null when instantiating a GraalVMProxyExecutable");
        }

    }

    public Method getMethod() {

        return method;
    }

    @Override
    public Object execute(final Value... arguments) {

        try {
            validateCorrectMethodWasPassedIn(arguments);
        } catch (Exception e) {
            logExceptionForExecutionOfMethodWithParameters(e, arguments);
            throw new GraalVMRuntimeException(e.getMessage(), e);
        }

        Object returnObj;

        if (method.getParameterCount() == 0) {
            returnObj = executeMethodWithNoParameters();
        } else {
            returnObj = executeMethodWithParameters(arguments);
        }

        return returnGuestLanguageObject(returnObj);
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName()).append(" wrapping ").append(hostObject.getClass().getName()).append(".").append(method.getName()).append("(");

        final List<String> classNames = new ArrayList<>();

        for (final Class<?> paramType : method.getParameterTypes()) {
            classNames.add(paramType.getName());
        }

        if (CollectionUtils.isNotEmpty(classNames)) {
            sb.append(StringUtils.join(classNames, ", "));
        }

        sb.append(")");

        return sb.toString();
    }

    Object executeMethodWithNoParameters() {

        Object returnObj;

        try {
            returnObj = method.invoke(hostObject);
        } catch (final Exception e) {
            logExceptionForExecutionOfMethodWithNoParameters(e);
            throw new GraalVMRuntimeException(e.getMessage(), e);
        }

        return returnObj;
    }


    Object executeMethodWithParameters(final Value... arguments) {

        GraalVMHostArguments hostArgs = null;
        Object returnObj;

        try {
            hostArgs = argUtl.getHostArguments(scriptEnvironment, method, arguments);
            returnObj = method.invoke(hostObject, hostArgs.getHostArgs());

            // This is to translate any Guest Language Objects that are maps, or arrays, that have been modified by the Host Language
            hostArgs.doTranslations();
        } catch (Exception e) {
            logExceptionForExecutionOfMethodWithParameters(hostArgs, e);
            throw new GraalVMRuntimeException(e.getMessage(), e);
        }

        return returnObj;
    }

    void validateCorrectMethodWasPassedIn(final Value... arguments) {

        final int paramCount = method.getParameterCount();
        final GraalVMHostArguments hostArgs = argUtl.getHostArgumentsWithNoBackfill(scriptEnvironment, method, arguments);
        final int hostArgLength = hostArgs.getLength();

        if (paramCount != hostArgLength || !validateParameterTypes(method, hostArgs)) {
            findMethod(hostArgs);
        }

    }

    void findMethod(final GraalVMHostArguments hostArguments) {

        if (findMethodBasedOnPassedInArguments(hostArguments)) {
            return;
        }

        final List<Method> methodsWithSameName = findMethodsWithTheSameName();
        final int numberOfMethodsWithSameName = methodsWithSameName.size();

        if (numberOfMethodsWithSameName == 0) {
            throw new GraalVMRuntimeException("GraalVMProxyExecutable.findMethod() could not find a method with name '" + method.getName() + "' on Host Object '" + hostObject.getClass().getName() + "'");
        } else if (numberOfMethodsWithSameName > 1) {

            for (final Method method : methodsWithSameName) {

                if (validateParameterTypes(method, hostArguments)) {
                    this.method = method;
                }

            }

        }

    }

    boolean findMethodBasedOnPassedInArguments(final GraalVMHostArguments hostArguments) {

        final Class<?>[] hostArgTypes = hostArguments.getHostArgTypes();

        try {
            method = hostObject.getClass().getMethod(method.getName(), hostArgTypes);
            return true;
        } catch (final Exception e) {
            // Do nothing, this may be a valid case where the arguments passed in do not match the method name
            // Guest Languages allow for the calling of a function/method with not the same number of arguments.
            // Guest Languages default to setting the parameters as null.  Java does not do this.
        }

        return false;
    }

    List<Method> findMethodsWithTheSameName() {

        final Method[] methods = hostObject.getClass().getMethods();
        final List<Method> methodsWithSameName = new ArrayList<>();

        for (final Method method : methods) {

            if (StringUtils.equals(this.method.getName(), method.getName())) {
                methodsWithSameName.add(method);
            }

        }

        return methodsWithSameName;
    }

    boolean validateParameterTypes(final Method method, final GraalVMHostArguments hostArguments) {

        final Class<?>[] paramTypes = method.getParameterTypes();
        final Object[] hostArgs = hostArguments.getHostArgs();
        final int paramTypeLength = paramTypes.length;
        final int hostArgLength = hostArgs.length;

        if (paramTypeLength < hostArgs.length) {
            return false;
        }

        for (int index = 0; index < paramTypeLength; index++) {
            final Class<?> paramType = paramTypes[index];

            if (index < hostArgLength) {
                final Object hostArg = hostArgs[index];

                if (hostArg == null) {

                    if (paramType.isPrimitive()) {
                        return false;
                    }

                } else {
                    final Class<?> hostArgClass = hostArg.getClass();

                    if (paramType.isPrimitive()) {
                        final Class<?> primitiveClass = ClassUtils.wrapperToPrimitive(hostArgClass);

                        if (paramType != primitiveClass) {
                            return false;
                        }

                    } else if (paramType.isArray()) {

                        if (hostArgClass.isArray()) {

                            if (paramType.arrayType() != hostArgClass.arrayType()) {
                                return false;
                            }

                        } else {
                            return false;
                        }

                    } else if (!paramType.isInstance(hostArg)) {
                        return false;
                    }

                }

            } else {

                if (paramType.isPrimitive()) {
                    return false;
                }

            }

        }

        return true;
    }

    Object returnGuestLanguageObject(final Object returnObj) {

        if (returnObj == null) {
            return valueUtil.createUndefined(context);
        }

        return hostToGuestUtil.createGuestLanguageObject(scriptEnvironment, returnObj);
    }

    private void logExceptionForExecutionOfMethodWithNoParameters(final Exception e) {

        LOGGER.error("***** Start Proxy Executable Invocation Exception *****");
        LOGGER.error("Script Type: {}", scriptEnvironment.getScriptType());
        LOGGER.error("Context: {}", context);
        LOGGER.error("Bindings: {}", scriptEnvironment.getBindings().getMemberKeys());
        LOGGER.error("Method: '{}' on Object: {}", method.getName(), hostObject.getClass().getName());
        LOGGER.error(e.getMessage(), e);
        LOGGER.error("***** End Proxy Executable Invocation Exception *****");
    }

    private void logExceptionForExecutionOfMethodWithParameters(final Exception exception, final Value... arguments) {

        final GraalVMHostArguments hostArgs = argUtl.getHostArgumentsWithNoBackfill(scriptEnvironment, method, arguments);
        logExceptionForExecutionOfMethodWithParameters(hostArgs, exception);
    }

    private void logExceptionForExecutionOfMethodWithParameters(final GraalVMHostArguments graalVMHostArguments, final Exception exception) {

        Object[] hostArgs;

        if (graalVMHostArguments == null) {
            hostArgs = new Object[0];
        } else {
            hostArgs = graalVMHostArguments.getHostArgs();
        }

        LOGGER.error("***** Start Proxy Executable Invocation Exception *****");
        LOGGER.error("Context: {}", context);
        LOGGER.error("Method: '{}' on Object: {}", method.getName(), hostObject.getClass().getName());
        LOGGER.error("Expected Parameter Types:");
        Arrays.asList(method.getParameterTypes()).forEach(type -> LOGGER.error("Parameter Type: {}", type.getName()));

        if (hostArgs == null) {
            LOGGER.error("Host Arguments are NULL");
        } else {
            LOGGER.error("Expected: {} and got {}", method.getParameterCount(), hostArgs.length);
            LOGGER.error("Argument Types:");

            for (final Object arg : hostArgs) {

                if (arg == null) {
                    LOGGER.error("Arg is NULL ");
                } else {
                    LOGGER.error("Arg Type: {}", arg.getClass().getName());
                    LOGGER.error("Arg: {}", arg);
                }

            }

        }

        LOGGER.error(exception.getMessage(), exception);
        LOGGER.error("***** End Proxy Executable Invocation Exception *****");
    }

}