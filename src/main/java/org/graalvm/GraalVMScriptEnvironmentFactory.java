/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.graalvm.javascript.GraalVMJavaScriptEnvironment;
import org.graalvm.polyglot.Context;
import org.graalvm.python.GraalVMPythonScriptEnvironment;
import org.graalvm.rlanguage.GraalVMRLanguageScriptEnvironment;
import org.graalvm.ruby.GraalVMRubyScriptEnvironment;
import org.graalvm.utilities.GraalVMScriptConstants;

// TODO: Is there a way to figure out which Languages are supported dynamically?

public class GraalVMScriptEnvironmentFactory {

    private final GraalVMContextFactory contextFactory = new GraalVMContextFactory();

    private final Map<String, Class<? extends GraalVMScriptEnvironment>> supportedLanguages;

    public GraalVMScriptEnvironmentFactory() {

        supportedLanguages = new HashMap<>();

        supportedLanguages.put(GraalVMScriptConstants.SCRIPT_TYPE_JS, GraalVMJavaScriptEnvironment.class);
        supportedLanguages.put(GraalVMScriptConstants.SCRIPT_TYPE_PYTHON, GraalVMPythonScriptEnvironment.class);
        supportedLanguages.put(GraalVMScriptConstants.SCRIPT_TYPE_R, GraalVMRLanguageScriptEnvironment.class);
        supportedLanguages.put(GraalVMScriptConstants.SCRIPT_TYPE_RUBY, GraalVMRubyScriptEnvironment.class);
    }

    public GraalVMScriptEnvironmentFactory(final Map<String, Class<? extends GraalVMScriptEnvironment>> supportedLanguages) {

        this.supportedLanguages = supportedLanguages;
    }

    public GraalVMScriptEnvironment createScriptEnvironment(final String scriptType, final Map<String, Object> contextConfigMap) {

        if (!supportedLanguages.containsKey(scriptType)) {
            throw new GraalVMRuntimeException("The Script Type: '" + scriptType + "' is not supported");
        }

        final Context context = createContext(scriptType, contextConfigMap);

        return createScriptEnvironment(context, scriptType);
    }

    public GraalVMScriptEnvironment createScriptEnvironment(final Context context, final String scriptType) {

        if (supportedLanguages.containsKey(scriptType)) {
            final Class<? extends GraalVMScriptEnvironment> scriptClass = supportedLanguages.get(scriptType);

            if (scriptClass == null) {
                throw new GraalVMRuntimeException("The Script Type: '" + scriptType + "' has no corresponding class");
            }

            return createScriptEnvironment(scriptClass, context);
        }

        throw new GraalVMRuntimeException("The Script Type: '" + scriptType + "' is not supported");

    }

    GraalVMScriptEnvironment createScriptEnvironment(final Class<? extends GraalVMScriptEnvironment> scriptEnvClass, final Context context) {

        try {
            final Constructor<? extends GraalVMScriptEnvironment> constructor = scriptEnvClass.getConstructor(Context.class);
            return constructor.newInstance(context);
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException |
                 IllegalAccessException e) {
            throw new GraalVMRuntimeException(e);
        }

    }

    Context createContext(final String scriptType, final Map<String, Object> contextConfigMap) {

        Context context = GraalVMContextCache.getContext();

        if (context == null) {
            context = contextFactory.buildContext(scriptType, contextConfigMap);
            GraalVMContextCache.setContext(context);
        }

        return context;
    }

}