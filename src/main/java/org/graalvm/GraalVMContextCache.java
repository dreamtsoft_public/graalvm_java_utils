package org.graalvm;

import org.graalvm.polyglot.Context;

public class GraalVMContextCache {

    private static ThreadLocal<Context> threadLocalContext = new ThreadLocal<>();

    public static Context getContext() {
        return threadLocalContext.get();
    }

    public static void setContext(final Context context) {
        threadLocalContext.set(context);
    }

    public static void removeContext() {
        threadLocalContext.remove();
    }

}