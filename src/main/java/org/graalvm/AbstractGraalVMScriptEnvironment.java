/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;

public abstract class AbstractGraalVMScriptEnvironment implements GraalVMScriptEnvironment {

    private final Context context;

    private String scriptType;

    public AbstractGraalVMScriptEnvironment(final Context context) {

        this.context = context;
    }

    public void close() {

        try {
            context.close();
            GraalVMContextCache.removeContext();
        } catch (final Exception e) {
            final String sb = "!!!!! Closing Context on Environment - " +
                    context +
                    " threw an exception" +
                    System.lineSeparator() +
                    e.getMessage();

            throw new GraalVMRuntimeException(sb, e);
        }

    }

    public Context getContext() {

        return context;
    }

    public String getScriptType() {

        return scriptType;
    }

    public Value getBindings() {

        return getContext().getBindings(getScriptType());
    }

    protected void setScriptType(final String scriptType) {

        this.scriptType = scriptType;
    }

}