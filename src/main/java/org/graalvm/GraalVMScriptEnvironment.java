/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Map;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;
import org.graalvm.proxy.GraalVMProxyExecutable;

public interface GraalVMScriptEnvironment {

    void close();

    String getScriptType();

    Context getContext();

    Value getBindings();

    GraalVMProxyExecutable getGuestLanguageExecutable(final Object hostObject, final Method method);

    Object getGuestLanguageObject(final Object hostObject);

    Object getHostLanguageObject(final Object guestObject);

    boolean isArray(final Object obj);

    boolean isNullOrUndefined(Object o);

    boolean isNotNullOrUndefined(Object o);

    boolean isFunction(final Object obj);

    boolean hasPrototypeFunction(final Object protoObject, final String functionName);

    GraalVMWrappedObject wrap(final Object guestObject);

    GraalVMWrappedObject createNewString(final Object... args);

    GraalVMWrappedObject createNewObject(final Object... args);

    GraalVMWrappedObject createNewArray(final Object... args);

    GraalVMWrappedObject createNewFunction(final Object... args);

    GraalVMWrappedObject createFunctionClass(final String name, final Object... args);

    GraalVMWrappedObject getStringClass();

    GraalVMWrappedObject getObjectClass();

    GraalVMWrappedObject getArrayClass();

    GraalVMWrappedObject getFunctionClass();

    GraalVMWrappedObject createUndefined();

    GraalVMWrappedObject createMemberClass(final String name);

    GraalVMWrappedObject getTypedClass(final String objectName);

    GraalVMWrappedObject getGlobalObject(final String name);

    GraalVMWrappedObject findGlobalObject(final String objectName);

    void setGlobals(final Map<String, Object> globalMap);

    void setGlobal(final String name, final Object value);

    GraalVMWrappedObject getGlobalProperty(final String objectName);

    GraalVMWrappedObject scriptEvaluate(final File scriptFile, Object... args);

    GraalVMWrappedObject scriptEvaluate(final String script, final Object... args);

    Object scriptEvaluateWithGlobalGindings(final String script, final Map<String, Object> globalBindings, final String var, final Object... args);

}