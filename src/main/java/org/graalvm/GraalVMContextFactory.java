/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.logging.Handler;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Engine;
import org.graalvm.polyglot.EnvironmentAccess;
import org.graalvm.polyglot.HostAccess;
import org.graalvm.polyglot.PolyglotAccess;
import org.graalvm.polyglot.ResourceLimits;
import org.graalvm.polyglot.io.FileSystem;
import org.graalvm.polyglot.io.MessageTransport;
import org.graalvm.polyglot.io.ProcessHandler;

public class GraalVMContextFactory {

    private final Map<String, Object> contextConfigMap;

    public GraalVMContextFactory() {

        contextConfigMap = new HashMap<>();
    }

    public GraalVMContextFactory(final Map<String, Object> contextConfigMap) {

        this.contextConfigMap = contextConfigMap;
    }

    public Context buildContext() {

        return buildContext(contextConfigMap);
    }

    public Context buildContext(final String scriptType) {

        return buildContext(scriptType, contextConfigMap);
    }

    public Context buildContext(final Map<String, Object> contextConfigMap) {

        return buildContext(StringUtils.EMPTY, contextConfigMap);
    }

    public Context buildContext(final String scriptType, final Map<String, Object> contextConfigMap) {

        Context.Builder builder = Context.newBuilder();

        if (MapUtils.isEmpty(contextConfigMap)) {
            builder.allowAllAccess(true);
        } else {
            builder = setAllowAllAccess(builder, contextConfigMap);
            builder = setAllowCreateProcess(builder, contextConfigMap);
            builder = setAllowCreateThread(builder, contextConfigMap);
            builder = setAllowEnvironmentAccess(builder, contextConfigMap);
            builder = setAllowExperimentalOptions(builder, contextConfigMap);
            builder = setAllowHostAccess(builder, contextConfigMap);
            builder = setAllowHostClassLoading(builder, contextConfigMap);
            builder = setAllowHostClassLookup(builder, contextConfigMap);
            builder = setAllowIO(builder, contextConfigMap);
            builder = setAllowNativeAccess(builder, contextConfigMap);
            builder = setAllowPolyglotAccess(builder, contextConfigMap);
            builder = setAllowValueSharing(builder, contextConfigMap);
            builder = setArguments(scriptType, builder, contextConfigMap);
            builder = setCurrentWorkingDirectory(builder, contextConfigMap);
            builder = setEngine(builder, contextConfigMap);
            builder = setEnvironment(builder, contextConfigMap);
            builder = setStandardError(builder, contextConfigMap);
            builder = setFileSystem(builder, contextConfigMap);
            builder = setStandardIn(builder, contextConfigMap);
            builder = setLogHander(builder, contextConfigMap);
            builder = setStandardOut(builder, contextConfigMap);
            builder = setProcessHandler(builder, contextConfigMap);
            builder = setResourceLimits(builder, contextConfigMap);
            builder = setServerTransport(builder, contextConfigMap);
            builder = setTimeZone(builder, contextConfigMap);
            builder = setUseSystemExit(builder, contextConfigMap);
        }

        return builder.build();
    }

    Context.Builder setAllowAllAccess(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final boolean enabled = getBooleanValue(GraalVMConfigConstants.ALLOW_ALL_ACCESS, contextConfigMap);

        return builder.allowAllAccess(enabled);
    }

    Context.Builder setAllowCreateProcess(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final boolean enabled = getBooleanValue(GraalVMConfigConstants.ALLOW_CREATE_PROCESS, contextConfigMap);

        return builder.allowCreateProcess(enabled);
    }

    Context.Builder setAllowCreateThread(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final boolean enabled = getBooleanValue(GraalVMConfigConstants.ALLOW_CREATE_THREAD, contextConfigMap);

        return builder.allowCreateThread(enabled);
    }

    Context.Builder setAllowEnvironmentAccess(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.ALLOW_ENVIRONMENT_ACCESS);

        if (value instanceof final EnvironmentAccess accessPolicy) {
            return builder.allowEnvironmentAccess(accessPolicy);
        }

        return builder;
    }

    Context.Builder setAllowExperimentalOptions(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final boolean enabled = getBooleanValue(GraalVMConfigConstants.ALLOW_EXPERIMENTAL_OPTIONS, contextConfigMap);

        if (enabled) {
            final Object optionsValue = contextConfigMap.get(GraalVMConfigConstants.OPTIONS);

            if (optionsValue instanceof final Map options) {
                return builder.allowExperimentalOptions(true).options(options);
            }

        }

        return builder.allowExperimentalOptions(enabled);
    }

    Context.Builder setAllowHostAccess(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.ALLOW_HOST_ACCESS);

        if (value instanceof final HostAccess hostAcess) {
            return builder.allowHostAccess(hostAcess);
        }

        return builder;
    }

    Context.Builder setAllowHostClassLoading(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final boolean enabled = getBooleanValue(GraalVMConfigConstants.ALLOW_HOST_CLASS_LOADING, contextConfigMap);

        if (enabled) {
            final Object value = contextConfigMap.get(GraalVMConfigConstants.HOST_CLASS_LOADER);

            if (value instanceof final ClassLoader classLoader) {
                return builder.allowHostClassLoading(true).hostClassLoader(classLoader);
            }

        }

        return builder.allowHostClassLoading(enabled);
    }

    Context.Builder setAllowHostClassLookup(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.ALLOW_HOST_CLASS_LOOKUP);

        if (value instanceof final Predicate predicate) {
            return builder.allowHostClassLookup(predicate);
        }

        return builder;
    }

    Context.Builder setAllowIO(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final boolean enabled = getBooleanValue(GraalVMConfigConstants.ALLOW_IO, contextConfigMap);

        return builder.allowIO(enabled);
    }

    Context.Builder setAllowNativeAccess(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final boolean enabled = getBooleanValue(GraalVMConfigConstants.ALLOW_NATIVE_ACCESS, contextConfigMap);

        return builder.allowNativeAccess(enabled);
    }

    Context.Builder setAllowPolyglotAccess(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.ALLOW_POLUGLOT_ACCESS);

        if (value instanceof PolyglotAccess accessPolicy) {
            return builder.allowPolyglotAccess(accessPolicy);
        }

        return builder;
    }

    Context.Builder setAllowValueSharing(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final boolean enabled = getBooleanValue(GraalVMConfigConstants.ALLOW_VALUE_SHARING, contextConfigMap);

        return builder.allowValueSharing(enabled);
    }

    Context.Builder setArguments(final String language, final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        if (StringUtils.isBlank(language)) {
            return builder;
        }

        final Object value = contextConfigMap.get(GraalVMConfigConstants.ARGUMENTS);

        if (value != null && value.getClass().isArray()) {

            if (value.getClass().getComponentType().equals(String.class)) {
                final Object[] objectArray = (Object[]) value;
                final String[] args = Arrays.copyOf(objectArray, objectArray.length, String[].class);
                return builder.arguments(language, args);
            }

        }

        return builder;
    }

    Context.Builder setCurrentWorkingDirectory(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.CURRENT_WORKING_DIRECTORY);

        if (value != null && value.getClass().isArray()) {

            if (value.getClass().getComponentType().equals(String.class)) {
                final Object[] objectArray = (Object[]) value;

                if (ArrayUtils.isEmpty(objectArray)) {
                    return builder;
                }

                final String[] paths = Arrays.copyOf(objectArray, objectArray.length, String[].class);

                if (objectArray.length == 1) {
                    return builder.currentWorkingDirectory(Paths.get(paths[0]));
                } else if (objectArray.length > 1) {
                    final String[] subPaths = Arrays.copyOfRange(paths, 1, paths.length);
                    return builder.currentWorkingDirectory(Paths.get(paths[0], subPaths));
                }

            }

        } else if (value instanceof final String path) {
            return builder.currentWorkingDirectory(Paths.get(path));
        } else if (value instanceof final File filePath) {
            return builder.currentWorkingDirectory(Paths.get(filePath.getAbsolutePath()));
        } else if (value instanceof final Path workingDirectory) {
            return builder.currentWorkingDirectory(workingDirectory);
        }

        return builder;
    }

    Context.Builder setEngine(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.ENGINE);

        if (value instanceof final Engine engine) {
            return builder.engine(engine);
        }

        return builder;
    }

    Context.Builder setEnvironment(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.ENVIRONMENT);

        if (value instanceof final Map map) {
            return builder.environment(map);
        }

        return builder;
    }

    Context.Builder setStandardError(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.STD_ERR);

        if (value instanceof final OutputStream stderr) {
            return builder.err(stderr);
        }

        return builder;
    }

    Context.Builder setFileSystem(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.FILE_SYSTEM);

        if (value instanceof final FileSystem fileSystem) {
            return builder.fileSystem(fileSystem);
        }

        return builder;
    }

    Context.Builder setStandardIn(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.STD_IN);

        if (value instanceof final InputStream stdin) {
            return builder.in(stdin);
        }

        return builder;
    }

    Context.Builder setLogHander(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.LOG_HANDLER);

        if (value instanceof final Handler logHandler) {
            return builder.logHandler(logHandler);
        } else if (value instanceof final OutputStream logHandler) {
            return builder.logHandler(logHandler);
        }

        return builder;
    }

    Context.Builder setStandardOut(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.STD_OUT);

        if (value instanceof final OutputStream stdout) {
            return builder.out(stdout);
        }

        return builder;
    }

    Context.Builder setProcessHandler(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.PROCESS_HANDLER);

        if (value instanceof final ProcessHandler handler) {
            return builder.processHandler(handler);
        }

        return builder;
    }

    Context.Builder setResourceLimits(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.RESOURCE_LIMITS);

        if (value instanceof final ResourceLimits limits) {
            return builder.resourceLimits(limits);
        }

        return builder;
    }

    Context.Builder setServerTransport(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.SERVER_TRANSPORT);

        if (value instanceof final MessageTransport serverTransport) {
            return builder.serverTransport(serverTransport);
        }

        return builder;
    }

    Context.Builder setTimeZone(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(GraalVMConfigConstants.TIME_ZONE);

        if (value instanceof final ZoneId zone) {
            return builder.timeZone(zone);
        }

        return builder;
    }

    Context.Builder setUseSystemExit(final Context.Builder builder, final Map<String, Object> contextConfigMap) {

        final boolean enabled = getBooleanValue(GraalVMConfigConstants.USER_SYSTEM_EXIT, contextConfigMap);

        return builder.useSystemExit(enabled);
    }

    boolean getBooleanValue(final String key, final Map<String, Object> contextConfigMap) {

        final Object value = contextConfigMap.get(key);

        if (value instanceof final Boolean booleanValue) {
            return booleanValue;
        }

        return false;
    }


}