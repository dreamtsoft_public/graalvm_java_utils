/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.rlanguage;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Map;

import org.graalvm.AbstractGraalVMScriptEnvironment;
import org.graalvm.GraalVMWrappedObject;
import org.graalvm.polyglot.Context;
import org.graalvm.proxy.GraalVMProxyExecutable;

public class GraalVMRLanguageScriptEnvironment extends AbstractGraalVMScriptEnvironment {


    public GraalVMRLanguageScriptEnvironment(final Context context) {

        super(context);
    }

    @Override
    public GraalVMProxyExecutable getGuestLanguageExecutable(final Object hostObject, final Method method) {

        return null;
    }

    @Override
    public Object getGuestLanguageObject(final Object hostObject) {

        return null;
    }

    @Override
    public Object getHostLanguageObject(final Object guestObject) {

        return null;
    }

    @Override
    public boolean isArray(final Object obj) {

        return false;
    }

    @Override
    public boolean isNullOrUndefined(final Object o) {

        return false;
    }

    @Override
    public boolean isNotNullOrUndefined(final Object o) {

        return false;
    }

    @Override
    public boolean isFunction(final Object obj) {

        return false;
    }

    @Override
    public boolean hasPrototypeFunction(final Object protoObject, final String functionName) {

        return false;
    }

    @Override
    public GraalVMWrappedObject wrap(final Object guestObject) {

        return null;
    }

    @Override
    public GraalVMWrappedObject createNewString(final Object... args) {

        return null;
    }

    @Override
    public GraalVMWrappedObject createNewObject(final Object... args) {

        return null;
    }

    @Override
    public GraalVMWrappedObject createNewArray(final Object... args) {

        return null;
    }

    @Override
    public GraalVMWrappedObject createNewFunction(final Object... args) {

        return null;
    }

    @Override
    public GraalVMWrappedObject createFunctionClass(final String name, final Object... args) {

        return null;
    }

    @Override
    public GraalVMWrappedObject getStringClass() {

        return null;
    }

    @Override
    public GraalVMWrappedObject getObjectClass() {

        return null;
    }

    @Override
    public GraalVMWrappedObject getArrayClass() {

        return null;
    }

    @Override
    public GraalVMWrappedObject getFunctionClass() {

        return null;
    }

    @Override
    public GraalVMWrappedObject createUndefined() {

        return null;
    }

    @Override
    public GraalVMWrappedObject createMemberClass(final String name) {

        return null;
    }

    @Override
    public GraalVMWrappedObject getTypedClass(final String objectName) {

        return null;
    }

    @Override
    public GraalVMWrappedObject getGlobalObject(final String name) {

        return null;
    }

    @Override
    public GraalVMWrappedObject findGlobalObject(final String objectName) {

        return null;
    }

    @Override
    public void setGlobals(final Map<String, Object> globalMap) {

    }

    @Override
    public void setGlobal(final String name, final Object value) {

    }

    @Override
    public GraalVMWrappedObject getGlobalProperty(final String objectName) {

        return null;
    }

    @Override
    public GraalVMWrappedObject scriptEvaluate(final File scriptFile, final Object... args) {

        return null;
    }

    @Override
    public GraalVMWrappedObject scriptEvaluate(final String script, final Object... args) {

        return null;
    }

    @Override
    public Object scriptEvaluateWithGlobalGindings(final String script, final Map<String, Object> globalBindings, final String var, final Object... args) {

        return null;
    }

}