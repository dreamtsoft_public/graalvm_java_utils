/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm;

public final class GraalVMConfigConstants {

    private GraalVMConfigConstants() {
        // Ensuring this is the only class.
    }

    public static final String ALLOW_ALL_ACCESS = "allowAllAccess";
    public static final String ALLOW_CREATE_PROCESS = "allowCreateProcess";
    public static final String ALLOW_CREATE_THREAD = "allowCreateThread";
    public static final String ALLOW_ENVIRONMENT_ACCESS = "allowEnvironmentAccess";
    public static final String ALLOW_EXPERIMENTAL_OPTIONS = "allowExperimentalOptions";
    public static final String ALLOW_HOST_ACCESS = "allowHostAccess";
    public static final String ALLOW_HOST_CLASS_LOADING = "allowHostClassLoading";
    public static final String ALLOW_HOST_CLASS_LOOKUP = "allowHostClassLookup";
    public static final String ALLOW_IO = "allowIO";
    public static final String ALLOW_NATIVE_ACCESS = "allowNativeAccess";
    public static final String ALLOW_POLUGLOT_ACCESS = "allowPolyglotAccess";
    public static final String ALLOW_VALUE_SHARING = "allowValueSharing";
    public static final String ARGUMENTS = "arguments";
    public static final String CURRENT_WORKING_DIRECTORY = "currentWorkingDirectory";
    public static final String ENGINE = "engine";
    public static final String ENVIRONMENT = "environment";
    public static final String STD_ERR = "stderr";
    public static final String FILE_SYSTEM = "fileSystem";
    public static final String HOST_CLASS_LOADER = "hostClassLoader";
    public static final String STD_IN = "stdin";
    public static final String LOG_HANDLER = "logHandler";
    public static final String OPTIONS = "options";
    public static final String STD_OUT = "stdout";
    public static final String PROCESS_HANDLER = "processHandler";
    public static final String RESOURCE_LIMITS = "resourceLimits";
    public static final String SERVER_TRANSPORT = "serverTransport";
    public static final String TIME_ZONE = "timeZone";
    public static final String USER_SYSTEM_EXIT = "useSystemExit";

}