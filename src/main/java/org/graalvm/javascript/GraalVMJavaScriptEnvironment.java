/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.javascript;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.graalvm.AbstractGraalVMScriptEnvironment;
import org.graalvm.GraalVMRuntimeException;
import org.graalvm.GraalVMWrappedObject;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;
import org.graalvm.proxy.GraalVMProxyExecutable;
import org.graalvm.utilities.GraalVMExecutableUtil;
import org.graalvm.utilities.GraalVMGuestLanguageToHostUtil;
import org.graalvm.utilities.GraalVMHostToGuestLanguageUtil;
import org.graalvm.utilities.GraalVMScriptConstants;
import org.graalvm.utilities.GraalVMUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GraalVMJavaScriptEnvironment extends AbstractGraalVMScriptEnvironment {

    private final Logger logger = LoggerFactory.getLogger(GraalVMJavaScriptEnvironment.class);
    private final GraalVMHostToGuestLanguageUtil hostToGuestUtil = new GraalVMHostToGuestLanguageUtil();
    private final GraalVMGuestLanguageToHostUtil guestToHostUtil = new GraalVMGuestLanguageToHostUtil();
    private final GraalVMExecutableUtil executableUtil = new GraalVMExecutableUtil();
    private final GraalVMUtil graalVmUtil = new GraalVMUtil();


    public GraalVMJavaScriptEnvironment(final Context context) {

        super(context);

        setScriptType(GraalVMScriptConstants.SCRIPT_TYPE_JS);
    }

    @Override
    public GraalVMWrappedObject wrap(final Object obj) {

        if (obj instanceof GraalVMWrappedObject) {
            return (GraalVMWrappedObject) obj;
        }

        if (obj == null) {
            return new GraalVMWrappedObject(this, getContext().asValue(null));
        }

        final Value value = switch (obj) {
            case Value objValue -> objValue;
            case String script -> getContext().eval(getScriptType(), script);
            default -> getContext().asValue(obj);
        };

        return new GraalVMWrappedObject(this, value);
    }

    @Override
    public GraalVMWrappedObject createUndefined() {

        return new GraalVMWrappedObject(this, getContext().asValue(null));
    }

    @Override
    public GraalVMProxyExecutable getGuestLanguageExecutable(final Object hostObject, final Method method) {

        return hostToGuestUtil.getExecutable(this, hostObject, method);
    }

    @Override
    public Object getGuestLanguageObject(final Object hostObject) {

        return hostToGuestUtil.createGuestLanguageObject(this, hostObject);
    }

    @Override
    public Object getHostLanguageObject(final Object guestObject) {

        return guestToHostUtil.getHostObject(this, guestObject);
    }

    @Override
    public boolean isArray(final Object obj) {

        return graalVmUtil.isArray(this, obj);
    }


    public boolean isNullOrUndefined(Object o) {

        return graalVmUtil.isNullOrUndefined(o);
    }

    public boolean isNotNullOrUndefined(Object o) {

        return !graalVmUtil.isNullOrUndefined(o);
    }

    @Override
    public boolean isFunction(final Object obj) {

        return graalVmUtil.isFunction(obj);
    }

    @Override
    public boolean hasPrototypeFunction(final Object protoObject, final String functionName) {

        if (protoObject == null) {
            return false;
        }

        boolean isFunc = false;

        if (protoObject instanceof final Value value) {
            final Value newValue = executableUtil.instantiateValue(this, value);

            isFunc = Objects.requireNonNullElse(newValue, value).hasMember(functionName);
        } else if (protoObject instanceof final GraalVMWrappedObject wrappedObject) {
            final Value value = wrappedObject.getValue();
            final Value newValue = executableUtil.instantiateValue(this, value);

            isFunc = Objects.requireNonNullElse(newValue, value).hasMember(functionName);
        } else {
            final Object guestObject = hostToGuestUtil.createGuestLanguageObject(this, protoObject);

            if (isNotNullOrUndefined(guestObject)) {
                final Object protoFunc = graalVmUtil.getMember(guestObject, functionName);

                if (isNotNullOrUndefined(protoFunc)) {
                    isFunc = isFunction(protoFunc);
                }

            }

        }

        return isFunc;
    }


    @Override
    public GraalVMWrappedObject createNewString(final Object... args) {

        return createFunctionClass(GraalVMScriptConstants.STRING_CLASS_NAME, args);
    }

    @Override
    public GraalVMWrappedObject createNewObject(final Object... args) {

        return createFunctionClass(GraalVMScriptConstants.OBJECT_CLASS_NAME, args);
    }

    @Override
    public GraalVMWrappedObject createNewArray(final Object... args) {

        return createFunctionClass(GraalVMScriptConstants.ARRAY_CLASS_NAME, args);
    }

    @Override
    public GraalVMWrappedObject createNewFunction(final Object... args) {

        return createFunctionClass(GraalVMScriptConstants.FUNCTION_CLASS_NAME, args);
    }

    @Override
    public GraalVMWrappedObject createFunctionClass(final String name, final Object... args) {

        final Value value = hostToGuestUtil.createJavascriptFunctionClass(this, name, args);

        if (value == null) {
            return null;
        }

        return new GraalVMWrappedObject(this, value);
    }

    @Override
    public GraalVMWrappedObject getStringClass() {

        return createMemberClass(GraalVMScriptConstants.STRING_CLASS_NAME);
    }

    @Override
    public GraalVMWrappedObject getObjectClass() {

        return createMemberClass(GraalVMScriptConstants.OBJECT_CLASS_NAME);
    }

    @Override
    public GraalVMWrappedObject getArrayClass() {

        return createMemberClass(GraalVMScriptConstants.ARRAY_CLASS_NAME);
    }

    @Override
    public GraalVMWrappedObject getFunctionClass() {

        return createMemberClass(GraalVMScriptConstants.FUNCTION_CLASS_NAME);
    }

    @Override
    public GraalVMWrappedObject createMemberClass(final String name) {

        final Value value = hostToGuestUtil.createJavascriptMemberClass(this, name);

        if (value == null) {
            return null;
        }

        return new GraalVMWrappedObject(this, value);
    }

    @Override
    public GraalVMWrappedObject getTypedClass(final String objectName) {

        if (StringUtils.isBlank(objectName)) {
            return null;
        }

        final String script = "(" + objectName + ")";
        final String scriptFile = "Create_new_" + objectName;

        try {
            final Source source = Source.newBuilder(getScriptType(), script, scriptFile).build();
            final Value value = getContext().eval(source);
            return new GraalVMWrappedObject(this, value);
        } catch (final Exception e) {
            throw new GraalVMRuntimeException(e.getMessage(), e);
        }

    }

    @Override
    public GraalVMWrappedObject getGlobalObject(final String name) {

        final Value value = getContext().getBindings(getScriptType()).getMember(name);

        if (value == null) {
            return null;
        }

        return new GraalVMWrappedObject(this, value);
    }


    @Override
    public GraalVMWrappedObject findGlobalObject(final String objectName) {

        return getGlobalProperty(objectName);
    }

    @Override
    public void setGlobals(final Map<String, Object> globalMap) {

        globalMap.forEach(this::setGlobal);
    }

    @Override
    public void setGlobal(final String name, final Object value) {

        Object globalValue;

        if (value instanceof final GraalVMWrappedObject wrappedObject) {
            globalValue = wrappedObject.getValue();
        } else if (value instanceof final Value polyglotValue) {
            globalValue = polyglotValue;
        } else {
            globalValue = hostToGuestUtil.createGuestLanguageObject(this, value);
        }

        getContext().getBindings(getScriptType()).putMember(name, globalValue);
    }

    @Override
    public GraalVMWrappedObject scriptEvaluate(final File scriptFile, final Object... args) {

        try {
            final String script = FileUtils.readFileToString(scriptFile, StandardCharsets.UTF_8);
            return scriptEvaluate(script, args);
        } catch (final IOException ioe) {
            throw new GraalVMRuntimeException(ioe.getMessage(), ioe);
        }

    }

    @Override
    public GraalVMWrappedObject scriptEvaluate(final String script, final Object... args) {

        try {
            final Value value = getContext().eval(getScriptType(), script);

            Value result = executableUtil.executeOrInstantiateValue(this, value, args);

            if (result == null) {
                result = value;
            }

            return new GraalVMWrappedObject(this, result);
        } catch (final Exception e) {
            logger.error("***** Start Sandbox Invocation Exception *****");
            logger.error("Script Type: {}", getScriptType());
            logger.error("Script: {}", script);
            logger.error("Args: {}", Arrays.toString(args));
            final Value bindings = getBindings();
            bindings.getMemberKeys().forEach(key -> logger.error("Global Binding({}): {}", key, bindings.getMember(key)));
            logger.error(e.getMessage(), e);
            logger.error("***** End Sandbox Invocation Exception *****");
            throw new GraalVMRuntimeException(e.getMessage(), e);
        }

    }


    @Override
    public Object scriptEvaluateWithGlobalGindings(final String script, final Map<String, Object> globalBindings, final String var, final Object... args) {

        final Value bindings = getBindings();

        for (Map.Entry<String, Object> entry : globalBindings.entrySet()) {
            final String key = entry.getKey();

            if (!bindings.hasMember(key)) {
                bindings.putMember(key, entry.getValue());
            }

        }

        Value result;

        try {
            final Value value = getContext().eval(getScriptType(), script);

            result = executableUtil.executeOrInstantiateValue(this, value);

            if (result == null) {
                result = value;
            }

        } catch (final Exception e) {
            logger.error("***** Start Sandbox Invocation Exception *****");
            logger.error("Script Type: {}", getScriptType());
            logger.error("Script: {}", script);
            logger.error("Args: {}", Arrays.toString(args));
            bindings.getMemberKeys().forEach(key -> logger.error("Global Binding({}): {}", key, bindings.getMember(key)));
            logger.error("Return Var: {}", var);
            logger.error(e.getMessage(), e);
            logger.error("***** End Sandbox Invocation Exception *****");
            throw e;
        } finally {

            // We are translate forward, but are not translating back.
            // If the JavaScript changes a Binding, we want to pass it back to the calling class.
            if (MapUtils.isNotEmpty(globalBindings)) {

                for (final String key : globalBindings.keySet()) {
                    final Value value = bindings.getMember(key);
                    final Object val = guestToHostUtil.getHostValue(this, value);
                    globalBindings.put(key, val);
                }

            }

        }

        final GraalVMWrappedObject returnObject;

        if (StringUtils.isBlank(var)) {
            returnObject = new GraalVMWrappedObject(this, result);
        } else {
            returnObject = new GraalVMWrappedObject(this, bindings.getMember(var));
        }

        return returnObject;
    }

    @Override
    public GraalVMWrappedObject getGlobalProperty(String objectName) {

        return switch (objectName) {
            case GraalVMScriptConstants.STRING_CLASS_NAME -> getStringClass();
            case GraalVMScriptConstants.OBJECT_CLASS_NAME -> getObjectClass();
            case GraalVMScriptConstants.ARRAY_CLASS_NAME -> getArrayClass();
            case GraalVMScriptConstants.FUNCTION_CLASS_NAME -> getFunctionClass();
            default -> getTypedClass(objectName);
        };

    }


}