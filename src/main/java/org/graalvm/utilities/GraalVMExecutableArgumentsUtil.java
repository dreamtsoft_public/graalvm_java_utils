/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.graalvm.GraalVMRuntimeException;
import org.graalvm.GraalVMScriptEnvironment;
import org.graalvm.polyglot.Value;

@SuppressWarnings("rawtypes")
public class GraalVMExecutableArgumentsUtil {

    private enum CountMatch {
        EQUAL,
        GREATER,
        LESS
    }

    private final GraalVMValueUtil valueUtil = new GraalVMValueUtil();
    private final GraalVMGuestLanguageToHostUtil guestToHostUtil = new GraalVMGuestLanguageToHostUtil();

    public GraalVMHostArguments getHostArgumentsWithNoBackfill(final GraalVMScriptEnvironment scriptEnv, final Method method, final Value... arguments) {

        if (method == null) {
            throw new GraalVMRuntimeException("Method can not be null");
        }

        if (ArrayUtils.isEmpty(arguments)) {

            return new GraalVMHostArguments(scriptEnv, 0);
        }

        final int paramCount = method.getParameterCount();

        if (paramCount == 0) {

            return new GraalVMHostArguments(scriptEnv, 0);
        }

        final String methodName = method.getName();
        final int argCount = arguments.length;
        final CountMatch argToParamCountMath = getCountMatch(paramCount, argCount);

        GraalVMHostArguments hostArgs;
        int count;

        if (argToParamCountMath == CountMatch.EQUAL || argToParamCountMath == CountMatch.LESS) {
            hostArgs = new GraalVMHostArguments(scriptEnv, argCount);
            count = argCount;
        } else {
            hostArgs = new GraalVMHostArguments(scriptEnv, paramCount);
            count = paramCount;
        }

        final Class[] paramTypes = method.getParameterTypes();

        for (int index = 0; index < count; index++) {
            final Class paramType = paramTypes[index];
            Value arg = arguments[index];

            if (isEndIndex(paramCount, index)) {

                if (paramType.isArray()) {

                    if (argToParamCountMath == CountMatch.GREATER) {
                        addHostArgsAsVarArgs(scriptEnv, methodName, paramCount, argCount, hostArgs, index, arguments);
                    } else {
                        addArray(scriptEnv, paramType, hostArgs, index, arg);
                    }

                } else {
                    addHostArg(scriptEnv, methodName, hostArgs, index, arg);
                }

            } else {

                if (paramType.isArray()) {
                    addArray(scriptEnv, paramType, hostArgs, index, arg);
                } else {
                    addHostArg(scriptEnv, methodName, hostArgs, index, arg);
                }

            }

        }

        return hostArgs;

    }


    // TODO: This method could use some refactoring
    public GraalVMHostArguments getHostArguments(final GraalVMScriptEnvironment scriptEnv, final Method method, final Value... arguments) {

        if (method == null) {
            throw new GraalVMRuntimeException("Method can not be NULL");
        }

        final int paramCount = method.getParameterCount();

        if (paramCount == 0) {
            return null;
        }

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnv, paramCount);

        if (ArrayUtils.isEmpty(arguments)) {
            backfill(hostArgs, 0);

            return hostArgs;
        }

        final String methodName = method.getName();
        final int argCount = arguments.length;
        final CountMatch argToParamCountMath = getCountMatch(paramCount, argCount);
        final Class[] paramTypes = method.getParameterTypes();

        for (int index = 0; index < paramCount; index++) {
            final Class paramType = paramTypes[index];
            Value arg = null;

            if (index < argCount) {
                arg = arguments[index];
            }

            if (isEndIndex(paramCount, index)) {

                if (paramType.isArray()) {

                    if (argToParamCountMath == CountMatch.EQUAL) {
                        addArray(scriptEnv, paramType, hostArgs, index, arg);
                    } else if (argToParamCountMath == CountMatch.LESS) {
                        backfill(hostArgs, index);
                    } else if (argToParamCountMath == CountMatch.GREATER) {
                        addHostArgsAsVarArgs(scriptEnv, methodName, paramCount, argCount, hostArgs, index, arguments);
                    }

                } else {
                    addHostArg(scriptEnv, methodName, hostArgs, index, arg);
                }

            } else {

                if (paramType.isArray()) {
                    addArray(scriptEnv, paramType, hostArgs, index, arg);
                } else {
                    addHostArg(scriptEnv, methodName, hostArgs, index, arg);
                }

            }

        }

        return hostArgs;
    }

    void addHostArg(final GraalVMScriptEnvironment scriptEnv, final String method, final GraalVMHostArguments hostArgs, final int index, final Value arg) {

        final Object hostArg = guestToHostUtil.getHostValue(scriptEnv, arg);

        if (hostArg != null && hostArg.getClass().isArray()) {
            throw new GraalVMRuntimeException("Received an Array when none was expected for method: '" + method + "' for the last parameter");
        }

        if (hostArg instanceof Map && !arg.isHostObject()) {
            hostArgs.addMutableArg(index, hostArg, arg);
        } else {
            hostArgs.addImmutableArg(index, hostArg);
        }

    }

    void addHostArgsAsVarArgs(final GraalVMScriptEnvironment scriptEnv, final String method, final int paramCount, final int argCount, final GraalVMHostArguments hostArgs, final int index, final Value[] arguments) {

        final int varArgCount = (argCount - paramCount) + 1;
        final GraalVMHostArguments hostVarArgs = new GraalVMHostArguments(scriptEnv, varArgCount);

        for (int varArgIndex = 0; varArgIndex < varArgCount; varArgIndex++) {
            final int argumentIndex = index + varArgIndex;
            final Value varArg = arguments[argumentIndex];
            addHostArg(scriptEnv, method, hostVarArgs, varArgIndex, varArg);
        }

        hostArgs.addMutableArg(index, hostVarArgs, null);
    }

    void addArray(final GraalVMScriptEnvironment scriptEnv, final Class paramType, final GraalVMHostArguments hostArgs, final int index, final Value arg) {


        if (valueUtil.isArray(arg)) {
            final Object[] varArgs = guestToHostUtil.getArray(scriptEnv, arg);
            final int varArgLen = varArgs.length;
            final Object hostArg = Array.newInstance(paramType.getComponentType(), varArgLen);

            for (int varArgIndex = 0; varArgIndex < varArgLen; varArgIndex++) {
                Array.set(hostArg, varArgIndex, varArgs[varArgIndex]);
            }

            hostArgs.addMutableArg(index, hostArg, arg);
        } else {
            final Object varArg = guestToHostUtil.getHostValue(scriptEnv, arg);
            final Object hostArg = Array.newInstance(paramType.getComponentType(), 1);
            Array.set(hostArg, 0, varArg);
            hostArgs.addMutableArg(index, hostArg, arg);
        }

    }


    void backfill(final GraalVMHostArguments hostArgs, final int startIndex) {

        final int len = hostArgs.getLength();

        for (int index = startIndex; index < len; index++) {
            hostArgs.addImmutableArg(index, null);
        }

    }

    boolean isEndIndex(final int argCount, final int index) {

        return ((index + 1) >= argCount);
    }

    CountMatch getCountMatch(final int paramCount, final int argCount) {

        if (argCount > paramCount) {
            return CountMatch.GREATER;
        } else if (argCount < paramCount) {
            return CountMatch.LESS;
        }

        return CountMatch.EQUAL;
    }

}