/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import java.util.HashMap;
import java.util.Map;

import org.graalvm.GraalVMRuntimeException;
import org.graalvm.GraalVMScriptEnvironment;
import org.graalvm.polyglot.Value;


public class GraalVMHostArguments {

    private final GraalVMHostToGuestLanguageUtil hostToGuestUtil = new GraalVMHostToGuestLanguageUtil();
    private final Map<Integer, TranslationMap> translations = new HashMap<>();

    private final GraalVMScriptEnvironment scriptEnv;

    private final Object[] hostArgs;

    public GraalVMHostArguments(final GraalVMScriptEnvironment scriptEnv, final int parameterCount) {

        this.scriptEnv = scriptEnv;

        if (parameterCount < 0) {
            hostArgs = new Object[0];
        } else {
            hostArgs = new Object[parameterCount];
        }

    }

    public int getLength() {

        return hostArgs.length;
    }

    public Object[] getHostArgs() {

        if (hostArgs.length == 0) {
            return hostArgs;
        } else if (hostArgs.length == 1) {

            if (hostArgs[0] instanceof final GraalVMHostArguments hostArgs) {
                return hostArgs.getHostArgs();
            }

            return hostArgs;
        }

        final Object[] returnArgs = new Object[hostArgs.length];

        for (int index = 0; index < hostArgs.length; index++) {
            final Object arg = hostArgs[index];

            if (arg instanceof final GraalVMHostArguments hostArg) {
                returnArgs[index] = hostArg.getHostArgs();
            } else {
                returnArgs[index] = arg;
            }

        }

        return returnArgs;
    }

    public Class<?>[] getHostArgTypes() {

        final Object[] hostArgs = getHostArgs();
        final int length = hostArgs.length;
        final Class<?>[] hostArgTypes = new Class[length];

        for (int index = 0; index < length; index++) {
            final Object hostArg = hostArgs[index];

            if (hostArg == null) {
                hostArgTypes[index] = null;
            } else if (hostArg.getClass().isArray()) {
                hostArgTypes[index] = hostArg.getClass().arrayType();
            } else {
                hostArgTypes[index] = hostArg.getClass();
            }

        }

        return hostArgTypes;
    }

    public Map<Integer, TranslationMap> getTranslations() {

        return translations;
    }

    public void addImmutableArg(final int index, final Object arg) {

        validateIndex(index, arg);

        if (arg instanceof final GraalVMHostArguments args) {
            hostArgs[index] = args.hostArgs;
        } else {
            hostArgs[index] = arg;
        }

    }

    public void addMutableArg(final int index, final Object arg, final Value originalArg) {

        addImmutableArg(index, arg);

        translations.put(index, new TranslationMap(arg, originalArg));
    }

    public void doTranslations() {

        translations.values().forEach(this::translate);
    }

    void translate(final TranslationMap transMap) {

        final Object arg = transMap.getArg();
        final Value origArg = transMap.getValue();

        if (arg == null) {

            if (origArg == null) {
                return;
            }

            throw new GraalVMRuntimeException("Could not translate Host Argument back to Guest Argument as Host Argument is NULL");
        }

        if (arg instanceof final GraalVMHostArguments varArgs && origArg == null) {
            translateVarArg(varArgs);
        } else {
            translateFromHostToGuest(arg, origArg);
        }

    }

    void translateVarArg(final GraalVMHostArguments varArgs) {

        varArgs.getTranslations().values().forEach(this::translate);
    }

    void translateFromHostToGuest(final Object arg, final Value origArg) {

        if (arg instanceof final Map argMap) {

            for (final Object key : argMap.keySet()) {
                final Object hostArg = argMap.get(key);
                final Object guestValue = hostToGuestUtil.createGuestLanguageObject(scriptEnv, hostArg);
                origArg.putMember(key.toString(), guestValue);
            }

        } else if (arg.getClass().isArray() && origArg.hasArrayElements()) {

            final Object[] hostArgs = (Object[]) arg;

            for (int index = 0; index < hostArgs.length; index++) {
                final Object hostArg = hostArgs[index];
                final Object guestValue = hostToGuestUtil.createGuestLanguageObject(scriptEnv, hostArg);
                origArg.setArrayElement(index, guestValue);
            }

        }

    }

    void validateIndex(final int index, final Object arg) {

        if (index >= hostArgs.length) {
            String argType;

            if (arg == null) {
                argType = "NULL Arg";
            } else {
                argType = arg.getClass().getName();
            }

            final String message = "Tried to add an argument to an index that is out of bounds. " +
                    "Parameter Count: " + hostArgs.length + ", Index: " + index + ", Arg Type: " + argType;

            throw new GraalVMRuntimeException(message);
        }

    }

    @SuppressWarnings("InnerClassMayBeStatic")
    static class TranslationMap {

        private final Object gArg;
        private final Value gValue;

        public TranslationMap(final Object arg, final Value value) {

            gArg = arg;
            gValue = value;
        }

        public Object getArg() {

            return gArg;
        }

        public Value getValue() {

            return gValue;
        }

    }

}