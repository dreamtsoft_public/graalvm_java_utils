/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import java.lang.reflect.Method;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.graalvm.GraalVMScriptEnvironment;
import org.graalvm.GraalVMWrappedObject;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.Proxy;
import org.graalvm.polyglot.proxy.ProxyDate;
import org.graalvm.polyglot.proxy.ProxyDuration;
import org.graalvm.polyglot.proxy.ProxyInstant;
import org.graalvm.polyglot.proxy.ProxyObject;
import org.graalvm.polyglot.proxy.ProxyTime;
import org.graalvm.polyglot.proxy.ProxyTimeZone;
import org.graalvm.proxy.GraalVMProxyExecutable;
import org.graalvm.proxy.GraalVMProxyObject;

@SuppressWarnings("unchecked")
public class GraalVMHostToGuestLanguageUtil {

    public Object[] createGuestLanguageObjects(final GraalVMScriptEnvironment scriptEnv, final Object... args) {

        if (ArrayUtils.isEmpty(args)) {
            return new Object[0];
        }

        final List<Object> values = new ArrayList<>();

        for (final Object arg : args) {
            values.add(createGuestLanguageObject(scriptEnv, arg));
        }

        return values.toArray();
    }

    public Object createGuestLanguageObject(final GraalVMScriptEnvironment scriptEnv, final Object arg) {

        if (arg == null) {
            return scriptEnv.createUndefined();
        }

        Object guestObject;

        final Value value = getValue(arg);

        if (value == null) {

            if (arg.getClass().isArray()) {
                guestObject = createArrayOfGuestLanguage(scriptEnv, (Object[]) arg);
            } else if (arg instanceof final ProxyObject proxyObject) {
                guestObject = new GraalVMProxyObject(scriptEnv, proxyObject);
            } else if (arg instanceof final Proxy proxy) {
                guestObject = proxy;
            } else if (arg instanceof final Iterator iterator) {
                guestObject = createIteratorOfGuestLanguage(scriptEnv, iterator);
            } else if (arg instanceof final Iterable iterable) {
                guestObject = createIterableOfGuestLanguage(scriptEnv, iterable);
            } else if (arg instanceof final Map map) {
                guestObject = createMapOfGuestLanguage(scriptEnv, map);
            } else {
                guestObject = getGraalVMHandledObject(arg);
            }

        } else {
            guestObject = value;
        }

        return guestObject;
    }

    public Object createArrayOfGuestLanguage(final GraalVMScriptEnvironment scriptEnv, final Object[] args) {

        if (ArrayUtils.isEmpty(args)) {
            return new Object[0];
        }

        final Value returnArray = createJavascriptFunctionClass(scriptEnv, GraalVMScriptConstants.ARRAY_CLASS_NAME);

        for (int index = 0; index < args.length; index++) {
            final Object entry = args[index];
            returnArray.setArrayElement(index, createGuestLanguageObject(scriptEnv, entry));
        }

        return returnArray;
    }

    public Object createIteratorOfGuestLanguage(final GraalVMScriptEnvironment scriptEnv, final Iterator<?> iterator) {

        final Value returnArray = createJavascriptFunctionClass(scriptEnv, GraalVMScriptConstants.ARRAY_CLASS_NAME);

        int index = 0;

        while (iterator.hasNext()) {
            final Object entry = iterator.next();
            returnArray.setArrayElement(index, createGuestLanguageObject(scriptEnv, entry));
            index++;
        }

        return returnArray;
    }

    public Object createIterableOfGuestLanguage(final GraalVMScriptEnvironment scriptEnv, final Iterable<?> iterable) {

        final Value returnArray = createJavascriptFunctionClass(scriptEnv, GraalVMScriptConstants.ARRAY_CLASS_NAME);

        int index = 0;

        for (final Object entry : iterable) {
            returnArray.setArrayElement(index, createGuestLanguageObject(scriptEnv, entry));
            index++;
        }

        return returnArray;
    }

    public Object createMapOfGuestLanguage(final GraalVMScriptEnvironment scriptEnv, final Map<Object, Object> map) {

        // GraalVM treats Java Maps as Java Maps and not JavaScript Maps.
        // The Legacy code uses JavaScript Objects as Maps, instead of JavaScript Maps.
        // We are defaulting ot JavaScript Objects as Maps
        // Read 'Objects vs Maps' at https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map
        final Value returnMap = createJavascriptFunctionClass(scriptEnv, GraalVMScriptConstants.OBJECT_CLASS_NAME);

        for (final Object key : map.keySet()) {
            final Object value = createGuestLanguageObject(scriptEnv, map.get(key));
            returnMap.putMember(key.toString(), value);
        }

        return returnMap;
    }

    public Object getGraalVMHandledObject(final Object hostObj) {

        return switch (hostObj) {
            // The Case is basing it off of the rules for Context.asValue()
            // https://www.graalvm.org/sdk/javadoc/org/graalvm/polyglot/Context.html#asValue-java.lang.Object-

            // #2
            case Value v -> v;
            // #3
            case Byte b -> b;
            case Short s -> s;
            case Integer i -> i;
            case Long l -> l;
            case Float f -> f;
            case Double d -> d;
            // #4
            case Character c -> c;
            case String s -> s;
            // #5
            case Boolean b -> b;
            // #6
            case LocalTime lt -> ProxyTime.from(lt);
            // #7
            case LocalDate ld -> ProxyDate.from(ld);
            case Date d -> ProxyDate.from(LocalDate.from(d.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));
            // #8
            case ZoneId zi -> ProxyTimeZone.from(zi);
            // #9
            case Instant i -> ProxyInstant.from(i);
            case LocalDateTime ldt -> ProxyInstant.from(ldt.toInstant(ZoneOffset.UTC));
            case ZonedDateTime zdt -> ProxyInstant.from(zdt.toInstant());
            // #10
            case Duration d -> ProxyDuration.from(d);
            // #11
            case Proxy p -> p;
            // #12 & #13
            // TODO: We should probably refactor this to be a "Get Guest Language Object" and not just a toString()
            default -> getObjectString(hostObj);
        };

    }

    public GraalVMProxyExecutable getExecutable(final GraalVMScriptEnvironment scriptEnvironment, final Object hostObject, final Method method) {

        return new GraalVMProxyExecutable(scriptEnvironment, hostObject, method);
    }

    public Value getValue(final GraalVMScriptEnvironment scriptEnv, final Object obj) {

        Value value = getValue(obj);

        if (value == null) {
            value = scriptEnv.getContext().asValue(obj);
        }

        return value;
    }

    public Value getValue(final Object obj) {

        Value value = null;

        if (obj instanceof final GraalVMWrappedObject wrappedObject) {
            value = wrappedObject.getValue();
        } else if (obj instanceof final Value objValue) {
            value = objValue;
        }

        return value;
    }

    public Value createGuestLanguageObjectClass(final GraalVMScriptEnvironment scriptEnv, final Object... args) {

        return createJavascriptFunctionClass(scriptEnv, GraalVMScriptConstants.OBJECT_CLASS_NAME, args);
    }

    public Value createJavascriptFunctionClass(final GraalVMScriptEnvironment scriptEnv, final String name, final Object... args) {

        final Value value = createJavascriptMemberClass(scriptEnv, name);
        Value newFunction;

        if (ArrayUtils.isEmpty(args)) {
            newFunction = value.newInstance();
        } else {
            newFunction = value.newInstance(args);
        }

        return newFunction;
    }

    public Value createJavascriptMemberClass(final GraalVMScriptEnvironment scriptEnv, final String name) {

        return scriptEnv.getBindings().getMember(name);
    }

    String getObjectString(final Object obj) {

        if (obj != null) {
            return obj.toString();
        }

        return null;
    }

}