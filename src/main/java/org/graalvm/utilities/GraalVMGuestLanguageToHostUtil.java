/*
Copyright 2022 Dreamtsoft, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.graalvm.GraalVMRuntimeException;
import org.graalvm.GraalVMScriptEnvironment;
import org.graalvm.GraalVMWrappedObject;
import org.graalvm.polyglot.Value;

public class GraalVMGuestLanguageToHostUtil {

    public static final String STATIC_MEMBER_NAME = "static";

    private final GraalVMValueUtil valueUtil = new GraalVMValueUtil();

    Object[] getReturnObjects(final GraalVMScriptEnvironment scriptEnv, final Object[] objArray) {

        final int length = objArray.length;
        final Object[] returnObjArray = new Object[length];

        for (int index = 0; index < length; index++) {
            final Object valueObj = objArray[index];
            returnObjArray[index] = getReturnObject(scriptEnv, valueObj, valueObj);
        }

        return returnObjArray;
    }

    Object getReturnObject(final GraalVMScriptEnvironment scriptEnv, final Object valueObj, final Object valueObj1) {

        if (valueObj instanceof final Value value) {
            return getHostValue(scriptEnv, value);
        } else if (valueObj instanceof final GraalVMWrappedObject wrappedObject) {
            return getHostValue(scriptEnv, wrappedObject.getValue());
        }

        return valueObj1;
    }

    public Object[] getHostObjects(final GraalVMScriptEnvironment scriptEnv, final Object... hostObjs) {

        if (ArrayUtils.isEmpty(hostObjs)) {
            return new Object[0];
        }

        final int len = hostObjs.length;
        final Object[] returnObjArray = new Object[len];

        for (int index = 0; index < len; index++) {
            returnObjArray[index] = getHostObject(scriptEnv, hostObjs[index]);
        }

        return returnObjArray;
    }

    public Object getHostObject(final GraalVMScriptEnvironment scriptEnv, final Object obj) {

        if (obj == null) {
            return null;
        }

        Object unwrappedObject = getUnwrappedObject(scriptEnv, obj);

        if (unwrappedObject != null && unwrappedObject.getClass().isArray()) {
            unwrappedObject = Arrays.asList((Object[]) unwrappedObject);
        }

        return unwrappedObject;
    }

    public Object getHostValue(final GraalVMScriptEnvironment scriptEnv, final Value value) {

        Object returnObj;

        if (valueUtil.isValueNullOrUndefined(value)) {
            returnObj = null;
        } else if (value.isProxyObject()) {
            returnObj = value.asProxyObject();
        } else if (value.isHostObject()) {
            returnObj = getHostObjectFromValue(scriptEnv, value);
        } else if (value.isMetaObject()) {

            if (value.isHostObject()) {
                returnObj = value.asHostObject();
            } else if (value.canExecute()) {
                returnObj = new GraalVMWrappedObject(scriptEnv, value);
            } else {
                returnObj = null;
            }

        } else if (value.isBoolean()) {
            returnObj = value.asBoolean();
        } else if (value.isNumber()) {
            returnObj = getNumber(value);
        } else if (value.isString()) {
            returnObj = getString(value.asString());
        } else if (value.isDate()) {
            returnObj = value.asDate();
        } else if (value.isTime()) {
            returnObj = value.asTime();
        } else if (value.isDuration()) {
            returnObj = value.asDuration();
        } else if (value.isInstant()) {
            returnObj = value.asInstant();
        } else if (value.isTimeZone()) {
            returnObj = value.asTimeZone();
        } else if (value.isIterator()) {
            returnObj = getIterator(scriptEnv, value);
        } else if (value.hasIterator()) {
            returnObj = getIterable(scriptEnv, value);
        } else if (value.hasArrayElements()) {
            returnObj = getArray(scriptEnv, value);
        } else if (value.hasHashEntries()) {
            returnObj = getMap(scriptEnv, value);
        } else if (value.hasMembers()) {
            returnObj = getGuestLanguageObject(scriptEnv, value);
        } else if (value.isException()) {
            returnObj = value.throwException();
        } else if (value.hasBufferElements()) {

            if (value.isBufferWritable()) {
                throw new GraalVMRuntimeException("GraalVM Value Has Buffered Elements and is Writable.");
            } else {
                throw new GraalVMRuntimeException("GraalVM Value Has Buffered Elements and is NOT Writable.");
            }

        } else if (value.canExecute()) {
            returnObj = new GraalVMWrappedObject(scriptEnv, value);
        } else {
            returnObj = null;
        }

        if (returnObj == null && value != null && !value.isNull()) {
            returnObj = getString(value.toString());
        }

        // We are ignoring the following attributes on a Value.  They are not needed here:
        // canInstantiate(); <-- This is functionality and not type.
        // isNativePointer() <-- Returns the Pointer Value. This is used with languages that support Pointers (ex - C++).

        return returnObj;
    }

    public Object getHostObjectFromValue(final GraalVMScriptEnvironment scriptEnv, final Value value) {

        if (valueUtil.isValueNullOrUndefined(value)) {
            return null;
        } else if (!value.isHostObject()) {
            return null;
        }

        final Object hostObject = value.asHostObject();

        return getHostObj(scriptEnv, hostObject);
    }

    /*
     *  TODO: This method assumes the Host Language is Java, and the Guest Language is JavaScript.
     *  TODO: If this changes, then this method will need to be redesigned to accomodate how other
     *        languages handle numbers.
     *
     *
     *  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#number_type
     *  The Number type is a double-precision 64-bit binary format IEEE 754 value
     *  [https://en.wikipedia.org/wiki/Double-precision_floating-point_format]
     *  (numbers between -(2^53 − 1) and 2^53 − 1). In addition to representing floating-point numbers,
     *  the number type has three symbolic values: +Infinity, -Infinity, and NaN ("Not a Number").
     *
     *  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#bigint_type
     *  The BigInt type is a numeric primitive in JavaScript that can represent integers with arbitrary
     *  precision. With BigInts, you can safely store and operate on large integers even beyond the safe
     *  integer limit for Numbers.
     *
     *  Because of the documentation above, we will look for Integers first, then Doubles.
     *  Essentially, GraalVM wil use those number types as defaults.
     *  GraalVM will check if the number is less than Integer.MAX_VALUE;
     *  AND
     *  the number is a whole number, or the number has a decimal followed only by zeros.
     *  If this is the case, then GraalVM will store the number as an Integer.
     *  If this is NOT the case, then GraalVM will store the number as a Double.
     *  We want to start with 'can the number be an Integer'?
     *  If so, use it.
     *  If not, use Double by default.
     *
     *  It is possible to have Short, Long, Float, BigInteger, BigDecimal.
     *  However, those will have Meta Data that states those types.
     *  The Meta Data will determine what Type of Number to use.
     */
    public Object getNumber(final Value value) {

        if (!value.isNumber()) {
            return null;
        }

        final Class<?> numberClass = getHostValueClass(value);

        Object returnObj;

        if (numberClass == null) {
            returnObj = getDefaultNumber(value);
        } else if (Byte.class.equals(numberClass)) {
            returnObj = value.asByte();
        } else if (Short.class.equals(numberClass)) {
            returnObj = value.asShort();
        } else if (Integer.class.equals(numberClass)) {
            returnObj = value.asInt();
        } else if (Long.class.equals(numberClass)) {
            returnObj = value.asLong();
        } else if (Float.class.equals(numberClass)) {
            returnObj = value.asFloat();
        } else if (Double.class.equals(numberClass)) {
            returnObj = value.asDouble();
        } else {
            returnObj = getDefaultNumber(value);
        }

        return returnObj;
    }

    /*
     * See comments for getNumber(Value) for what a Default Number is and why.
     */
    public Number getDefaultNumber(final Value value) {

        Number returnObj;

        if (value == null) {
            returnObj = null;
        } else if (!value.isNumber()) {
            returnObj = null;
        } else if (value.fitsInInt()) {
            returnObj = value.asInt();
        } else {
            returnObj = value.asDouble();
        }

        return returnObj;
    }

    /*
     * This method looks to see if the Host Class implements java.lang.Iterable.
     * https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/Iterable.html
     *
     * Iterable Objects are Lists, Sets, Queues, etc. If Value is Null or Undefined, OR does
     * not implement Iterable; then an Empty ArrayList will be returned.
     *
     * This is different from the getIterator(Value) method because this deals with the Iterable Interface.
     * The getIterator(Value) method deals with either the java.lang.Iterator, or a JavaScript Iterator.
     * See the comments for getIterator(Value) for more.
     *
     */
    public Iterable<?> getIterable(final GraalVMScriptEnvironment scriptEnv, final Value value) {

        Iterable<?> returnObj;

        if (valueUtil.isValueNullOrUndefined(value)) {
            returnObj = new ArrayList<>();
        } else if (value.hasIterator()) {
            final Value iteratorValue = value.getIterator();
            final Iterator<?> iterator = getIterator(scriptEnv, iteratorValue);

            returnObj = IteratorUtils.toList(iterator);
        } else if (value.isHostObject()) {
            final Object objValue = value.asHostObject();

            if (objValue instanceof final Iterable iterable) {
                returnObj = iterable;
            } else {
                returnObj = new ArrayList<>();
            }

        } else {
            returnObj = new ArrayList<>();
        }

        return returnObj;
    }

    /*
     *
     * Java already has a specific Iterator Object.  This method will look for that first.
     *
     * If it is a JavaScript Iterator, it must follow the specification below:
     *
     * Specifically, an iterator is any object which implements the Iterator protocol by having a next() method that returns an object with two properties:
     * -- value: The next value in the iteration sequence.
     * -- done" This is true if the last value in the sequence has already been consumed. If value is present alongside done, it is the iterator's return value.
     * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_Generators
     *
     */
    public Iterator<?> getIterator(final GraalVMScriptEnvironment scriptEnv, final Value value) {

        final ArrayList<Object> iteratorList = new ArrayList<>();

        if (valueUtil.isValueNotNullOrUndefined(value)) {

            if (value.isIterator()) {
                addHostObjectsFromIterator(scriptEnv, value, iteratorList);
            } else if (value.hasMembers()) {
                addGuestElementsFromIterator(scriptEnv, value, iteratorList);
            }

        }

        return iteratorList.iterator();
    }

    public Object[] getArray(final GraalVMScriptEnvironment scriptEnv, final Value value) {

        Object[] returnObj;

        if (valueUtil.isValueNullOrUndefined(value)) {
            returnObj = null;
        } else if (!value.hasArrayElements()) {
            returnObj = null;
        } else {

            final long size = value.getArraySize();
            final ArrayList<Object> arrayList = new ArrayList<>();

            for (long index = 0; index < size; index++) {
                final Value arrayValue = value.getArrayElement(index);
                arrayList.add(getHostValue(scriptEnv, arrayValue));
            }

            returnObj = arrayList.toArray();
        }

        return returnObj;
    }


    public Object getGuestLanguageObject(final GraalVMScriptEnvironment scriptEnv, final Value value) {

        Object returnObj;

        if (valueUtil.isValueNullOrUndefined(value)) {
            returnObj = null;
        } else if (value.isMetaObject()) {

            if (value.isHostObject()) {
                returnObj = value.asHostObject();
            } else {
                returnObj = null;
            }

        } else {
            final Value metaValue = value.getMetaObject();

            if (metaValue != null && CollectionUtils.isNotEmpty(metaValue.getMemberKeys()) && (metaValue.hasMember(STATIC_MEMBER_NAME))) {
                returnObj = value.asHostObject();
            } else {
                returnObj = getMap(scriptEnv, value);
            }

        }

        return returnObj;
    }

    public Map<String, Object> getMap(final GraalVMScriptEnvironment scriptEnv, final Value value) {

        final Map<String, Object> valueMap = new HashMap<>();

        if (valueUtil.isValueNotNullOrUndefined(value)) {

            if (value.hasHashEntries()) {
                setHashEntriesInMap(scriptEnv, value, valueMap);
            } else if (value.hasMembers()) {
                setMembersInMap(scriptEnv, value, valueMap);
            }

        }

        return valueMap;
    }

    public void setHashEntriesInMap(final GraalVMScriptEnvironment scriptEnv, final Value value, final Map<String, Object> valueMap) {

        if (valueUtil.isValueNotNullOrUndefined(value) && value.hasHashEntries()) {
            final Value keysValue = value.getHashKeysIterator();

            while (keysValue.hasIteratorNextElement()) {
                final Value hashKey = keysValue.getIteratorNextElement();
                final Value hashValue = value.getHashValue(hashKey);

                final String key = hashKey.toString();
                final Object keyValue = getHostValue(scriptEnv, hashValue);

                valueMap.put(key, keyValue);
            }

        }

    }

    public void setMembersInMap(final GraalVMScriptEnvironment scriptEnv, final Value value, final Map<String, Object> valueMap) {

        if (valueUtil.isValueNotNullOrUndefined(value)) {
            final Set<String> memberKeys = value.getMemberKeys();

            if (value.hasMembers() && CollectionUtils.isNotEmpty(memberKeys)) {

                for (String key : memberKeys) {
                    final Value memberValue = value.getMember(key);
                    final Object keyValue = getHostValue(scriptEnv, memberValue);

                    valueMap.put(key, keyValue);
                }

            }

        }

    }

    /*
     * There is a special Member named 'static' that has the static reference to the java.lang.Class that the object is stored as.
     */
    public Class<?> getHostValueClass(final Value value) {

        Class<?> clazz;

        if (valueUtil.isValueNullOrUndefined(value)) {
            clazz = null;
        } else {

            final Value metaValue = value.getMetaObject();

            if (valueUtil.isValueNullOrUndefined(metaValue)) {
                clazz = null;
            } else if (CollectionUtils.isNotEmpty(metaValue.getMemberKeys()) && (metaValue.hasMember(STATIC_MEMBER_NAME))) {
                clazz = metaValue.getMember(STATIC_MEMBER_NAME).asHostObject();
            } else {
                clazz = null;
            }

        }

        return clazz;
    }

    public Object getUnwrappedObject(final GraalVMScriptEnvironment scriptEnv, final Object obj) {

        if (obj == null) {
            return null;
        }

        if (obj.getClass().isArray()) {
            final Object[] objArray = (Object[]) obj;

            return getReturnObjects(scriptEnv, objArray);
        }

        Object returnObj = obj;

        if (returnObj instanceof final Value value) {
            returnObj = getHostValue(scriptEnv, value);
        } else if (returnObj instanceof final GraalVMWrappedObject wrappedObject) {
            returnObj = getHostValue(scriptEnv, wrappedObject.getValue());
        }

        return returnObj;
    }


    @SuppressWarnings("unchecked")
    Object getHostObj(final GraalVMScriptEnvironment scriptEnv, final Object object) {

        if (object instanceof final Map map) {
            return getObjectMap(scriptEnv, map);
        }

        if (object instanceof final List list) {
            return getObjectList(scriptEnv, list);
        }

        if (object instanceof final Iterator iterator) {
            return getObjectIterator(scriptEnv, iterator);
        }

        return object;
    }

    Map<Object, Object> getObjectMap(final GraalVMScriptEnvironment scriptEnv, final Map<Object, Object> map) {

        for (final Object key : map.keySet()) {
            final Object mapValue = map.get(key);
            Object newMapValue;

            if (mapValue instanceof final Value keyValue) {
                newMapValue = getHostValue(scriptEnv, keyValue);
            } else {
                newMapValue = getHostObj(scriptEnv, mapValue);
            }

            map.put(key, newMapValue);
        }

        return map;
    }

    List<Object> getObjectList(final GraalVMScriptEnvironment scriptEnv, final List<?> list) {

        final List<Object> returnList = new ArrayList<>();

        for (final Object entry : list) {
            Object newEntry;

            if (entry instanceof final Value entryValue) {
                newEntry = getHostValue(scriptEnv, entryValue);
            } else {
                newEntry = getHostObj(scriptEnv, entry);
            }

            returnList.add(newEntry);
        }

        return returnList;
    }

    Iterator<Object> getObjectIterator(final GraalVMScriptEnvironment scriptEnv, final Iterator<?> iterator) {

        final List<Object> returnList = new ArrayList<>();

        while (iterator.hasNext()) {
            Object entry = iterator.next();

            if (entry instanceof final Value entryValue) {
                entry = getHostValue(scriptEnv, entryValue);
            } else {
                entry = getHostObj(scriptEnv, entry);
            }

            returnList.add(entry);
        }

        return returnList.iterator();
    }

    void addHostObjectsFromIterator(final GraalVMScriptEnvironment scriptEnv, final Value value, final ArrayList<Object> iteratorList) {

        while (value.hasIteratorNextElement()) {
            final Value iteratorValue = value.getIteratorNextElement();
            final Object iteratorHostObject = getHostValue(scriptEnv, iteratorValue);

            // We are allowing for the Host Object to just be NULL.
            iteratorList.add(iteratorHostObject);
        }

    }

    void addGuestElementsFromIterator(final GraalVMScriptEnvironment scriptEnv, final Value value, final ArrayList<Object> iteratorList) {

        if (value.hasMembers() && value.hasMember("next")) {
            final Value next = value.getMember("next");

            while (next.canExecute()) {
                final Value nextValue = next.execute();

                if (nextValue.hasMember("value") && nextValue.hasMember("done")) {
                    final Value memberValue = nextValue.getMember("value");
                    final Value done = nextValue.getMember("done");

                    if (done.isBoolean()) {

                        if (done.asBoolean()) {
                            return;
                        }

                        iteratorList.add(getHostValue(scriptEnv, memberValue));
                    } else {
                        // This means we do not have an Iterator!
                        return;
                    }

                }

            }

        }

    }

    String getString(final String valueString) {

        if (valueString == null) {
            return null;
        }

        if (StringUtils.equalsIgnoreCase(valueString, "null")) {
            return null;
        }

        return valueString;
    }

}