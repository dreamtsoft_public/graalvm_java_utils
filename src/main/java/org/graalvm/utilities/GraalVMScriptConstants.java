/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

public final class GraalVMScriptConstants {

    private GraalVMScriptConstants() {
        // Ensuring this class is really final
    }

    public static final String STRING_CLASS_NAME = "String";
    public static final String OBJECT_CLASS_NAME = "Object";
    public static final String ARRAY_CLASS_NAME = "Array";
    public static final String FUNCTION_CLASS_NAME = "Function";
    public static final String SCRIPT_TYPE_JS = "js";

    public static final String SCRIPT_TYPE_PYTHON = "python";

    public static final String SCRIPT_TYPE_RUBY = "ruby";

    public static final String SCRIPT_TYPE_R = "r";

}