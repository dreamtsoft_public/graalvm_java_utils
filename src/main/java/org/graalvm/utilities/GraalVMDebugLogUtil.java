/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.graalvm.polyglot.Language;
import org.graalvm.polyglot.PolyglotException;
import org.graalvm.polyglot.SourceSection;
import org.graalvm.polyglot.Value;
import org.slf4j.Logger;

public class GraalVMDebugLogUtil {

    public void logObject(final Logger logger, final Object obj) {

        if (logger.isTraceEnabled()) {

            if (obj == null) {
                traceLog(logger, "Object: Null");
            } else {
                traceLog(logger, "Object Class: ", obj.getClass().getName());

                if (obj.getClass().isArray()) {
                    traceLog(logger, "Object Array: ", ArrayUtils.toString(obj));
                } else {
                    traceLog(logger, "Object: ", obj);
                }

            }

        }

    }

    public void logObjectArray(final Logger logger, final Object... arguments) {

        if (logger.isTraceEnabled()) {

            if (ArrayUtils.isEmpty(arguments)) {
                traceLog(logger, "Argument Object Array is Empty");
            } else {
                traceLog(logger, "Argument Object Array:");
                Arrays.asList(arguments).forEach(argument -> logObject(logger, argument));
            }

        }

    }

    public void logValueArray(final Logger logger, final Value... arguments) {

        if (logger.isTraceEnabled()) {

            if (ArrayUtils.isEmpty(arguments)) {
                traceLog(logger, "Argument Value Array is Empty");
            } else {
                traceLog(logger, "Argument Value Array:");
                Arrays.asList(arguments).forEach(argument -> logValue(logger, argument));
            }

        }

    }

    public void logIterable(final Logger logger, final Iterable<?> list) {

        if (logger.isTraceEnabled()) {
            list.forEach(entry -> logObject(logger, entry));
        }

    }

    public void logIterator(final Logger logger, final Iterator<?> iterator) {

        if (logger.isTraceEnabled()) {

            while (iterator.hasNext()) {
                logObject(logger, iterator.next());
            }

        }

    }

    public void logMap(final Logger logger, final Map<?, ?> map) {

        if (logger.isTraceEnabled()) {

            for (final Map.Entry<?, ?> entry : map.entrySet()) {
                logObject(logger, "Key: {}" + entry.getKey().toString());
                logObject(logger, entry.getValue());
            }

        }

    }

    public void logValue(final Logger logger, final Value value) {

        if (logger.isTraceEnabled()) {

            if (value == null) {
                traceLog(logger, "Value: Null");
            } else {
                traceLog(logger, "Value: {}", value);
            }

        }

    }

    public void traceLog(final Logger logger, final String message, final Object... args) {

        if (logger.isTraceEnabled()) {

            final StringBuilder sb = new StringBuilder();
            sb.append(message);

            Arrays.asList(args).forEach(sb::append);

            logger.trace(sb.toString());
        }

    }

    public void debugLog(final Logger logger, final String message, final Object... args) {

        if (logger.isDebugEnabled()) {

            final StringBuilder sb = new StringBuilder();
            sb.append(message);

            Arrays.asList(args).forEach(sb::append);

            logger.debug(sb.toString());
        }

    }

    public void logPolyglotException(final Logger logger, final PolyglotException pge) {

        if (logger == null) {
            return;
        }

        if (pge == null) {
            logError(logger, "No PolyglotException", null);
            return;
        }

        if (pge.isCancelled()) {
            logError(logger, "PolyglotException is Cancelled", null);
        }

        if (pge.isExit()) {
            logError(logger, "PolyglotException is Exit", null);
        }

        if (pge.isGuestException()) {
            logError(logger, "PolyglotException is Guest Exception", null);
        }

        if (pge.isHostException()) {
            logError(logger, "PolyglotException is Host Exception", null);
        }

        if (pge.isIncompleteSource()) {
            logError(logger, "PolyglotException is Incomplete Source", null);
        }

        if (pge.isInternalError()) {
            logError(logger, "PolyglotException is Internal Error", null);
        }

        if (pge.isInterrupted()) {
            logError(logger, "PolyglotException is Interrupted", null);
        }

        if (pge.isResourceExhausted()) {
            logError(logger, "PolyglotException is Resource Exhausted", null);
        }

        if (pge.isSyntaxError()) {
            logError(logger, "PolyglotException is Syntax Error", null);
        }

        logError(logger, null, pge);

        if (logger.isDebugEnabled()) {
            final Iterable<PolyglotException.StackFrame> stack = pge.getPolyglotStackTrace();

            stack.forEach(frame -> debugStackFrame(logger, frame));
        }

    }

    public void debugStackFrame(final Logger logger, final PolyglotException.StackFrame frame) {

        if (!logger.isDebugEnabled()) {
            return;
        }

        if (frame == null) {
            debugLog(logger, "No PolyglotException.StackFrame");
            return;
        }

        if (frame.isHostFrame()) {
            debugLog(logger, "Is Host Frame");

            final StackTraceElement element = frame.toHostFrame();

            debugLog(logger, element.toString());
        }

        if (frame.isGuestFrame()) {
            debugLog(logger, "Is Guest Frame");
        }

        debugLog(logger, "Root Name: ", frame.getRootName());

        displaySourceSection(logger, frame.getSourceLocation());
        displayLanguage(logger, frame.getLanguage());
    }

    public void displaySourceSection(final Logger logger, final SourceSection sourceSection) {

        if (!logger.isDebugEnabled()) {
            return;
        }

        if (sourceSection == null) {
            debugLog(logger, "No Source Section");
            return;
        }

        final StringBuilder sourceSb = new StringBuilder();

        sourceSb.append("Is Available: ").append(sourceSection.isAvailable()).append(System.lineSeparator())
                .append("Has Lines: ").append(sourceSection.hasLines()).append(System.lineSeparator());

        if (sourceSection.hasLines()) {
            sourceSb.append("- Start Line: ").append(sourceSection.getStartLine()).append(System.lineSeparator())
                    .append("- End Line: ").append(sourceSection.getEndLine()).append(System.lineSeparator());
        }

        sourceSb.append("Has Columns: ").append(sourceSection.hasColumns()).append(System.lineSeparator());

        if (sourceSection.hasColumns()) {
            sourceSb.append("- Start Column: ").append(sourceSection.getStartColumn()).append(System.lineSeparator())
                    .append("- End Column: ").append(sourceSection.getEndColumn()).append(System.lineSeparator());
        }

        sourceSb.append("Has Char Index: ").append(sourceSection.hasCharIndex()).append(System.lineSeparator());

        if (sourceSection.hasCharIndex()) {
            sourceSb.append("- Start Char Index: ").append(sourceSection.getCharIndex()).append(System.lineSeparator())
                    .append("- End Char Index: ").append(sourceSection.getCharEndIndex()).append(System.lineSeparator());
        }

        sourceSb.append("Char Length: ").append(sourceSection.getCharLength()).append(System.lineSeparator())
                .append("Characters: ").append(sourceSection.getCharacters()).append(System.lineSeparator());

        final String message = sourceSb.toString();

        debugLog(logger, message);
    }

    public void displayLanguage(final Logger logger, final Language lang) {

        if (!logger.isDebugEnabled()) {
            return;
        }

        if (lang == null) {
            debugLog(logger, "No Language");
            return;
        }

        final String message = "ID: " + lang.getId() + System.lineSeparator() +
                "Name: " + lang.getName() + System.lineSeparator() +
                "Implementation Name: " + lang.getImplementationName() + System.lineSeparator() +
                "Version: " + lang.getVersion() + System.lineSeparator() +
                "Is Interactive: " + lang.isInteractive() + System.lineSeparator() +
                "Default Mime Type: " + lang.getDefaultMimeType() + System.lineSeparator() +
                "Mime Types: " + System.lineSeparator();

        debugLog(logger, message);
    }

    public void logError(final Logger logger, final String error, final Exception exception) {

        if (logger == null) {
            return;
        }

        if (StringUtils.isBlank(error) && exception == null) {
            logger.error("No Error Message or Exception passed in");
        } else if (StringUtils.isBlank(error) && exception != null) {
            logger.error(exception.getMessage(), exception);
        } else if (StringUtils.isNotBlank(error) && exception == null) {
            logger.error(error);
        } else {
            logger.error(error, exception);
        }

    }

    public void displayValue(final Logger logger, final Value value) {

        if (value == null) {
            logger.info(" Value is NULL");
            return;
        }

        if (value.isProxyObject()) {
            logger.info(" Value is a Proxy Object");
        }
        if (value.isHostObject()) {
            logger.info(" Value is a Host Object");
        }

        if (value.isBoolean()) {
            logger.info(" Value is a Boolean");
        }

        if (value.isNumber()) {
            logger.info(" Value is a Number");
        }

        if (value.isString()) {
            logger.info(" Value is a String");
        }

        if (value.isDate()) {
            logger.info(" Value is a Date");
        }

        if (value.isDuration()) {
            logger.info(" Value is a Duration");
        }

        if (value.isInstant()) {
            logger.info(" Value is an Instant");
        }

        if (value.isTime()) {
            logger.info(" Value is a Time");
        }

        if (value.isTimeZone()) {
            logger.info(" Value is a TimeZone");
        }

        if (value.isNativePointer()) {
            logger.info(" Value is a Native Pointer");
        }

        if (value.hasIterator()) {
            logger.info(" Value has an Object that implements Iterable");
        }

        if (value.isIterator()) {
            logger.info(" Value is an Iterator");
        }

        if (value.hasArrayElements()) {
            logger.info(" Value has Array Elements");
        }

        if (value.hasHashEntries()) {
            logger.info(" Value has Hash Entries");
        }

        if (value.hasMembers()) {
            logger.info(" Value has Members");
        }

        if (value.hasBufferElements()) {
            logger.info(" Value has Buffer Elements");

            if (value.isBufferWritable()) {
                logger.info(" Value is a Buffer Writable");
            }

        }

        if (value.isException()) {
            logger.info(" Value is an Exception");
        }

        if (value.isMetaObject()) {
            logger.info(" Value is a Meta Object");
        }

        if (value.canInstantiate()) {
            logger.info(" Value can be Instantiated");
        }

        if (value.canExecute()) {
            logger.info(" Value can be Executed");
        }

    }

}