/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class GraalVMValueUtil {

    private final Logger logger = LoggerFactory.getLogger(GraalVMValueUtil.class);

    public Value createUndefined(final Context context) {

        return context.asValue(null);
    }

    public void setMember(final Value value, final String name, final Object obj) {

        //noinspection StatementWithEmptyBody
        if (isValueNullOrUndefined(value)) {
            // Do Nothing - IntelliJ doesn't understand Value could be NULL and a NullPointerException will be thrown in the 'else if'
        } else if (value.hasHashEntries()) {
            value.putHashEntry(name, obj);
        } else if (value.hasMembers()) {

            try {
                value.putMember(name, obj);
            } catch (final Exception e) {
                // https://www.graalvm.org/sdk/javadoc/org/graalvm/polyglot/Value.html#putMember-java.lang.String-java.lang.Object-
                logger.info("Could not add member: {} - Message: {}", name, e.getMessage());
                logger.debug(e.getMessage(), e);
            }

        }

    }

    public Value getMember(final Value value, final String name) {

        Value returnObj = null;

        if (isValueNullOrUndefined(value)) {
            // IntelliJ doesn't understand that the value could be NULL and throw a NullPointerException in the 'else if'
            //noinspection ConstantConditions
            returnObj = null;
        } else if (value.hasMembers() && value.hasMember(name)) {
            returnObj = value.getMember(name);
        } else if (value.hasHashEntries()) {
            returnObj = value.getHashValue(name);
        }

        return returnObj;
    }

    public boolean hasMember(final Value value, final String name) {

        boolean hasMem;

        if (isValueNullOrUndefined(value)) {
            hasMem = false;
        } else if (value.hasHashEntries()) {
            hasMem = false;
            final Value iterator = value.getHashKeysIterator();

            while (iterator.hasIteratorNextElement()) {
                final Value hashKey = iterator.getIteratorNextElement();

                if (StringUtils.equals(name, hashKey.asString())) {
                    hasMem = true;
                }

            }

        } else if (value.hasMembers()) {
            hasMem = value.hasMember(name);
        } else {
            hasMem = false;
        }

        return hasMem;
    }

    public boolean hasFunction(final Value value, final String name) {

        boolean hasFunc;

        if (isValueNullOrUndefined(value)) {
            hasFunc = false;
        } else if (StringUtils.isBlank(name)) {
            hasFunc = false;
        } else if (value.hasMembers() && value.hasMember(name)) {
            hasFunc = isFunction(value.getMember(name));
        } else {
            hasFunc = false;
        }

        return hasFunc;
    }

    public boolean isFunction(final Value value) {

        boolean isFunc;

        if (isValueNullOrUndefined(value)) {
            isFunc = false;
        } else {
            isFunc = value.canExecute();
        }

        return isFunc;
    }

    public boolean isObject(Value value) {

        boolean isObj;

        if (isValueNullOrUndefined(value)) {
            isObj = false;
        } else if (value.isHostObject()) {
            isObj = true;
        } else if (value.canInstantiate()) {
            isObj = true;
        } else {
            isObj = isGuestMap(value);
        }

        return isObj;
    }

    public boolean isMap(final Value value) {

        boolean isMap;

        if (isValueNullOrUndefined(value)) {
            isMap = false;
        } else if (isHostMap(value)) {
            isMap = true;
        } else {
            isMap = isGuestMap(value);
        }

        return isMap;
    }

    public boolean isHostMap(final Value value) {

        boolean isHostMap;

        if (isValueNullOrUndefined(value)) {
            isHostMap = false;
        } else if (value.isHostObject()) {
            final Object obj = value.asHostObject();
            isHostMap = obj instanceof Map;
        } else {
            isHostMap = value.hasHashEntries();
        }

        return isHostMap;
    }

    public boolean isGuestMap(final Value value) {

        boolean isGuestMap;

        if (isValueNullOrUndefined(value)) {
            isGuestMap = false;
        } else if (isArray(value)) {
            isGuestMap = false;
        } else if (value.hasMembers()) {
            final Value metaObject = value.getMetaObject();

            if (metaObject != null && metaObject.hasMembers()) {
                isGuestMap = CollectionUtils.isEmpty(metaObject.getMemberKeys());
            } else {
                isGuestMap = false;
            }

        } else {
            isGuestMap = value.hasHashEntries();
        }

        return isGuestMap;
    }

    public boolean isArray(final Value value) {

        if (value == null) {
            return false;
        }

        boolean isArray;

        isArray = value.hasArrayElements();

        if (!isArray && value.isHostObject()) {
            final Object obj = value.asHostObject();
            isArray = obj.getClass().isArray();
        }

        return isArray;
    }

    public boolean isValueNotNullOrUndefined(final Value value) {

        return !isValueNullOrUndefined(value);
    }

    public boolean isValueNullOrUndefined(final Value value) {

        if (value == null) {
            return true;
        }

        return value.isNull();
    }

}