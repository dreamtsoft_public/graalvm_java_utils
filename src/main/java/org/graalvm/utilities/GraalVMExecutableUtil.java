/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import org.apache.commons.lang3.ArrayUtils;
import org.graalvm.GraalVMScriptEnvironment;
import org.graalvm.polyglot.Value;

public class GraalVMExecutableUtil {

    private final GraalVMHostToGuestLanguageUtil hostToGuestUtil = new GraalVMHostToGuestLanguageUtil();

    public Value executeOrInstantiateValue(final GraalVMScriptEnvironment scriptEnv, final Value value, final Object... args) {

        Value result = executeValue(scriptEnv, value, args);

        if (result == null) {
            result = instantiateValue(scriptEnv, value, args);
        }

        return result;
    }

    public Value executeValue(final GraalVMScriptEnvironment scriptEnv, final Value value, final Object... args) {

        Value result = null;

        if (value.canExecute()) {

            if (ArrayUtils.isEmpty(args)) {
                result = value.execute();
            } else {
                final Object[] scriptArgs = hostToGuestUtil.createGuestLanguageObjects(scriptEnv, args);
                result = value.execute(scriptArgs);
            }

        }

        return result;
    }

    public Value instantiateValue(final GraalVMScriptEnvironment scriptEnv, final Value value, final Object... args) {

        Value result = null;

        if (value.canInstantiate()) {

            if (ArrayUtils.isEmpty(args)) {
                result = value.newInstance();
            } else {
                final Object[] scriptArgs = hostToGuestUtil.createGuestLanguageObjects(scriptEnv, args);

                result = value.newInstance(scriptArgs);
            }

        }

        return result;
    }

}