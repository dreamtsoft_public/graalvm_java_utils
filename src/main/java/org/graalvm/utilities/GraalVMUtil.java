/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.graalvm.GraalVMRuntimeException;
import org.graalvm.GraalVMScriptEnvironment;
import org.graalvm.GraalVMWrappedObject;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyObject;
import org.graalvm.proxy.GraalVMScriptable;

public final class GraalVMUtil {

    private final GraalVMValueUtil valueUtil = new GraalVMValueUtil();
    private final GraalVMGuestLanguageToHostUtil guestToHostUtil = new GraalVMGuestLanguageToHostUtil();
    private final GraalVMHostToGuestLanguageUtil hostToGuestUtil = new GraalVMHostToGuestLanguageUtil();


    public List<Object> createArgumentsWithModuleAndExports(final GraalVMScriptEnvironment scriptEnvironment) {

        final Value moduleValue = hostToGuestUtil.createJavascriptFunctionClass(scriptEnvironment, GraalVMScriptConstants.OBJECT_CLASS_NAME);

        final Value moduleExportsValue = hostToGuestUtil.createJavascriptFunctionClass(scriptEnvironment, GraalVMScriptConstants.OBJECT_CLASS_NAME);
        moduleValue.putMember("exports", moduleExportsValue);

        final Value exportsValue = hostToGuestUtil.createJavascriptFunctionClass(scriptEnvironment, GraalVMScriptConstants.OBJECT_CLASS_NAME);

        final List<Object> argValues = new ArrayList<>();
        argValues.add(moduleValue);
        argValues.add(exportsValue);

        return argValues;
    }

    public String moduleWrap(final String script, final Map<String, Object> argMap) {

        String newScript;

        if (MapUtils.isEmpty(argMap)) {
            newScript = moduleWrap(script);
        } else {
            final List<String> arguments = new ArrayList<>(argMap.keySet());
            final String[] args = arguments.toArray(new String[0]);

            newScript = moduleWrap(script, args);
        }

        return newScript;
    }

    public String moduleWrap(final String script, final String... arguments) {

        final String addArgString = StringUtils.join(arguments, ", ");

        final StringBuilder sb = new StringBuilder();
        sb.append("(function(module, exports");

        if (StringUtils.isNotBlank(addArgString)) {
            sb.append(", ").append(addArgString);
        }

        sb.append(") {").append(script).append("})");

        return sb.toString();
    }

    public Object callFunction(final GraalVMScriptEnvironment scriptEnv, final Object obj, final String functionName, final Object... args) {

        final Value value = getMember(obj, functionName);

        if (value == null) {
            return valueUtil.createUndefined(scriptEnv.getContext());
        }

        return callFunction(scriptEnv, value, args);
    }

    // TODO: This method could use some refactoring
    public Value callFunction(final GraalVMScriptEnvironment scriptEnv, final Object obj, final Object... args) {

        if (obj == null) {
            throw new GraalVMRuntimeException("Can not call function on Null Object");
        }

        final Context context = scriptEnv.getContext();
        Value value;

        switch (obj) {
            case String objString -> value = context.eval(scriptEnv.getScriptType(), objString);
            case GraalVMWrappedObject wrappedObject -> value = wrappedObject.getValue();
            case Value valueObject -> value = valueObject;
            case GraalVMScriptable ignored -> value = valueUtil.createUndefined(context);
            default -> value = context.asValue(obj);

        }

        Value result = null;

        if (valueUtil.isFunction(value)) {

            if (ArrayUtils.isEmpty(args)) {
                result = value.execute();
            } else if (args.length == 1) {
                final Object safeArg = hostToGuestUtil.createGuestLanguageObject(scriptEnv, args[0]);

                result = value.execute(safeArg);
            } else {
                final Object[] safeArgs = hostToGuestUtil.createGuestLanguageObjects(scriptEnv, args);

                result = value.execute(safeArgs);
            }

        }

        if (result == null) {
            result = valueUtil.createUndefined(context);
        }

        return result;
    }


    public Value getMember(Object obj, String name) {

        final Value value = hostToGuestUtil.getValue(obj);

        if (value == null) {
            return Value.asValue(null);
        }

        return valueUtil.getMember(value, name);
    }

    public void setMember(Object valueObj, String name, Object obj) {

        final Value value = hostToGuestUtil.getValue(valueObj);

        if (value != null) {
            valueUtil.setMember(value, name, obj);
        }

    }

    public boolean hasMember(Object obj, String name) {

        final Value value = hostToGuestUtil.getValue(obj);

        boolean hasMem = false;

        if (value != null) {
            hasMem = valueUtil.hasMember(value, name);
        }

        return hasMem;
    }

    public boolean isObject(final Object obj) {

        boolean isObj;

        switch (obj) {
            case null -> isObj = false;
            case GraalVMWrappedObject wrappedObject -> {
                final Value value = wrappedObject.getValue();
                isObj = !valueUtil.isArray(value) && !valueUtil.isFunction(value);
            }
            case Value valueObj -> isObj = !valueUtil.isArray(valueObj) && !valueUtil.isFunction(valueObj);
            default -> isObj = true;
        }

        return isObj;
    }

    public boolean isFunction(final Object obj) {

        boolean isFunc;

        switch (obj) {
            case GraalVMWrappedObject wrappedObject -> isFunc = valueUtil.isFunction(wrappedObject.getValue());
            case Value valueObj -> isFunc = valueUtil.isFunction(valueObj);
            default -> isFunc = false;

        }

        return isFunc;
    }

    public boolean hasFunction(final Object obj, final String functionName) {

        boolean hasFunc;

        if (obj == null) {
            hasFunc = false;
        } else if (StringUtils.isBlank(functionName)) {
            hasFunc = false;
        } else if (obj instanceof final GraalVMWrappedObject wrappedObject) {
            final Value value = wrappedObject.getValue();
            hasFunc = valueUtil.hasFunction(value, functionName);
        } else if (obj instanceof final Value value) {
            hasFunc = valueUtil.hasFunction(value, functionName);
        } else {
            hasFunc = false;
        }

        return hasFunc;
    }

    public boolean isArray(final GraalVMScriptEnvironment scriptEnv, final Object obj) {

        if (obj == null) {
            return false;
        } else if (obj.getClass().isArray()) {
            return true;
        }

        Object unwrappedObject = guestToHostUtil.getUnwrappedObject(scriptEnv, obj);

        boolean isArr;

        if (unwrappedObject == null) {
            isArr = false;
        } else if (unwrappedObject.getClass().isArray()) {
            isArr = true;
        } else if (unwrappedObject instanceof final Value value) {
            isArr = valueUtil.isArray(value);
        } else {
            isArr = false;
        }

        return isArr;
    }

    public boolean isNullOrUndefined(final Object obj) {

        return switch (obj) {
            case null -> true;
            case Value value -> valueUtil.isValueNullOrUndefined(value);
            case String objectString -> StringUtils.isBlank(objectString);
            case GraalVMWrappedObject wrappedObject -> valueUtil.isValueNullOrUndefined(wrappedObject.getValue());
            default -> false;
        };

    }

    public boolean isScriptObject(final Object obj) {

        return obj instanceof GraalVMScriptable || obj instanceof Value || obj instanceof ProxyObject;
    }

}