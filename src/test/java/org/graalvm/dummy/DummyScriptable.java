/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.dummy;

import java.util.List;

import org.graalvm.proxy.GraalVMScriptable;

public class DummyScriptable implements GraalVMScriptable {

    private Object obj;

    public DummyScriptable(final Object obj) {

        this.obj = obj;
    }


    @Override
    public Object getMember(final String key) {

        return null;
    }

    @Override
    public List<String> getMemberKeys() {

        return null;
    }

    @Override
    public void putMember(final String key, final Object value) {

    }

    @Override
    public boolean removeMember(final String key) {

        return false;
    }

}