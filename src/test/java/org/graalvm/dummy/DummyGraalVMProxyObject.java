/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.dummy;

import java.util.HashMap;
import java.util.Map;

import org.graalvm.polyglot.HostAccess;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyObject;


public class DummyGraalVMProxyObject implements ProxyObject {

    private Map<String, Object> memberMap = new HashMap<>();

    @Override
    public Object getMember(final String key) {

        if (memberMap.containsKey(key)) {
            return memberMap.get(key);
        }

        return null;
    }

    @Override
    public Object getMemberKeys() {

        return memberMap.keySet();
    }

    @Override
    public boolean hasMember(final String key) {

        return memberMap.containsKey(key);
    }

    @Override
    public boolean removeMember(final String key) {

        final Object value = memberMap.get(key);

        if (value == null) {
            return true;
        }

        return memberMap.remove(key, value);
    }

    @Override
    public void putMember(final String key, final Value value) {

        memberMap.put(key, value);
    }

    @HostAccess.Export
    public String getProxyName() {

        return "ProxyName";
    }

}