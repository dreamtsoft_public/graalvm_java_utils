/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.dummy;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.graalvm.GraalVMRuntimeException;
import org.graalvm.GraalVMScriptEnvironment;
import org.graalvm.polyglot.HostAccess;
import org.graalvm.proxy.GraalVMProxyExecutable;
import org.graalvm.proxy.GraalVMScriptable;

public class DummyScriptObject implements GraalVMScriptable {

    private final Map<String, Object> memberMap = new HashMap<>();
    private final GraalVMScriptEnvironment scriptEnvironment;
    private final String passedInString;

    public DummyScriptObject(final GraalVMScriptEnvironment arg1, final Object... args) {

        scriptEnvironment = arg1;

        if (ArrayUtils.isEmpty(args)) {
            passedInString = "";
        } else {
            passedInString = (String) args[0];
        }

    }

    public GraalVMScriptEnvironment getScriptEnvironment() {

        return scriptEnvironment;
    }

    @HostAccess.Export
    public String getPassedInString() {

        return passedInString;
    }

    public String toString() {

        return passedInString;
    }

    @Override
    public Object getMember(final String key) {

        if (StringUtils.equals(key, "getPassedInString")) {

            try {
                final Method method = getClass().getMethod("getPassedInString");
                return new GraalVMProxyExecutable(scriptEnvironment, this, method);
            } catch (NoSuchMethodException e) {
                throw new GraalVMRuntimeException(e);
            }

        } else if (memberMap.containsKey(key)) {
            return memberMap.get(key);
        }

        return scriptEnvironment.createUndefined();
    }

    @Override
    public List<String> getMemberKeys() {

        final List<String> memKeys = new ArrayList<>();
        memKeys.add("getPassedInString");

        return memKeys;
    }

    @Override
    public void putMember(final String key, final Object value) {

        memberMap.put(key, value);
    }

    @Override
    public boolean removeMember(final String key) {

        if (memberMap.containsKey(key)) {
            final Object value = memberMap.get(key);
            return memberMap.remove(key, value);
        }

        return true;
    }

}