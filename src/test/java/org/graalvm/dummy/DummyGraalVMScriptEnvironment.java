/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.dummy;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Map;

import org.graalvm.GraalVMScriptEnvironment;
import org.graalvm.GraalVMWrappedObject;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;
import org.graalvm.proxy.GraalVMProxyExecutable;
import org.graalvm.utilities.GraalVMScriptConstants;
import org.graalvm.utilities.GraalVMUtil;

public class DummyGraalVMScriptEnvironment implements GraalVMScriptEnvironment {

    private final GraalVMUtil graalVmUtil = new GraalVMUtil();

    private Context context;
    private String scriptType;

    public DummyGraalVMScriptEnvironment() {

        scriptType = GraalVMScriptConstants.SCRIPT_TYPE_JS;
    }

    public DummyGraalVMScriptEnvironment(final Context context) {

        this.context = context;
        scriptType = GraalVMScriptConstants.SCRIPT_TYPE_JS;
    }

    public DummyGraalVMScriptEnvironment(final Context context, final String scriptType) {

        this.context = context;
        this.scriptType = scriptType;
    }

    public void initialize() {

        if (context == null) {
            context = Context.newBuilder().allowAllAccess(true).build();
        }

    }

    @Override
    public void close() {

        context.close();
    }

    @Override
    public String getScriptType() {

        return scriptType;
    }

    @Override
    public Context getContext() {

        return context;
    }

    @Override
    public Value getBindings() {

        return context.getBindings(scriptType);
    }

    @Override
    public GraalVMWrappedObject wrap(final Object guestObject) {


        return new GraalVMWrappedObject(this, context.asValue(guestObject));
    }

    @Override
    public GraalVMWrappedObject createUndefined() {

        return new GraalVMWrappedObject(this, context.asValue(null));
    }

    @Override
    public GraalVMProxyExecutable getGuestLanguageExecutable(final Object hostObject, final Method method) {

        return new GraalVMProxyExecutable(this, hostObject, method);
    }

    @Override
    public Object getGuestLanguageObject(final Object hostObject) {

        return context.asValue(hostObject);
    }

    @Override
    public Object getHostLanguageObject(final Object guestObject) {

        if (guestObject instanceof final Value value) {

            if (value.isHostObject()) {
                return value.asHostObject();
            } else {
                return value.asString();
            }

        } else if (guestObject instanceof final GraalVMWrappedObject wrappedObject) {
            final Value value = wrappedObject.getValue();

            if (value.isHostObject()) {
                return value.asHostObject();
            } else {
                return value.asString();
            }

        }

        return guestObject;
    }

    @Override
    public boolean isArray(final Object obj) {

        return graalVmUtil.isArray(this, obj);
    }

    @Override
    public boolean isNullOrUndefined(final Object obj) {

        if (obj == null) {
            return true;
        }

        if (obj instanceof final Value value) {

            return value.isNull();
        }

        return false;
    }

    @Override
    public boolean isNotNullOrUndefined(final Object o) {

        return false;
    }

    @Override
    public boolean isFunction(final Object obj) {

        return false;
    }

    @Override
    public boolean hasPrototypeFunction(final Object protoObject, final String functionName) {

        return false;
    }

    @Override
    public GraalVMWrappedObject createNewString(final Object... args) {

        return null;
    }

    @Override
    public GraalVMWrappedObject createNewObject(final Object... args) {

        return null;
    }

    @Override
    public GraalVMWrappedObject createNewArray(final Object... args) {

        return null;
    }

    @Override
    public GraalVMWrappedObject createNewFunction(final Object... args) {

        return null;
    }

    @Override
    public GraalVMWrappedObject createFunctionClass(final String name, final Object... args) {

        return null;
    }

    @Override
    public GraalVMWrappedObject getStringClass() {

        return null;
    }

    @Override
    public GraalVMWrappedObject getObjectClass() {

        return null;
    }

    @Override
    public GraalVMWrappedObject getArrayClass() {

        return null;
    }

    @Override
    public GraalVMWrappedObject getFunctionClass() {

        return null;
    }

    @Override
    public GraalVMWrappedObject createMemberClass(final String name) {

        return null;
    }

    @Override
    public GraalVMWrappedObject getTypedClass(final String objectName) {

        return null;
    }

    @Override
    public GraalVMWrappedObject getGlobalObject(final String name) {

        return null;
    }

    @Override
    public GraalVMWrappedObject findGlobalObject(final String objectName) {

        return new GraalVMWrappedObject(this, getBindings().getMember(objectName));
    }

    @Override
    public void setGlobals(final Map<String, Object> globalMap) {

        final Value bindings = getBindings();
        globalMap.forEach((key, value) -> bindings.putMember(key, value));
    }

    @Override
    public void setGlobal(final String name, final Object value) {

        getBindings().putMember(name, value);
    }

    @Override
    public GraalVMWrappedObject getGlobalProperty(final String objectName) {

        return null;
    }

    @Override
    public GraalVMWrappedObject scriptEvaluate(final File scriptFile, final Object... args) {

        return null;
    }

    @Override
    public GraalVMWrappedObject scriptEvaluate(final String script, final Object... args) {

        return null;
    }

    @Override
    public Object scriptEvaluateWithGlobalGindings(final String script, final Map<String, Object> globalBindings, final String var, final Object... args) {

        return null;
    }

}