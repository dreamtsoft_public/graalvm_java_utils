/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.graalvm.dummy.DummyGraalVMScriptEnvironment;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;
import org.graalvm.utilities.GraalVMValueUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GraalVMWrappedObjectTest {

    private Context context;

    private GraalVMScriptEnvironment defaulScriptEnvironment;

    @BeforeEach
    public void setup() {

        context = Context.newBuilder().allowAllAccess(true).build();

        defaulScriptEnvironment = new DummyGraalVMScriptEnvironment(context);
    }

    @Test
    public void testCreateWrappedObjectFailNoScriptEnvironment() {

        final Value value = context.asValue(FOO);

        assertThrows(GraalVMRuntimeException.class, () -> new GraalVMWrappedObject(null, value));
    }

    @Test
    public void testCreateWrappedObjectFailNoValue() {

        assertThrows(GraalVMRuntimeException.class, () -> new GraalVMWrappedObject(defaulScriptEnvironment, null));
    }

    @Test
    public void testGetValue() {

        final Value value = context.asValue(FOO);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        final Value result = wrappedObject.getValue();

        assertThat(result).as("Wrapped Object Value").isEqualTo(value);
    }

    @Test
    public void testToString() {

        final Value value = context.asValue(FOO);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        final String result = wrappedObject.toString();

        assertThat(result).as("Wrapped Object To String").isEqualTo(FOO);
    }

    @Test
    public void testToStringWithMap() {

        final String expected = "{id=42, text=42}";
        final Map<String, Object> map = createMemberMap();

        final Value value = context.asValue(map);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        final String result = wrappedObject.toString();

        assertThat(result).as("Wrapped Object To String").isEqualTo(expected);
    }

    @Test
    public void testIsFunctionTrue() throws Exception {

        final Source source = Source.newBuilder(JS, JS_FUNCTION, "JsFunction").build();
        final Value value = context.eval(source);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        assertThat(wrappedObject.isFunction()).as("Wrapped Object Is Function").isTrue();
    }

    @Test
    public void testIsFunctionFalse() {

        final Value value = context.asValue(createMemberMap());

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        assertThat(wrappedObject.isFunction()).as("Wrapped Object Is Function").isFalse();
    }

    @Test
    public void testIsArrayTrue() {

        final Value value = context.asValue(DEFAULT_INT_ARRAY);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        assertThat(wrappedObject.isArray()).as("Wrapped Object Is Array").isTrue();
    }

    @Test
    public void testIsArrayFalse() {

        final Value value = context.asValue(FOO);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        assertThat(wrappedObject.isArray()).as("Wrapped Object Is Array").isFalse();
    }

    @Test
    public void testHasFunctionTrue() throws Exception {

        final Source source = Source.newBuilder(JS, JS_OBJECT, "JsObject").build();
        final Value value = context.eval(source);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        assertThat(wrappedObject.hasFunction("fullName")).as("Wrapped Object Has Function").isTrue();
    }

    @Test
    public void testHasFunctionFalse() {

        final Value value = context.asValue(createMemberMap());

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        assertThat(wrappedObject.hasFunction("fullName")).as("Wrapped Object Has Function").isFalse();
    }

    @Test
    public void testUnwrapToString() {

        final Value value = context.asValue(FOO);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        final String result = wrappedObject.unwrapToString();

        assertThat(result).as("Wrapped Object Unwrap To String").isEqualTo(FOO);
    }

    @Test
    public void testToObject() {

        final Map<String, Object> memberMap = createMemberMap();

        final Value value = context.asValue(memberMap);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        final Object result = wrappedObject.toObject();

        assertThat(result).as("Wrapped Object To Object").isEqualTo(memberMap);
    }

    @Test
    public void testUnwrap() {

        final Map<String, Object> memberMap = createMemberMap();

        final Value value = context.asValue(memberMap);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        final Object result = wrappedObject.unwrap();

        assertThat(result).as("Wrapped Object Unwrap").isEqualTo(memberMap);
    }

    @Test
    public void testSetGetObject() {

        final Map<String, Object> memberMap = createMemberMap();

        final Value value = context.asValue(memberMap);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        wrappedObject.setObject(FOO, BAR);

        final Object result = wrappedObject.getObject(FOO);

        assertThat(result).as("Wrapped Object Get Objecct - Foo").isEqualTo(BAR);
    }

    @Test
    public void testHasObjectTrue() {

        final Map<String, Object> memberMap = createMemberMap();

        final Value value = context.asValue(memberMap);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        assertThat(wrappedObject.hasObject("id")).as("Wrapped Object Has Object - id").isTrue();
    }

    @Test
    public void testHasObjectFalse() {

        final Map<String, Object> memberMap = createMemberMap();

        final Value value = context.asValue(memberMap);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        assertThat(wrappedObject.hasObject("arr")).as("Wrapped Object Has Object - arr").isFalse();
    }

    @Test
    public void testCallFunction() throws Exception {

        final Source source = Source.newBuilder(JS, JS_OBJECT, "JsObject").build();
        final Value value = context.eval(source);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        final Object result = wrappedObject.callFunction("fullName", (Object[]) null);

        assertThat(result).as("Wrapped Object Call Function result class").isInstanceOf(String.class);
        assertThat(result.toString()).as("Wrapped Object Call Function result").isEqualTo("John Doe");
    }

    @Test
    public void testNewObject() {

        final Value value = context.asValue(createMemberMap());

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, value);

        Object result = wrappedObject.newObject((Object[]) null);

        assertThat(result).as("Wrapped Object New Object result class").isInstanceOf(GraalVMWrappedObject.class);

        result = ((GraalVMWrappedObject) result).unwrap();

        assertThat(result).as("Wrapped Object New Object result").isEqualTo(createMemberMap());
    }

    @Test
    public void testCall() throws Exception {

        final Source source = Source.newBuilder(JS, JS_FUNCTION, "JsFunction").build();

        final Value callValue = context.eval(source);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(defaulScriptEnvironment, callValue);

        final Object result = wrappedObject.call("Foo", 2);

        assertThat(result).as("Wrapped Object Call result class").isInstanceOf(GraalVMWrappedObject.class);

        final Value unwrappedObj = ((GraalVMWrappedObject) result).getValue();

        final GraalVMValueUtil gValueUtil = new GraalVMValueUtil();
        assertThat(gValueUtil.isValueNullOrUndefined(unwrappedObj)).as("Wrapped Object Call result").isTrue();
    }

    private static Map<String, Object> createMemberMap() {

        final Map<String, Object> memberMap = new HashMap<>();
        memberMap.put("id", 42);
        memberMap.put("text", "42");

        return memberMap;
    }

    private static final String JS = "js";

    private static final String FOO = "Foo";

    private static final String BAR = "Bar";

    private static final Integer[] DEFAULT_INT_ARRAY = {1, 42, 3};

    private static final String JS_OBJECT = """
            ({
              firstName: "John",
              lastName : "Doe",
              id       : 5566,
              fullName : function() {
                return this.firstName + " " + this.lastName;
              }
            })""";

    private static final String JS_FUNCTION = """
            (function showValue(value, numberOfTimes) {

                if ((!value || /^\\s*$/.test(value))) {
                    throw 'Value is Blank';
                }

                if (isNaN(numberOfTimes)) {
                    throw 'Number of Times is Not A Number';
                }

                if (numberOfTimes === undefined || numberOfTimes == 0) {
                    console.log(value);
                }

                for (let i = 0; i < numberOfTimes; i++) {
                    console.log(value);
                }

            })

            """;


}