/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import static org.assertj.core.api.Assertions.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.graalvm.dummy.DummyFunctionalInterface;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GraalVMValueUtilTest {

    private static final List<Integer> INT_LIST = new ArrayList<>();
    private final GraalVMValueUtil valueUtil = new GraalVMValueUtil();
    private Context context;

    @BeforeEach
    public void setup() {

        context = Context.newBuilder().allowAllAccess(true).build();

        if (CollectionUtils.isEmpty(INT_LIST)) {
            INT_LIST.add(1);
            INT_LIST.add(42);
            INT_LIST.add(3);
        }

    }

    @Test
    public void testIsValueNullOrUndefined() {

        assertThat(valueUtil.isValueNullOrUndefined(null)).as("Value Null or Undefined").isTrue();
    }

    @Test
    public void testIsValueNullOrUndefinedWithNullValue() {

        final Value value = context.asValue(null);

        assertThat(valueUtil.isValueNullOrUndefined(value)).as("Object Null or Undefined").isTrue();
    }

    @Test
    public void testIsValueNullOrUndefinedWithNonNullValue() {

        final Value value = context.asValue(FOO);

        assertThat(valueUtil.isValueNullOrUndefined(value)).as("Object Null or Undefined").isFalse();
    }

    @Test
    public void testIsArrayFromArray() {

        final String[] stringArray = {FOO, BAR};

        final Value value = context.asValue(stringArray);

        assertThat(valueUtil.isArray(value)).as("Is Value an Array").isTrue();
    }

    @Test
    public void testIsArrayFromCollection() {

        final List<String> stringArray = new ArrayList<>();
        stringArray.add(FOO);
        stringArray.add(BAR);

        final Value value = context.asValue(stringArray);

        assertThat(valueUtil.isArray(value)).as("Is Value an Array").isTrue();
    }

    @Test
    public void testIsArrayFromEval() {

        final String stringArray = "['foo', 'bar']";
        final Value value = context.eval(JS, stringArray);

        assertThat(valueUtil.isArray(value)).as("Is Value an Array").isTrue();
    }

    @Test
    public void testIsArrayFalse() {

        final String stringArray = "foobar";
        final Value value = context.asValue(stringArray);

        assertThat(valueUtil.isArray(value)).as("Is Value an Array").isFalse();
    }

    @Test
    public void testIsArrayFalseFromEval() {

        final String stringArray = "var text = 'foobar'";
        final Value value = context.eval(JS, stringArray);

        assertThat(valueUtil.isArray(value)).as("Is Value an Array").isFalse();
    }


    @Test
    public void testIsGuestMap() {

        final Value value = context.eval(JS, JS_MAP);

        assertThat(valueUtil.isGuestMap(value)).as("Guest Map").isTrue();
    }

    @Test
    public void testIsGuestMapEmptyMap() {

        final Value value = context.eval(JS, "({})");

        assertThat(valueUtil.isGuestMap(value)).as("Guest Map").isTrue();
    }

    @Test
    public void testIsGuestMapFalse() {

        final Map<String, Object> memberMap = createMemberMap();

        final Value value = context.asValue(memberMap);

        assertThat(valueUtil.isGuestMap(value)).as("Guest Map").isFalse();
    }

    @Test
    public void testIsGuestMapFalseNullValue() {

        assertThat(valueUtil.isGuestMap(null)).as("Guest Map").isFalse();
    }

    @Test
    public void testIsHostMap() {

        final Map<String, Object> memberMap = createMemberMap();

        final Value value = context.asValue(memberMap);

        assertThat(valueUtil.isHostMap(value)).as("Host Map").isTrue();
    }

    @Test
    public void testIsHostMapFalse() {

        final Value value = context.eval(JS, JS_MAP);

        assertThat(valueUtil.isHostMap(value)).as("Host Map").isFalse();
    }

    @Test
    public void testIsHostMapFalseNullValue() {

        assertThat(valueUtil.isHostMap(null)).as("Host Map").isFalse();
    }

    @Test
    public void testIsMapFromHostMap() {

        final Map<String, Object> memberMap = createMemberMap();

        final Value value = context.asValue(memberMap);

        assertThat(valueUtil.isMap(value)).as("Map").isTrue();
    }

    @Test
    public void testIsMapFromGuestMap() {

        final Value value = context.eval(JS, "({})");

        assertThat(valueUtil.isGuestMap(value)).as("Map").isTrue();
    }

    @Test
    public void testIsMapFalseFromHost() {

        final Value value = context.asValue("Foo");

        assertThat(valueUtil.isMap(value)).as("Map").isFalse();
    }

    @Test
    public void testIsMapFalseFromGuest() {

        final Value value = context.eval(JS, JS_ARRAY);

        assertThat(valueUtil.isMap(value)).as("Map").isFalse();
    }

    @Test
    public void testIsObjectFromGuest() {

        final Value value = context.eval(JS, JS_OBJECT);

        assertThat(valueUtil.isObject(value)).as("Object").isTrue();
    }

    @Test
    public void testIsObjectFromHost() {

        final Value value = context.asValue(BigDecimal.ONE);

        assertThat(valueUtil.isObject(value)).as("Object").isTrue();
    }

    @Test
    public void testIsObjectFalseFromGuest() {

        final Value value = context.eval(JS, JS_ARRAY);

        assertThat(valueUtil.isObject(value)).as("Object").isFalse();
    }

    @Test
    public void testIsObjectFalseFromHost() {

        final Value value = context.asValue("Foo");

        assertThat(valueUtil.isObject(value)).as("Object").isFalse();
    }

    @Test
    public void testIsFunctionFromGuest() throws Exception {

        final Source source = Source.newBuilder(JS, JS_FUNCTION, "JsFunction").build();
        final Value value = context.eval(source);

        assertThat(valueUtil.isFunction(value)).as("Is Function from Guest").isTrue();
    }

    @Test
    public void testIsFunctionFromHost() {

        // Javadoc says: "A single method interfaces annotated with FunctionalInterface are executable directly."
        // https://www.baeldung.com/java-8-functional-interfaces
        // https://www.geeksforgeeks.org/functional-interfaces-java/

        final DummyFunctionalInterface dfi = (int x) -> x * x;
        final Value value = context.asValue(dfi);

        assertThat(valueUtil.isFunction(value)).as("Is Function from Host").isTrue();
    }

    @Test
    public void testIsFunctionFalseFromGuest() {

        final Value value = context.eval(JS, JS_MAP);

        assertThat(valueUtil.isFunction(value)).as("Is Function from Guest").isFalse();
    }

    @Test
    public void testIsFunctionFalseFromHost() {

        final Value value = context.asValue(JS_FUNCTION);

        assertThat(valueUtil.isFunction(value)).as("Is Function from Host").isFalse();
    }

    @Test
    public void testHasFunctionFromGuest() throws Exception {

        final Source source = Source.newBuilder(JS, JS_OBJECT, "JSObject").build();
        final Value value = context.eval(source);

        assertThat(valueUtil.hasFunction(value, "fullName")).as("Has Function - fullName").isTrue();
    }

    @Test
    public void testHasFunctionFromHost() {

        final Map<String, Object> memberMap = createMemberMap();

        final Value value = context.asValue(memberMap);

        assertThat(valueUtil.hasFunction(value, "isEmpty")).as("Has Function - isEmpty").isTrue();
    }

    @Test
    public void testHasFunctionFalseFromGuest() {

        final Value value = context.eval(JS, JS_MAP);

        assertThat(valueUtil.hasFunction(value, "fullName")).as("Has Function - fullName").isFalse();
    }

    @Test
    public void testHasFunctionFalseFromHost() {

        final Map<String, Object> memberMap = createMemberMap();

        final Value value = context.asValue(memberMap);

        assertThat(valueUtil.hasFunction(value, "calculateSum")).as("Has Function - calculate").isFalse();
    }

    @Test
    public void testHasFunctionFalseNullValue() {

        final Value value = context.asValue(null);

        assertThat(valueUtil.hasFunction(value, "fullName")).as("Has Function - NULL Value").isFalse();
    }

    @Test
    public void testHasFunctionFalseEmptyStringName() {

        final Value value = context.eval(JS, JS_MAP);

        assertThat(valueUtil.hasFunction(value, "")).as("Has Function - Empty Name").isFalse();
    }

    @Test
    public void testSetHasGetMemberFromGuest() {

        final Value expected = context.asValue(BAR);

        final Value value = context.eval(JS, JS_MAP);
        valueUtil.setMember(value, FOO, BAR);
        assertThat(valueUtil.hasMember(value, FOO)).as("Member from Guest - Foo").isTrue();
        assertThat(valueUtil.getMember(value, FOO).toString()).as("Member from Guest - Foo").isEqualTo(expected.toString());
    }

    @Test
    public void testSetGetMemberFromHost() {

        final Map<String, Object> memberMap = createMemberMap();

        final Value expected = context.asValue(BAR);

        final Value value = context.asValue(memberMap);

        valueUtil.setMember(value, FOO, BAR);
        assertThat(valueUtil.hasMember(value, FOO)).as("Member from Host - Foo").isTrue();
        assertThat(valueUtil.getMember(value, FOO)).as("Member from Host - Foo").isEqualTo(expected);
    }

    @Test
    public void testSetHasGetMemberNullValue() {

        valueUtil.setMember(null, FOO, BAR);
        assertThat(valueUtil.hasMember(null, FOO)).as("Value is Null").isFalse();
        assertThat(valueUtil.getMember(null, FOO)).as("Value is Null").isNull();
    }

    @Test
    public void testGetMemberValueNoMembers() {

        final Value value = context.eval(JS, "var text = 'FooBar'");

        valueUtil.setMember(value, FOO, BAR);
        assertThat(valueUtil.hasMember(value, FOO)).as("No Members").isFalse();
        assertThat(valueUtil.getMember(value, FOO)).as("No Members").isNull();
    }


    private Map<String, Object> createMemberMap() {

        final Map<String, Object> memberMap = new HashMap<>();
        memberMap.put("id", 42);
        memberMap.put("text", "42");
        memberMap.put("arr", DEFAULT_INT_ARRAY);

        return memberMap;
    }

    private static final String JS = "js";

    public static final String FOO = "Foo";

    public static final String BAR = "Bar";

    private static final Integer[] DEFAULT_INT_ARRAY = {1, 42, 3};

    private static final String JS_ARRAY = "([1,42,3])";

    private static final String JS_MAP = """
            ({
             id   : 42,\s
            text : '42',
             arr  : [1,42,3]\s
            })""";

    private static final String JS_OBJECT = """
            ({
              firstName: "John",
              lastName : "Doe",
              id       : 5566,
              fullName : function() {
                return this.firstName + " " + this.lastName;
              }
            })""";

    private static final String JS_FUNCTION = """
            (function showValue(value, numberOfTimes) {

                if ((!value || /^\\s*$/.test(value))) {
                    throw 'Value is Blank';
                }

                if (isNaN(numberOfTimes)) {
                    throw 'Number of Times is Not A Number';
                }

                if (numberOfTimes === undefined || numberOfTimes == 0) {
                    console.log(value);
                }

                for (let i = 0; i < numberOfTimes; i++) {
                    console.log(value);
                }

            })

            """;

}