/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.graalvm.GraalVMRuntimeException;
import org.graalvm.dummy.DummyGraalVMScriptEnvironment;
import org.graalvm.polyglot.Value;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class GraalVMExecutableArgumentsUtilTest {

    private GraalVMExecutableArgumentsUtil util;
    private ClassWithMethodsWithParameters testClass;
    private Class<?> klass;
    private DummyGraalVMScriptEnvironment scriptEnvironment;


    @BeforeEach
    public void setup() {

        util = new GraalVMExecutableArgumentsUtil();
        testClass = new ClassWithMethodsWithParameters();
        klass = testClass.getClass();

        scriptEnvironment = new DummyGraalVMScriptEnvironment();
        scriptEnvironment.initialize();
    }

    @Test
    public void testIsEndIndexTrue() {

        assertThat(util.isEndIndex(2, 1)).as("Is End Index").isTrue();
    }

    @Test
    public void testIsEndIndexFalse() {

        assertThat(util.isEndIndex(2, 0)).as("Is End Index").isFalse();
    }

    @Test
    public void testIsEndIndexTrueOutOfBounds() {

        assertThat(util.isEndIndex(2, 2)).as("Is End Index").isTrue();
    }

    @Test
    public void testBackFill() {

        final GraalVMHostArguments hostArguments = new GraalVMHostArguments(scriptEnvironment, 3);
        hostArguments.addImmutableArg(0, "FOO");

        final int startIndex = 1;

        util.backfill(hostArguments, startIndex);

        final Object[] hostArgs = hostArguments.getHostArgs();

        assertThat(hostArgs).as("Host Args").hasSize(3);
        assertThat(hostArgs[0]).as("Host Args - 0").isEqualTo("FOO");
        assertThat(hostArgs[1]).as("Host Args - 1").isNull();
        assertThat(hostArgs[2]).as("Host Args - 2").isNull();
    }

    @Test
    public void testAddArray() {

        final Object[] objArray = {"Foo", "Bar"};
        final Class<?> paramType = objArray.getClass();
        final GraalVMHostArguments hostArguments = new GraalVMHostArguments(scriptEnvironment, 1);
        final int index = 0;
        final Value arg = scriptEnvironment.getContext().asValue(objArray);

        util.addArray(scriptEnvironment, paramType, hostArguments, index, arg);

        final Object[] hostArgs = hostArguments.getHostArgs();

        assertThat(hostArgs).as("Host Args").hasSize(1);

        final Object hostArg = hostArgs[0];

        assertThat(hostArg).as("Host Arg - 0").isNotNull();
        assertThat(hostArg.getClass().isArray()).as("Host Arg - 0 - is array").isTrue();

        final Object[] hostArgArray = (Object[]) hostArg;

        assertThat(hostArgArray).as("Host Arg Array size").hasSize(2);
        assertThat(hostArgArray[0]).as("Host Arg Array - 0").isEqualTo("Foo");
        assertThat(hostArgArray[1]).as("Host Arg Array - 0").isEqualTo("Bar");
    }

    @Test
    public void testAddArrayWhenArgIsNotAnArray() {

        final String[] objArray = {"Foo", "Bar"};
        final Class<?> paramType = objArray.getClass();
        final GraalVMHostArguments hostArguments = new GraalVMHostArguments(scriptEnvironment, 1);
        final int index = 0;
        final Value arg = scriptEnvironment.getContext().asValue("Foo");

        util.addArray(scriptEnvironment, paramType, hostArguments, index, arg);

        final Object[] hostArgs = hostArguments.getHostArgs();

        assertThat(hostArgs).as("Host Args").hasSize(1);

        final Object hostArg = hostArgs[0];

        assertThat(hostArg).as("Host Arg - 0").isNotNull();
        assertThat(hostArg.getClass().isArray()).as("Host Arg - 0 - is array").isTrue();

        final Object[] hostArgArray = (Object[]) hostArg;

        assertThat(hostArgArray).as("Host Arg Array size").hasSize(1);
        assertThat(hostArgArray[0]).as("Host Arg Array - 0").isEqualTo("Foo");
    }

    @Test
    public void testAddHostArgsAsVarArgs() {

        final String method = "someMethod";
        final int paramCount = 3;
        final int argCount = 4;
        final int index = 2;

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnvironment, 4);
        hostArgs.addImmutableArg(0, "Foo");
        hostArgs.addImmutableArg(1, "Bar");

        final Value[] arguments = new Value[4];
        arguments[0] = scriptEnvironment.getContext().asValue("Arg 1");
        arguments[1] = scriptEnvironment.getContext().asValue("Arg 2");
        arguments[2] = scriptEnvironment.getContext().asValue("Arg 3");
        arguments[3] = scriptEnvironment.getContext().asValue("Arg 4");


        util.addHostArgsAsVarArgs(scriptEnvironment, method, paramCount, argCount, hostArgs, index, arguments);

        final Object[] hostArgArray = hostArgs.getHostArgs();
        assertThat(hostArgArray).as("Host Args length").hasSize(4);
        assertThat(hostArgArray[0]).as("Host Arg 1").isEqualTo("Foo");
        assertThat(hostArgArray[1]).as("Host Arg 2").isEqualTo("Bar");
    }

    @Test
    public void testAddHostArgInvalid() {

        final Object[] objArray = {"Foo", "Bar"};
        final String method = "someMethod";

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnvironment, 1);
        final int index = 0;
        final Value arg = scriptEnvironment.getContext().asValue(objArray);

        assertThrows(GraalVMRuntimeException.class, () -> util.addHostArg(scriptEnvironment, method, hostArgs, index, arg));
    }

    @Test
    public void testAddHostArg() {

        final String method = "someMethod";

        final GraalVMHostArguments hostArguments = new GraalVMHostArguments(scriptEnvironment, 1);
        final int index = 0;
        final Value arg = scriptEnvironment.getContext().asValue("Foo");

        util.addHostArg(scriptEnvironment, method, hostArguments, index, arg);

        final Object[] hostArgs = hostArguments.getHostArgs();

        assertThat(hostArgs).as("Host Args").hasSize(1);

        final Object hostArg = hostArgs[0];

        assertThat(hostArg).as("Host Arg - 0").isEqualTo("Foo");
    }

    @Test
    public void testAddHostArgJavaScriptMap() {

        final String jsMap = """
                ({
                    id: 1,
                    name: 'Foo'
                })
                """;

        final String method = "someMethod";

        final GraalVMHostArguments hostArguments = new GraalVMHostArguments(scriptEnvironment, 1);
        final int index = 0;
        final Value arg = scriptEnvironment.getContext().eval(GraalVMScriptConstants.SCRIPT_TYPE_JS, jsMap);

        util.addHostArg(scriptEnvironment, method, hostArguments, index, arg);

        final Object[] hostArgs = hostArguments.getHostArgs();

        assertThat(hostArgs).as("Host Args").hasSize(1);

        final Object hostArg = hostArgs[0];

        assertThat(hostArg).as("Host Arg - 0").isInstanceOf(Map.class);
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void testGetHostArgumentsWithNoParameters() throws Exception {

        final Method method = getMethod("noParams");

        final GraalVMHostArguments hostArgs = util.getHostArguments(scriptEnvironment, method);

        assertThat(hostArgs).as("No Parameter Args").isNull();

        final Object[] args = null;

        invokeMethod(method, args);
    }

    @Test
    public void testGetHostArgumentsWithOneParameter() throws Exception {

        final String expected = "Param";

        final Method method = getMethod("oneParam");
        final Value arg = scriptEnvironment.getContext().asValue(expected);

        final GraalVMHostArguments hostArgs = util.getHostArguments(scriptEnvironment, method, arg);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("One Parameter Args").isNotNull();
        assertThat(args).as("One Parameter Args Length").hasSize(1);

        assertThat(args[0]).as("One Parameter - Arg 1").isNotNull();
        assertThat(args[0]).as("One Parameter - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("One Parameter - Arg 1 Value").isEqualTo(expected);

        invokeMethod(method, args);
    }

    @Test
    public void testGetHostArgumentsWithOneParameterNoArgument() throws Exception {

        final Method method = getMethod("oneParam");

        final GraalVMHostArguments hostArgs = util.getHostArguments(scriptEnvironment, method);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("One Parameter Args").isNotNull();
        assertThat(args).as("One Parameter Args Length").hasSize(1);

        assertThat(args[0]).as("One Parameter - Arg 1").isNull();

        invokeMethod(method, args);
    }

    @Test
    public void testGetHostArgumentsWithOneParameterTooManyArguments() throws Exception {

        final String expected1 = "Param1";
        final String expected2 = "Param2";

        final Method method = getMethod("oneParam");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);
        final Value arg2 = scriptEnvironment.getContext().asValue(expected2);

        final GraalVMHostArguments hostArgs = util.getHostArguments(scriptEnvironment, method, arg1, arg2);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("One Parameter Args").isNotNull();
        assertThat(args).as("One Parameter Args Length").hasSize(1);

        assertThat(args[0]).as("One Parameter - Arg 1").isNotNull();
        assertThat(args[0]).as("One Parameter - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("One Parameter - Arg 1 Value").isEqualTo(expected1);

        invokeMethod(method, args);
    }

    @Test
    public void testGetHostArgumentsWithOneParameterWithVarArgs() throws Exception {

        final String param1 = "Param 1";
        final String[] param2 = {"Param 2.1", "Param 2.2"};

        final Value arg1 = scriptEnvironment.getContext().asValue(param1);
        final Value arg2 = scriptEnvironment.getContext().asValue(param2);

        final Method method = getMethod("oneParamWithVarArgs");

        final GraalVMHostArguments hostArgs = util.getHostArguments(scriptEnvironment, method, arg1, arg2);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("One Parameter Args").isNotNull();
        assertThat(args).as("One Parameter Args Length").hasSize(2);

        final Object hostArg1 = args[0];

        assertThat(hostArg1).as("One Parameter - Arg 1").isNotNull();
        assertThat(hostArg1).as("One Parameter - Arg 1 Class").isInstanceOf(String.class);
        assertThat(hostArg1).as("One Parameter - Arg 1 Value").isEqualTo(param1);

        final Object hostArg2 = args[1];

        assertThat(hostArg2).as("Var Arg Parameter - Arg 2").isNotNull();
        assertThat(hostArg2.getClass().isArray()).as("Var Arg Parameter - Arg 2 is Array").isTrue();

        final Object[] hostArg2Array = (Object[]) hostArg2;

        assertThat(hostArg2Array).as("Var Arg Parameter - Arg 2 Size").hasSize(2);
        assertThat(hostArg2Array[0]).as("Var Arg Parameter - Arg 2-0 Class").isInstanceOf(String.class);
        assertThat(hostArg2Array[0]).as("Var Arg Parameter - Arg 2-0 Class").isEqualTo("Param 2.1");
        assertThat(hostArg2Array[1]).as("Var Arg Parameter - Arg 2-1 Class").isInstanceOf(String.class);
        assertThat(hostArg2Array[1]).as("Var Arg Parameter - Arg 2-1 Class").isEqualTo("Param 2.2");

        invokeMethod(method, args);
    }

    @Test
    public void testGetHostArgumentsWithTwoParameters() throws Exception {

        final String expected1 = "Param1";
        final String expected2 = "Param2";

        final Method method = getMethod("twoParam");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);
        final Value arg2 = scriptEnvironment.getContext().asValue(expected2);

        final GraalVMHostArguments hostArgs = util.getHostArguments(scriptEnvironment, method, arg1, arg2);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Two Parameter Args").isNotNull();
        assertThat(args).as("Two Parameter Args Length").hasSize(2);

        assertThat(args[0]).as("Two Parameter - Arg 1").isNotNull();
        assertThat(args[0]).as("Two Parameter - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("Two Parameter - Arg 1 Value").isEqualTo(expected1);

        assertThat(args[1]).as("Two Parameter - Arg 2").isNotNull();
        assertThat(args[1]).as("Two Parameter - Arg 2 Class").isInstanceOf(String.class);
        assertThat(args[1]).as("Two Parameter - Arg 2 Value").isEqualTo(expected2);

        invokeMethod(method, args);
    }

    @Test
    public void testGetHostArgumentsWithTwoParametersNullSecondParameter() throws Exception {

        final String expected1 = "Param1";

        final Method method = getMethod("twoParam");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);

        final GraalVMHostArguments hostArgs = util.getHostArguments(scriptEnvironment, method, arg1);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Two Parameter Args").isNotNull();
        assertThat(args).as("Two Parameter Args Length").hasSize(2);

        assertThat(args[0]).as("Two Parameter - Arg 1").isNotNull();
        assertThat(args[0]).as("Two Parameter - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("Two Parameter - Arg 1 Value").isEqualTo(expected1);

        assertThat(args[1]).as("Two Parameter - Arg 2").isNull();

        invokeMethod(method, args);
    }

    @Test
    public void testGetHostArgumentsWithTwoParametersNullParameters() throws Exception {

        final Method method = getMethod("twoParam");

        final GraalVMHostArguments hostArgs = util.getHostArguments(scriptEnvironment, method);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Two Parameter Args").isNotNull();
        assertThat(args).as("Two Parameter Args Length").hasSize(2);

        assertThat(args[0]).as("Two Parameter - Arg 1").isNull();
        assertThat(args[1]).as("Two Parameter - Arg 2").isNull();

        invokeMethod(method, args);
    }

    @Test
    public void testGetHostArgumentsWithTwoParametersWithTooManyArguments() throws Exception {

        final String expected1 = "Param1";
        final String expected2 = "Param2";
        final String expected3 = "Param3";

        final Method method = getMethod("twoParam");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);
        final Value arg2 = scriptEnvironment.getContext().asValue(expected2);
        final Value arg3 = scriptEnvironment.getContext().asValue(expected3);

        final GraalVMHostArguments hostArgs = util.getHostArguments(scriptEnvironment, method, arg1, arg2, arg3);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Two Parameter ").isNotNull();
        assertThat(args).as("Two Parameter Length").hasSize(2);

        assertThat(args[0]).as("Two Parameter - Arg 1").isNotNull();
        assertThat(args[0]).as("Two Parameter - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("Two Parameter - Arg 1 Value").isEqualTo(expected1);

        assertThat(args[1]).as("Two Parameter - Arg 2").isNotNull();
        assertThat(args[1]).as("Two Parameter - Arg 2 Class").isInstanceOf(String.class);
        assertThat(args[1]).as("Two Parameter - Arg 2 Value").isEqualTo(expected2);

        invokeMethod(method, args);
    }

    @Test
    public void testGetHostArgumentsWithVarArgs() throws Exception {

        final String expected1 = "Param1";
        final String expected2 = "Param2";
        final String expected3 = "Param3";

        final Object[] args = {expected1, expected2, new String[]{expected3}};

        final Method method = getMethod("twoParamWithVarArg");
        invokeMethod(method, args);
    }

    @Test
    public void testGetHostArgumentsWithTwoParametersWithVarArgs() throws Exception {

        final String expected1 = "Param1";
        final String expected2 = "Param2";
        final String expected3 = "Param3";

        final Method method = getMethod("twoParamWithVarArg");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);
        final Value arg2 = scriptEnvironment.getContext().asValue(expected2);
        final Value arg3 = scriptEnvironment.getContext().asValue(expected3);

        final GraalVMHostArguments hostArgs = util.getHostArguments(scriptEnvironment, method, arg1, arg2, arg3);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Two Parameter with Var Args").isNotNull();
        assertThat(args).as("Two Parameter with Var Args Length").hasSize(3);

        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1").isNotNull();
        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1 Value").isEqualTo(expected1);

        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2").isNotNull();
        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2 Class").isInstanceOf(String.class);
        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2 Value").isEqualTo(expected2);

        final Object varArg = args[2];

        assertThat(varArg).as("Two Parameter with Var Arg - Arg 3").isNotNull();
        assertThat(varArg.getClass().isArray()).as("Two Parameter with Var Arg - Arg 3 Is Array").isTrue();

        final Object[] varArgs = (Object[]) varArg;

        assertThat(varArgs).as("Two Parameter with Var Arg - Arg 3 Length").hasSize(1);
        assertThat(varArgs[0]).as("Two Parameter with Var Arg - Arg 3,1 Class").isInstanceOf(String.class);
        assertThat(varArgs[0]).as("Two Parameter with Var Arg - Arg 3,1 Value").isEqualTo(expected3);

        invokeMethod(method, args);
    }

    @Test
    public void testGetHostArgumentsWithTwoParametersWithVarArgsWithArray() throws Exception {

        final String expected1 = "Param1";
        final String expected2 = "Param2";
        final String expected3 = "Param3";
        final String expected4 = "Param4";
        final String[] expectedArray = {expected3, expected4};

        final Method method = getMethod("twoParamWithVarArg");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);
        final Value arg2 = scriptEnvironment.getContext().asValue(expected2);
        final Value arg3 = scriptEnvironment.getContext().asValue(expectedArray);

        final GraalVMHostArguments hostArgs = util.getHostArguments(scriptEnvironment, method, arg1, arg2, arg3);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Two Parameter with Var Args").isNotNull();
        assertThat(args).as("Two Parameter with Var Args Length").hasSize(3);

        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1").isNotNull();
        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1 Value").isEqualTo(expected1);

        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2").isNotNull();
        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2 Class").isInstanceOf(String.class);
        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2 Value").isEqualTo(expected2);

        final Object varArg = args[2];

        assertThat(varArg).as("Two Parameter with Var Arg - Arg 3").isNotNull();
        assertThat(varArg.getClass().isArray()).as("Two Parameter with Var Arg - Arg 3 Is Array").isTrue();

        final Object[] varArgs = (Object[]) varArg;

        assertThat(varArgs).as("Two Parameter with Var Arg - Arg 3 Length").hasSize(2);
        assertThat(varArgs[0]).as("Two Parameter with Var Arg - Arg 3,1 Class").isInstanceOf(String.class);
        assertThat(varArgs[0]).as("Two Parameter with Var Arg - Arg 3,1 Value").isEqualTo(expected3);
        assertThat(varArgs[1]).as("Two Parameter with Var Arg - Arg 3,2 Class").isInstanceOf(String.class);
        assertThat(varArgs[1]).as("Two Parameter with Var Arg - Arg 3,2 Value").isEqualTo(expected4);

        invokeMethod(method, args);
    }

    @Test
    public void testGetHostArgumentsWithTwoParametersWithNoVarArgs() throws Exception {

        final String expected1 = "Param1";
        final String expected2 = "Param2";

        final Method method = getMethod("twoParamWithVarArg");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);
        final Value arg2 = scriptEnvironment.getContext().asValue(expected2);

        final GraalVMHostArguments hostArgs = util.getHostArguments(scriptEnvironment, method, arg1, arg2);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Two Parameter with Var Args").isNotNull();
        assertThat(args).as("Two Parameter with Var Args Length").hasSize(3);

        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1").isNotNull();
        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1 Value").isEqualTo(expected1);

        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2").isNotNull();
        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2 Class").isInstanceOf(String.class);
        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2 Value").isEqualTo(expected2);

        final Object varArg = args[2];

        assertThat(varArg).as("Two Parameter with Var Arg - Arg 3").isNull();

        invokeMethod(method, args);
    }

    @Test
    public void testGetHostArgumentsWithParamArrayParamVarArg() throws Exception {

        final String expected1 = "Param1";
        final String expected2 = "Param2";
        final String expected3 = "Param3";
        final String[] expectedArray = {expected2, expected3};
        final String expected4 = "Param4";
        final String expected5 = "Param5";
        final String expected6 = "Param6";
        final String[] expectedVarArg = {expected5, expected6};

        final Method method = getMethod("paramArrayParamVarArg");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);
        final Value arg2 = scriptEnvironment.getContext().asValue(expectedArray);
        final Value arg3 = scriptEnvironment.getContext().asValue(expected4);
        final Value arg4 = scriptEnvironment.getContext().asValue(expectedVarArg);

        final GraalVMHostArguments hostArgs = util.getHostArguments(scriptEnvironment, method, arg1, arg2, arg3, arg4);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Param Array Param Var Args").isNotNull();
        assertThat(args).as("Param Array Param Var Args Length").hasSize(4);

        assertThat(args[0]).as("Param Array Param Var Args - Arg 1").isNotNull();
        assertThat(args[0]).as("Param Array Param Var Args - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("Param Array Param Var Args - Arg 1 Value").isEqualTo(expected1);

        final Object arrayArg = args[1];

        assertThat(arrayArg).as("Param Array Param Var Args - Arg 2").isNotNull();
        assertThat(arrayArg.getClass().isArray()).as("Param Array Param Var Args - Arg 2 Is Array").isTrue();

        final Object[] arrayArgs = (Object[]) arrayArg;

        assertThat(arrayArgs).as("Param Array Param Var Args - Arg 2 Length").hasSize(2);
        assertThat(arrayArgs[0]).as("Param Array Param Var Args - Arg 2,1").isNotNull();
        assertThat(arrayArgs[0]).as("Param Array Param Var Args - Arg 2,1 Class").isInstanceOf(String.class);
        assertThat(arrayArgs[0]).as("Param Array Param Var Args - Arg 2,1 Value").isEqualTo(expected2);
        assertThat(arrayArgs[1]).as("Param Array Param Var Args - Arg 2,2").isNotNull();
        assertThat(arrayArgs[1]).as("Param Array Param Var Args - Arg 2,2 Class").isInstanceOf(String.class);
        assertThat(arrayArgs[1]).as("Param Array Param Var Args - Arg 2,2 Value").isEqualTo(expected3);

        assertThat(args[2]).as("Param Array Param Var Args - Arg 3").isNotNull();
        assertThat(args[2]).as("Param Array Param Var Args - Arg 3 Class").isInstanceOf(String.class);
        assertThat(args[2]).as("Param Array Param Var Args - Arg 3 Value").isEqualTo(expected4);

        final Object varArg = args[3];

        assertThat(varArg).as("Param Array Param Var Args - Arg 4").isNotNull();
        assertThat(varArg.getClass().isArray()).as("Param Array Param Var Args - Arg 4 Is Array").isTrue();

        final Object[] varArgs = (Object[]) varArg;

        assertThat(varArgs).as("Param Array Param Var Args - Arg 4 Length").hasSize(2);
        assertThat(varArgs[0]).as("Param Array Param Var Args - Arg 4,1 Class").isInstanceOf(String.class);
        assertThat(varArgs[0]).as("Param Array Param Var Args - Arg 4,1 Value").isEqualTo(expected5);
        assertThat(varArgs[1]).as("Param Array Param Var Args - Arg 4,2 Class").isInstanceOf(String.class);
        assertThat(varArgs[1]).as("Param Array Param Var Args - Arg 4,2 Value").isEqualTo(expected6);

        invokeMethod(method, args);
    }

    @Test
    public void testGetHostArgumentsWithNoBackfillWithNoParameters() {

        final Method method = getMethod("noParams");

        final GraalVMHostArguments hostArgs = util.getHostArgumentsWithNoBackfill(scriptEnvironment, method);

        assertThat(hostArgs).as("No Parameter Args").isNotNull();
        assertThat(hostArgs.getLength()).as("No Parameter Args").isEqualTo(0);
    }

    @Test
    public void testGetHostArgumentsWithNoBackfillWithOneParameter() {

        final String expected = "Param";

        final Method method = getMethod("oneParam");
        final Value arg = scriptEnvironment.getContext().asValue(expected);

        final GraalVMHostArguments hostArgs = util.getHostArgumentsWithNoBackfill(scriptEnvironment, method, arg);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("One Parameter Args").isNotNull();
        assertThat(args).as("One Parameter Args Length").hasSize(1);

        assertThat(args[0]).as("One Parameter - Arg 1").isNotNull();
        assertThat(args[0]).as("One Parameter - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("One Parameter - Arg 1 Value").isEqualTo(expected);
    }

    @Test
    public void testGetHostArgumentsWithNoBackfillWithOneParameterTooManyArguments() {

        final String expected1 = "Param1";
        final String expected2 = "Param2";

        final Method method = getMethod("oneParam");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);
        final Value arg2 = scriptEnvironment.getContext().asValue(expected2);

        final GraalVMHostArguments hostArgs = util.getHostArgumentsWithNoBackfill(scriptEnvironment, method, arg1, arg2);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("One Parameter Args").isNotNull();
        assertThat(args).as("One Parameter Args Length").hasSize(1);

        assertThat(args[0]).as("One Parameter - Arg 1").isNotNull();
        assertThat(args[0]).as("One Parameter - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("One Parameter - Arg 1 Value").isEqualTo(expected1);
    }

    @Test
    public void testGetHostArgumentsWithNoBackfillWithOneParameterWithVarArgs() {

        final String param1 = "Param 1";
        final String[] param2 = {"Param 2.1", "Param 2.2"};

        final Value arg1 = scriptEnvironment.getContext().asValue(param1);
        final Value arg2 = scriptEnvironment.getContext().asValue(param2);

        final Method method = getMethod("oneParamWithVarArgs");

        final GraalVMHostArguments hostArgs = util.getHostArgumentsWithNoBackfill(scriptEnvironment, method, arg1, arg2);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("One Parameter Args").isNotNull();
        assertThat(args).as("One Parameter Args Length").hasSize(2);

        final Object hostArg1 = args[0];

        assertThat(hostArg1).as("One Parameter - Arg 1").isNotNull();
        assertThat(hostArg1).as("One Parameter - Arg 1 Class").isInstanceOf(String.class);
        assertThat(hostArg1).as("One Parameter - Arg 1 Value").isEqualTo(param1);

        final Object hostArg2 = args[1];

        assertThat(hostArg2).as("Var Arg Parameter - Arg 2").isNotNull();
        assertThat(hostArg2.getClass().isArray()).as("Var Arg Parameter - Arg 2 is Array").isTrue();

        final Object[] hostArg2Array = (Object[]) hostArg2;

        assertThat(hostArg2Array).as("Var Arg Parameter - Arg 2 Size").hasSize(2);
        assertThat(hostArg2Array[0]).as("Var Arg Parameter - Arg 2-0 Class").isInstanceOf(String.class);
        assertThat(hostArg2Array[0]).as("Var Arg Parameter - Arg 2-0 Class").isEqualTo("Param 2.1");
        assertThat(hostArg2Array[1]).as("Var Arg Parameter - Arg 2-1 Class").isInstanceOf(String.class);
        assertThat(hostArg2Array[1]).as("Var Arg Parameter - Arg 2-1 Class").isEqualTo("Param 2.2");
    }

    @Test
    public void testGetHostArgumentsWithNoBackfillWithTwoParameters() {

        final String expected1 = "Param1";
        final String expected2 = "Param2";

        final Method method = getMethod("twoParam");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);
        final Value arg2 = scriptEnvironment.getContext().asValue(expected2);

        final GraalVMHostArguments hostArgs = util.getHostArgumentsWithNoBackfill(scriptEnvironment, method, arg1, arg2);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Two Parameter Args").isNotNull();
        assertThat(args).as("Two Parameter Args Length").hasSize(2);

        assertThat(args[0]).as("Two Parameter - Arg 1").isNotNull();
        assertThat(args[0]).as("Two Parameter - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("Two Parameter - Arg 1 Value").isEqualTo(expected1);

        assertThat(args[1]).as("Two Parameter - Arg 2").isNotNull();
        assertThat(args[1]).as("Two Parameter - Arg 2 Class").isInstanceOf(String.class);
        assertThat(args[1]).as("Two Parameter - Arg 2 Value").isEqualTo(expected2);
    }

    @Test
    public void testGetHostArgumentsWithNoBackfillWithTwoParametersNullSecondParameter() {

        final String expected1 = "Param1";

        final Method method = getMethod("twoParam");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);

        final GraalVMHostArguments hostArgs = util.getHostArgumentsWithNoBackfill(scriptEnvironment, method, arg1);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Two Parameter Args").isNotNull();
        assertThat(args).as("Two Parameter Args Length").hasSize(1);

        assertThat(args[0]).as("Two Parameter - Arg 1").isNotNull();
        assertThat(args[0]).as("Two Parameter - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("Two Parameter - Arg 1 Value").isEqualTo(expected1);
    }

    @Test
    public void testGetHostArgumentsWithNoBackfillWithTwoParametersWithTooManyArguments() {

        final String expected1 = "Param1";
        final String expected2 = "Param2";
        final String expected3 = "Param3";

        final Method method = getMethod("twoParam");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);
        final Value arg2 = scriptEnvironment.getContext().asValue(expected2);
        final Value arg3 = scriptEnvironment.getContext().asValue(expected3);

        final GraalVMHostArguments hostArgs = util.getHostArgumentsWithNoBackfill(scriptEnvironment, method, arg1, arg2, arg3);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Two Parameter ").isNotNull();
        assertThat(args).as("Two Parameter Length").hasSize(2);

        assertThat(args[0]).as("Two Parameter - Arg 1").isNotNull();
        assertThat(args[0]).as("Two Parameter - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("Two Parameter - Arg 1 Value").isEqualTo(expected1);

        assertThat(args[1]).as("Two Parameter - Arg 2").isNotNull();
        assertThat(args[1]).as("Two Parameter - Arg 2 Class").isInstanceOf(String.class);
        assertThat(args[1]).as("Two Parameter - Arg 2 Value").isEqualTo(expected2);
    }

    @Test
    public void testGetHostArgumentsWithNoBackfillWithTwoParametersWithVarArgs() {

        final String expected1 = "Param1";
        final String expected2 = "Param2";
        final String expected3 = "Param3";

        final Method method = getMethod("twoParamWithVarArg");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);
        final Value arg2 = scriptEnvironment.getContext().asValue(expected2);
        final Value arg3 = scriptEnvironment.getContext().asValue(expected3);

        final GraalVMHostArguments hostArgs = util.getHostArgumentsWithNoBackfill(scriptEnvironment, method, arg1, arg2, arg3);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Two Parameter with Var Args").isNotNull();
        assertThat(args).as("Two Parameter with Var Args Length").hasSize(3);

        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1").isNotNull();
        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1 Value").isEqualTo(expected1);

        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2").isNotNull();
        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2 Class").isInstanceOf(String.class);
        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2 Value").isEqualTo(expected2);

        final Object varArg = args[2];

        assertThat(varArg).as("Two Parameter with Var Arg - Arg 3").isNotNull();
        assertThat(varArg.getClass().isArray()).as("Two Parameter with Var Arg - Arg 3 Is Array").isTrue();

        final Object[] varArgs = (Object[]) varArg;

        assertThat(varArgs).as("Two Parameter with Var Arg - Arg 3 Length").hasSize(1);
        assertThat(varArgs[0]).as("Two Parameter with Var Arg - Arg 3,1 Class").isInstanceOf(String.class);
        assertThat(varArgs[0]).as("Two Parameter with Var Arg - Arg 3,1 Value").isEqualTo(expected3);
    }

    @Test
    public void testGetHostArgumentsWithNoBackfillWithTwoParametersWithVarArgsWithArray() {

        final String expected1 = "Param1";
        final String expected2 = "Param2";
        final String expected3 = "Param3";
        final String expected4 = "Param4";
        final String[] expectedArray = {expected3, expected4};

        final Method method = getMethod("twoParamWithVarArg");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);
        final Value arg2 = scriptEnvironment.getContext().asValue(expected2);
        final Value arg3 = scriptEnvironment.getContext().asValue(expectedArray);

        final GraalVMHostArguments hostArgs = util.getHostArgumentsWithNoBackfill(scriptEnvironment, method, arg1, arg2, arg3);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Two Parameter with Var Args").isNotNull();
        assertThat(args).as("Two Parameter with Var Args Length").hasSize(3);

        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1").isNotNull();
        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1 Value").isEqualTo(expected1);

        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2").isNotNull();
        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2 Class").isInstanceOf(String.class);
        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2 Value").isEqualTo(expected2);

        final Object varArg = args[2];

        assertThat(varArg).as("Two Parameter with Var Arg - Arg 3").isNotNull();
        assertThat(varArg.getClass().isArray()).as("Two Parameter with Var Arg - Arg 3 Is Array").isTrue();

        final Object[] varArgs = (Object[]) varArg;

        assertThat(varArgs).as("Two Parameter with Var Arg - Arg 3 Length").hasSize(2);
        assertThat(varArgs[0]).as("Two Parameter with Var Arg - Arg 3,1 Class").isInstanceOf(String.class);
        assertThat(varArgs[0]).as("Two Parameter with Var Arg - Arg 3,1 Value").isEqualTo(expected3);
        assertThat(varArgs[1]).as("Two Parameter with Var Arg - Arg 3,2 Class").isInstanceOf(String.class);
        assertThat(varArgs[1]).as("Two Parameter with Var Arg - Arg 3,2 Value").isEqualTo(expected4);
    }

    @Test
    public void testGetHostArgumentsWithNoBackfillWithTwoParametersWithNoVarArgs() {

        final String expected1 = "Param1";
        final String expected2 = "Param2";

        final Method method = getMethod("twoParamWithVarArg");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);
        final Value arg2 = scriptEnvironment.getContext().asValue(expected2);

        final GraalVMHostArguments hostArgs = util.getHostArgumentsWithNoBackfill(scriptEnvironment, method, arg1, arg2);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Two Parameter with Var Args").isNotNull();
        assertThat(args).as("Two Parameter with Var Args Length").hasSize(2);

        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1").isNotNull();
        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("Two Parameter with Var Arg - Arg 1 Value").isEqualTo(expected1);

        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2").isNotNull();
        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2 Class").isInstanceOf(String.class);
        assertThat(args[1]).as("Two Parameter with Var Arg - Arg 2 Value").isEqualTo(expected2);
    }

    @Test
    public void testGetHostArgumentsWithNoBackfillWithParamArrayParamVarArg() {

        final String expected1 = "Param1";
        final String expected2 = "Param2";
        final String expected3 = "Param3";
        final String[] expectedArray = {expected2, expected3};
        final String expected4 = "Param4";
        final String expected5 = "Param5";
        final String expected6 = "Param6";
        final String[] expectedVarArg = {expected5, expected6};

        final Method method = getMethod("paramArrayParamVarArg");
        final Value arg1 = scriptEnvironment.getContext().asValue(expected1);
        final Value arg2 = scriptEnvironment.getContext().asValue(expectedArray);
        final Value arg3 = scriptEnvironment.getContext().asValue(expected4);
        final Value arg4 = scriptEnvironment.getContext().asValue(expectedVarArg);

        final GraalVMHostArguments hostArgs = util.getHostArgumentsWithNoBackfill(scriptEnvironment, method, arg1, arg2, arg3, arg4);
        final Object[] args = hostArgs.getHostArgs();

        assertThat(args).as("Param Array Param Var Args").isNotNull();
        assertThat(args).as("Param Array Param Var Args Length").hasSize(4);

        assertThat(args[0]).as("Param Array Param Var Args - Arg 1").isNotNull();
        assertThat(args[0]).as("Param Array Param Var Args - Arg 1 Class").isInstanceOf(String.class);
        assertThat(args[0]).as("Param Array Param Var Args - Arg 1 Value").isEqualTo(expected1);

        final Object arrayArg = args[1];

        assertThat(arrayArg).as("Param Array Param Var Args - Arg 2").isNotNull();
        assertThat(arrayArg.getClass().isArray()).as("Param Array Param Var Args - Arg 2 Is Array").isTrue();

        final Object[] arrayArgs = (Object[]) arrayArg;

        assertThat(arrayArgs).as("Param Array Param Var Args - Arg 2 Length").hasSize(2);
        assertThat(arrayArgs[0]).as("Param Array Param Var Args - Arg 2,1").isNotNull();
        assertThat(arrayArgs[0]).as("Param Array Param Var Args - Arg 2,1 Class").isInstanceOf(String.class);
        assertThat(arrayArgs[0]).as("Param Array Param Var Args - Arg 2,1 Value").isEqualTo(expected2);
        assertThat(arrayArgs[1]).as("Param Array Param Var Args - Arg 2,2").isNotNull();
        assertThat(arrayArgs[1]).as("Param Array Param Var Args - Arg 2,2 Class").isInstanceOf(String.class);
        assertThat(arrayArgs[1]).as("Param Array Param Var Args - Arg 2,2 Value").isEqualTo(expected3);

        assertThat(args[2]).as("Param Array Param Var Args - Arg 3").isNotNull();
        assertThat(args[2]).as("Param Array Param Var Args - Arg 3 Class").isInstanceOf(String.class);
        assertThat(args[2]).as("Param Array Param Var Args - Arg 3 Value").isEqualTo(expected4);

        final Object varArg = args[3];

        assertThat(varArg).as("Param Array Param Var Args - Arg 4").isNotNull();
        assertThat(varArg.getClass().isArray()).as("Param Array Param Var Args - Arg 4 Is Array").isTrue();

        final Object[] varArgs = (Object[]) varArg;

        assertThat(varArgs).as("Param Array Param Var Args - Arg 4 Length").hasSize(2);
        assertThat(varArgs[0]).as("Param Array Param Var Args - Arg 4,1 Class").isInstanceOf(String.class);
        assertThat(varArgs[0]).as("Param Array Param Var Args - Arg 4,1 Value").isEqualTo(expected5);
        assertThat(varArgs[1]).as("Param Array Param Var Args - Arg 4,2 Class").isInstanceOf(String.class);
        assertThat(varArgs[1]).as("Param Array Param Var Args - Arg 4,2 Value").isEqualTo(expected6);
    }

    private Method getMethod(final String name) {

        final Method[] methods = klass.getMethods();


        for (final Method method : methods) {

            if (StringUtils.equals(method.getName(), name)) {
                return method;
            }

        }

        throw new IllegalStateException("Could not find method: '" + name + "'");
    }

    private void invokeMethod(final Method method, final Object[] args) throws IllegalAccessException, InvocationTargetException {

        final Boolean result = (Boolean) method.invoke(testClass, args);

        assertThat(result).as("Result").isTrue();
    }

    @SuppressWarnings({"unused", "InnerClassMayBeStatic"})
    private class ClassWithMethodsWithParameters {


        public boolean noParams() {

            return true;
        }

        public boolean oneParam(final String param) {

            return true;
        }

        public boolean oneParamWithVarArgs(final String param, final Object... param2) {

            return true;
        }

        public boolean twoParam(final String param1, final String param2) {

            return true;
        }

        public boolean twoParamWithVarArg(final String param1, final String param2, final String... param3) {

            return true;
        }

        public boolean paramArrayParamVarArg(final String param1, final String[] param2, final String param3, final String... param4) {

            return true;
        }

    }

}