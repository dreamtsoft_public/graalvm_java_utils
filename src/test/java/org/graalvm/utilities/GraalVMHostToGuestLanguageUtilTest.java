/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import static org.assertj.core.api.Assertions.*;

import java.lang.reflect.Method;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.graalvm.GraalVMWrappedObject;
import org.graalvm.dummy.DummyGraalVMScriptEnvironment;
import org.graalvm.dummy.DummyProxyObject;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyDate;
import org.graalvm.polyglot.proxy.ProxyDuration;
import org.graalvm.polyglot.proxy.ProxyInstant;
import org.graalvm.polyglot.proxy.ProxyObject;
import org.graalvm.polyglot.proxy.ProxyTime;
import org.graalvm.polyglot.proxy.ProxyTimeZone;
import org.graalvm.proxy.GraalVMProxyExecutable;
import org.graalvm.proxy.GraalVMProxyObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GraalVMHostToGuestLanguageUtilTest {

    private static final List<Integer> INT_LIST = new ArrayList<>();
    private final GraalVMHostToGuestLanguageUtil hostToGuestUtil = new GraalVMHostToGuestLanguageUtil();

    private DummyGraalVMScriptEnvironment scriptEnvironment;

    @BeforeEach
    public void setup() {

        scriptEnvironment = new DummyGraalVMScriptEnvironment();
        scriptEnvironment.initialize();

        if (CollectionUtils.isEmpty(INT_LIST)) {
            INT_LIST.add(1);
            INT_LIST.add(42);
            INT_LIST.add(3);
        }

    }

    @Test
    public void testGetObjectString() {

        final List<Object> obj = new ArrayList<>();
        obj.add(FOO);

        final String result = hostToGuestUtil.getObjectString(obj);

        assertThat(result).as("Object String").isEqualTo("[Foo]");
    }

    @Test
    public void testGetObjectStringNull() {

        assertThat(hostToGuestUtil.getObjectString(null)).as("Object String").isNull();
    }

    @Test
    public void testCreateJavascriptMemberClassString() {

        final Value value = hostToGuestUtil.createJavascriptMemberClass(scriptEnvironment, "String");

        assertThat(value).as("String Class Value").isNotNull();
        assertThat(value.canExecute()).as("String Class Value can execute").isTrue();
        assertThat(value.canInstantiate()).as("String Class Value can instantiate").isTrue();
        assertThat(value.toString()).as("String Class Value to string").isEqualTo("function String() { [native code] }");
    }

    @Test
    public void testCreateJavascriptMemberClassFunction() {

        final Value value = hostToGuestUtil.createJavascriptMemberClass(scriptEnvironment, "Function");

        assertThat(value).as("Function Class Value").isNotNull();
        assertThat(value.canExecute()).as("Function Class Value can execute").isTrue();
        assertThat(value.canInstantiate()).as("Function Class Value can instantiate").isTrue();
        assertThat(value.toString()).as("Function Class Value to string").isEqualTo("function Function() { [native code] }");
    }

    @Test
    public void testCreateJavascriptMemberClassObject() {

        final Value value = hostToGuestUtil.createJavascriptMemberClass(scriptEnvironment, "Object");

        assertThat(value).as("Object Class Value").isNotNull();
        assertThat(value.canExecute()).as("Object Class Value can execute").isTrue();
        assertThat(value.canInstantiate()).as("Object Class Value can instantiate").isTrue();
        assertThat(value.toString()).as(" Object Class Value to string").isEqualTo("function Object() { [native code] }");
    }

    @Test
    public void testCreateJavascriptMemberClassArray() {

        final Value value = hostToGuestUtil.createJavascriptMemberClass(scriptEnvironment, "Array");

        assertThat(value).as("Array Class Value").isNotNull();
        assertThat(value.canExecute()).as("Array Class Value can execute").isTrue();
        assertThat(value.canInstantiate()).as("Array Class Value can instantiate").isTrue();
        assertThat(value.toString()).as(" Array Class Value to string").isEqualTo("function Array() { [native code] }");
    }

    @Test
    public void testCreateJavascriptMemberClassNotFound() {

        final Value value = hostToGuestUtil.createJavascriptMemberClass(scriptEnvironment, FOO_BAR);

        assertThat(value).as("Not Found Member Class").isNull();
    }

    @Test
    public void testCreateNewStringWithArgs() {

        final String expected = "String{[[PrimitiveValue]]: \"Foo\"}";

        final Value value = hostToGuestUtil.createJavascriptFunctionClass(scriptEnvironment, "String", FOO);

        assertThat(value).as("String Value").isNotNull();
        assertThat(value.canExecute()).as("String Value can execute").isFalse();
        assertThat(value.canInstantiate()).as("String Value can instantiate").isFalse();
        assertThat(value.toString()).as("String Value to string").isEqualTo(expected);
    }

    @Test
    public void testCreateNewStringWithNoArgs() {

        final String expected = "String{[[PrimitiveValue]]: \"\"}";

        final Value value = hostToGuestUtil.createJavascriptFunctionClass(scriptEnvironment, "String");

        assertThat(value).as("String Value").isNotNull();
        assertThat(value.canExecute()).as("String Value can execute").isFalse();
        assertThat(value.canInstantiate()).as("String Value can instantiate").isFalse();
        assertThat(value.toString()).as("String Value to string").isEqualTo(expected);
    }

    @Test
    public void testCreateNewObjectWithArgs() {

        final String expected = "{id=42, text=42}";

        final Map<String, Object> memberMap = new HashMap<>();
        memberMap.put("id", 42);
        memberMap.put("text", "42");

        final Value value = hostToGuestUtil.createJavascriptFunctionClass(scriptEnvironment, "Object", memberMap);

        assertThat(value).as("Object Value").isNotNull();
        assertThat(value.canExecute()).as("Object Value can execute").isFalse();
        assertThat(value.canInstantiate()).as("Object Value can instantiate").isFalse();
        assertThat(value.toString()).as("Object Value to string").isEqualTo(expected);
    }

    @Test
    public void testCreateNewObjectWithNoArgs() {

        final String expected = "{}";

        final Value value = hostToGuestUtil.createJavascriptFunctionClass(scriptEnvironment, "Object");

        assertThat(value).as("Object Value").isNotNull();
        assertThat(value.canExecute()).as("Object Value can execute").isFalse();
        assertThat(value.canInstantiate()).as("Object Value can instantiate").isFalse();
        assertThat(value.toString()).as("Object Value to string").isEqualTo(expected);
    }

    @Test
    public void testCreateNewArrayWithArgs() {

        final String expected = "(3)[1, 42, 3]";

        final Value value = hostToGuestUtil.createJavascriptFunctionClass(scriptEnvironment, "Array", (Object[]) DEFAULT_INT_ARRAY);

        assertThat(value).as("Array Value").isNotNull();
        assertThat(value.canExecute()).as("Array Value can execute").isFalse();
        assertThat(value.canInstantiate()).as("Array Value can instantiate").isFalse();
        assertThat(value.toString()).as("Array Value to string").isEqualTo(expected);
    }

    @Test
    public void testCreateNewArrayWithNoArgs() {

        final String expected = "[]";

        final Value value = hostToGuestUtil.createJavascriptFunctionClass(scriptEnvironment, "Array");

        assertThat(value).as("Array Value").isNotNull();
        assertThat(value.canExecute()).as("Array Value can execute").isFalse();
        assertThat(value.canInstantiate()).as("Array Value can instantiate").isFalse();
        assertThat(value.toString()).as("Array Value to string").isEqualTo(expected);
    }

    @Test
    public void testCreateNewFunctionWithArgs() {

        final Value value = hostToGuestUtil.createJavascriptFunctionClass(scriptEnvironment, "Function", FOO, BAR, "console.log(Foo + '=' + Bar);");

        assertThat(value).as("Function Class Value").isNotNull();
        assertThat(value.canExecute()).as("Function Class Value can execute").isTrue();
        assertThat(value.canInstantiate()).as("Function Class Value can instantiate").isTrue();
        assertThat(value.toString()).as("Function Class Value to string").isEqualTo(ANON_FUNCTION_WITH_ARGS);
    }

    @Test
    public void testCreateNewFunctionWithNoArgs() {

        final Value value = hostToGuestUtil.createJavascriptFunctionClass(scriptEnvironment, "Function");

        assertThat(value).as("Function Class Value").isNotNull();
        assertThat(value.canExecute()).as("Function Class Value can execute").isTrue();
        assertThat(value.canInstantiate()).as("Function Class Value can instantiate").isTrue();
        assertThat(value.toString()).as("Function Class Value to string").isEqualTo(ANON_FUNCTION_NO_ARGS);
    }

    @Test
    public void testCreateGuestLanguageObjectClass() {

        final String expected = "{id=42, text=42}";

        final Map<String, Object> memberMap = new HashMap<>();
        memberMap.put("id", 42);
        memberMap.put("text", "42");

        final Value value = hostToGuestUtil.createGuestLanguageObjectClass(scriptEnvironment, memberMap);

        assertThat(value).as("Object Value").isNotNull();
        assertThat(value.canExecute()).as("Object Value can execute").isFalse();
        assertThat(value.canInstantiate()).as("Object Value can instantiate").isFalse();
        assertThat(value.toString()).as("Object Value to string").isEqualTo(expected);
    }

    @Test
    public void testGetValue() {

        final Object value = scriptEnvironment.getContext().asValue(FOO);

        final Value result = hostToGuestUtil.getValue(value);

        assertThat(result).as("Value").isEqualTo(value);
    }

    @Test
    public void testGetValueNotValue() {

        final Value result = hostToGuestUtil.getValue(FOO);

        assertThat(result).as("Value").isNull();
    }

    @Test
    public void testGetValueNullValue() {

        final Value result = hostToGuestUtil.getValue(null);

        assertThat(result).as("Value").isNull();
    }

    @Test
    public void testGetExecutable() throws Exception {

        final Method method = FOO.getClass().getMethod("isEmpty");
        final GraalVMProxyExecutable executable = hostToGuestUtil.getExecutable(scriptEnvironment, FOO, method);

        assertThat(executable).as("Executable").isNotNull();
    }

    @Test
    public void testGetGraalVMHandledObjectValue() {

        final Value value = scriptEnvironment.getContext().asValue(FOO);

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isSameAs(value);
    }

    @Test
    public void testGetGraalVMHandledObjectByte() {

        final Byte value = (byte) 0;

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isSameAs(value);
    }

    @Test
    public void testGetGraalVMHandledObjectShort() {

        final Short value = (short) 1;

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isSameAs(value);
    }

    @Test
    public void testGetGraalVMHandledObjectInteger() {

        final Integer value = 1;

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isSameAs(value);
    }

    @Test
    public void testGetGraalVMHandledObjectLong() {

        final Long value = 1L;

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isSameAs(value);
    }

    @Test
    public void testGetGraalVMHandledObjectFloat() {

        final Float value = 1.0f;

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isSameAs(value);
    }

    @Test
    public void testGetGraalVMHandledObjectDouble() {

        final Double value = 1.0;

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isSameAs(value);
    }

    @Test
    public void testGetGraalVMHandledObjectCharacter() {

        final Character value = 'a';

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isSameAs(value);
    }

    @Test
    public void testGetGraalVMHandledObjectString() {

        final String value = FOO;

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isSameAs(value);
    }

    @Test
    public void testGetGraalVMHandledObjectValueLocalTime() {

        final LocalTime value = LocalTime.now();

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isInstanceOf(ProxyTime.class);
        assertThat(((ProxyTime) result).asTime()).as("Local Time").isEqualTo(value);
    }

    @Test
    public void testGetGraalVMHandledObjectLocalDate() {

        final LocalDate value = LocalDate.now();

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isInstanceOf(ProxyDate.class);
        assertThat(((ProxyDate) result).asDate()).as("Local Date").isEqualTo(value);
    }

    @Test
    public void testGetGraalVMHandledObjectDate() {

        final Date value = new Date();

        final LocalDate expected = LocalDate.from(value.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isInstanceOf(ProxyDate.class);
        assertThat(((ProxyDate) result).asDate()).as("Local Date").isEqualTo(expected);
    }

    @Test
    public void testGetGraalVMHandledObjectZoneId() {

        final ZoneId value = ZoneId.of("UTC");

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isInstanceOf(ProxyTimeZone.class);
        assertThat(((ProxyTimeZone) result).asTimeZone()).as("Time Zome").isEqualTo(value);
    }

    @Test
    public void testGetGraalVMHandledObjectInstant() {

        final Instant value = Instant.now();

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isInstanceOf(ProxyInstant.class);
        assertThat(((ProxyInstant) result).asInstant()).as("Instant").isEqualTo(value);
    }

    @Test
    public void testGetGraalVMHandledObjectLocalDateTime() {

        final LocalDateTime value = LocalDateTime.now();

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isInstanceOf(ProxyInstant.class);
        assertThat(((ProxyInstant) result).asDate()).as("Local Date").isEqualTo(value.toLocalDate());
        assertThat(((ProxyInstant) result).asTime()).as("Local Time").isEqualTo(value.toLocalTime());
    }

    @Test
    public void testGetGraalVMHandledObjectZonedDateTime() {

        final ZonedDateTime value = ZonedDateTime.now();

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isInstanceOf(ProxyInstant.class);
        assertThat(((ProxyInstant) result).asInstant().atZone(ZoneId.systemDefault())).as("Time Zone").isEqualTo(value);
    }

    @Test
    public void testGetGraalVMHandledObjectDuration() {

        final Duration value = Duration.ofDays(1);

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isInstanceOf(ProxyDuration.class);
        assertThat(((ProxyDuration) result).asDuration()).as("Duration").isEqualTo(value);
    }

    @Test
    public void testGetGraalVMHandledObjectProxy() {

        final DummyProxyObject value = new DummyProxyObject();

        final Object result = hostToGuestUtil.getGraalVMHandledObject(value);

        assertThat(result).as("GraalVM Handled Object").isSameAs(value);
    }

    @Test
    public void testGetGraalVMHandledObjectDefault() {

        final List<Object> obj = new ArrayList<>();
        obj.add(FOO);

        final Object result = hostToGuestUtil.getGraalVMHandledObject(obj);

        assertThat(result).as("GraalVM Handled Object").isEqualTo("[Foo]");
    }

    @Test
    public void testCreateMapOfGuestLanguage() {

        final Map<Object, Object> valueMap = new HashMap<>();
        valueMap.put(FOO, FOO);
        valueMap.put(BAR, BAR);

        final Object result = hostToGuestUtil.createMapOfGuestLanguage(scriptEnvironment, valueMap);

        assertThat(result).as("Guest Language Map").isInstanceOf(Value.class);

        final Value resultValue = (Value) result;

        assertThat(resultValue.hasMembers()).as("Result Value has Members").isTrue();
        assertThat(resultValue.hasMember(FOO)).as("Result Value Has Member Foo").isTrue();
        assertThat(resultValue.getMember(FOO).asString()).as("Result Value Member - Foo").isEqualTo(FOO);
        assertThat(resultValue.hasMember(BAR)).as("Result Value Has Member Bar").isTrue();
        assertThat(resultValue.getMember(BAR).asString()).as("Result Value Member - Bar").isEqualTo(BAR);
    }

    @Test
    public void testCreateIterableOfGuestLanguage() {

        final List<Object> valueList = new ArrayList<>();
        valueList.add(FOO);
        valueList.add(BAR);

        final Object result = hostToGuestUtil.createIterableOfGuestLanguage(scriptEnvironment, valueList);

        assertThat(result).as("Guest Language Map").isInstanceOf(Value.class);

        final Value resultValue = (Value) result;

        assertThat(resultValue.hasArrayElements()).as("Result Value has Array Elements").isTrue();
        assertThat(resultValue.getArraySize()).as("Result Value size").isEqualTo(2);
        assertThat(resultValue.getArrayElement(0).asString()).as("Result Value - 0").isEqualTo(FOO);
        assertThat(resultValue.getArrayElement(1).asString()).as("Result Value - 1").isEqualTo(BAR);

    }

    @Test
    public void testCreateIteratorOfGuestLanguage() {

        final List<Object> valueList = new ArrayList<>();
        valueList.add(FOO);
        valueList.add(BAR);

        final Object result = hostToGuestUtil.createIteratorOfGuestLanguage(scriptEnvironment, valueList.iterator());

        assertThat(result).as("Guest Language Map").isInstanceOf(Value.class);

        final Value resultValue = (Value) result;

        assertThat(resultValue.hasArrayElements()).as("Result Value has Array Elements").isTrue();
        assertThat(resultValue.getArraySize()).as("Result Value size").isEqualTo(2);
        assertThat(resultValue.getArrayElement(0).asString()).as("Result Value - 0").isEqualTo(FOO);
        assertThat(resultValue.getArrayElement(1).asString()).as("Result Value - 1").isEqualTo(BAR);
    }

    @Test
    public void testCreateArrayOfGuestLanguage() {

        final List<Object> valueList = new ArrayList<>();
        valueList.add(FOO);
        valueList.add(BAR);

        final Object result = hostToGuestUtil.createArrayOfGuestLanguage(scriptEnvironment, valueList.toArray());

        assertThat(result).as("Guest Language Map").isInstanceOf(Value.class);

        final Value resultValue = (Value) result;

        assertThat(resultValue.hasArrayElements()).as("Result Value has Array Elements").isTrue();
        assertThat(resultValue.getArraySize()).as("Result Value size").isEqualTo(2);
        assertThat(resultValue.getArrayElement(0).asString()).as("Result Value - 0").isEqualTo(FOO);
        assertThat(resultValue.getArrayElement(1).asString()).as("Result Value - 1").isEqualTo(BAR);
    }

    @Test
    public void testCreateGuestLanguageObjectNull() {

        final Object result = hostToGuestUtil.createGuestLanguageObject(scriptEnvironment, null);

        assertThat(result).as("Guest Language Object").isNotNull();
        assertThat(result).as("Guest Language Object").isInstanceOf(GraalVMWrappedObject.class);

        final GraalVMWrappedObject wrappedObject = (GraalVMWrappedObject) result;

        assertThat(wrappedObject.getValue().isNull()).as("Guest Language Object").isTrue();
    }

    @Test
    public void testCreateGuestLanguageObjectValue() {

        final Value value = scriptEnvironment.getContext().asValue(FOO);

        final Object result = hostToGuestUtil.createGuestLanguageObject(scriptEnvironment, value);

        assertThat(result).as("Guest Language Object").isSameAs(value);
    }

    @Test
    public void testCreateGuestLanguageObjectArray() {

        final List<Object> valueList = new ArrayList<>();
        valueList.add(FOO);
        valueList.add(BAR);

        final Object result = hostToGuestUtil.createGuestLanguageObject(scriptEnvironment, valueList.toArray());

        assertThat(result).as("Guest Language Map").isInstanceOf(Value.class);

        final Value resultValue = (Value) result;

        assertThat(resultValue.hasArrayElements()).as("Result Value has Array Elements").isTrue();
    }

    @Test
    public void testCreateGuestLanguageObjectProxyObject() {

        final Map<String, Object> valueMap = new HashMap<>();
        valueMap.put(FOO, FOO);
        valueMap.put(BAR, BAR);

        final Object result = hostToGuestUtil.createGuestLanguageObject(scriptEnvironment, ProxyObject.fromMap(valueMap));

        assertThat(result).as("Guest Language Map").isInstanceOf(GraalVMProxyObject.class);
    }

    @Test
    public void testCreateGuestLanguageObjectProxyIterator() {

        final List<Object> valueList = new ArrayList<>();
        valueList.add(FOO);
        valueList.add(BAR);

        final Object result = hostToGuestUtil.createIteratorOfGuestLanguage(scriptEnvironment, valueList.iterator());

        assertThat(result).as("Guest Language Map").isInstanceOf(Value.class);

        final Value resultValue = (Value) result;

        assertThat(resultValue.hasArrayElements()).as("Result Value has Array Elements").isTrue();
    }

    @Test
    public void testCreateGuestLanguageObjectIterableMap() {

        final List<Object> valueList = new ArrayList<>();
        valueList.add(FOO);
        valueList.add(BAR);

        final Object result = hostToGuestUtil.createGuestLanguageObject(scriptEnvironment, valueList);

        assertThat(result).as("Guest Language Map").isInstanceOf(Value.class);

        final Value resultValue = (Value) result;

        assertThat(resultValue.hasArrayElements()).as("Result Value has Array Elements").isTrue();
    }

    @Test
    public void testCreateGuestLanguageObject() {

        final Object result = hostToGuestUtil.createGuestLanguageObject(scriptEnvironment, FOO);

        assertThat(result).as("Guest Language Object").isInstanceOf(String.class);
        assertThat(result).as("Guest Language Object").isEqualTo(FOO);
    }


    @Test
    public void testCreateGuestLanguageObjects() {

        final Object[] args = {"Foo", "Bar"};

        final Object[] values = hostToGuestUtil.createGuestLanguageObjects(scriptEnvironment, args);

        assertThat(values).as("Arg Values").isNotNull();
        assertThat(values).as("Arg Values").hasSize(2);
    }


    public static final String FOO = "Foo";

    public static final String BAR = "Bar";

    public static final String FOO_BAR = FOO + BAR;

    private static final Integer[] DEFAULT_INT_ARRAY = {1, 42, 3};

    private static final String ANON_FUNCTION_WITH_ARGS = """
            function anonymous(Foo,Bar
            ) {
            console.log(Foo + '=' + Bar);
            }""";

    private static final String ANON_FUNCTION_NO_ARGS = """
            function anonymous(
            ) {
                        
            }""";

}