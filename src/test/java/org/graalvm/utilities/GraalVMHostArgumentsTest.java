/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.graalvm.GraalVMRuntimeException;
import org.graalvm.dummy.DummyGraalVMScriptEnvironment;
import org.graalvm.polyglot.Value;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GraalVMHostArgumentsTest {

    private static final String FOO = "foo";
    private static final String BAR = "bar";

    private DummyGraalVMScriptEnvironment scriptEnvironment;

    @BeforeEach
    public void setup() {

        scriptEnvironment = new DummyGraalVMScriptEnvironment();
        scriptEnvironment.initialize();
    }

    @Test
    public void testGetHostArgsWithEmptyArgs() {

        final GraalVMHostArguments hostArguments = new GraalVMHostArguments(scriptEnvironment, 0);

        final Object[] hostArg = hostArguments.getHostArgs();

        assertThat(hostArg).as("Host Args").isNotNull();
        assertThat(hostArg).as("Host Args Size").hasSize(0);
    }

    @Test
    public void testGetHostArgsWithOneArg() {

        final GraalVMHostArguments hostArguments = new GraalVMHostArguments(scriptEnvironment, 1);
        hostArguments.addImmutableArg(0, FOO);

        final Object[] hostArgs = hostArguments.getHostArgs();

        assertThat(hostArgs).as("Host Args").isNotNull();
        assertThat(hostArgs).as("Host Args Size").hasSize(1);
        assertThat(hostArgs[0]).as("Host Arg - 0").isEqualTo(FOO);
    }

    @Test
    public void testGetHostArgsWithTwoArgs() {

        final GraalVMHostArguments hostArguments = new GraalVMHostArguments(scriptEnvironment, 2);
        hostArguments.addImmutableArg(0, FOO);
        hostArguments.addImmutableArg(1, BAR);

        final Object[] hostArgs = hostArguments.getHostArgs();

        assertThat(hostArgs).as("Host Args").isNotNull();
        assertThat(hostArgs).as("Host Args Size").hasSize(2);
        assertThat(hostArgs[0]).as("Host Arg - 0").isEqualTo(FOO);
        assertThat(hostArgs[1]).as("Host Arg - 1").isEqualTo(BAR);
    }

    @Test
    public void testGetHostArgsWithArray() {

        final GraalVMHostArguments arrayArguments = new GraalVMHostArguments(scriptEnvironment, 2);
        arrayArguments.addImmutableArg(0, FOO);
        arrayArguments.addImmutableArg(1, BAR);

        final GraalVMHostArguments hostArguments = new GraalVMHostArguments(scriptEnvironment, 1);
        hostArguments.addImmutableArg(0, arrayArguments);

        final Object[] hostArgs = hostArguments.getHostArgs();

        assertThat(hostArgs).as("Host Args").isNotNull();
        assertThat(hostArgs).as("Host Args Size").hasSize(1);

        final Object[] arrayArgs = (Object[]) hostArgs[0];

        assertThat(arrayArgs).as("Array Args").isNotNull();
        assertThat(arrayArgs).as("Array Args Size").hasSize(2);
        assertThat(arrayArgs[0]).as("Array Arg - 0").isEqualTo(FOO);
        assertThat(arrayArgs[1]).as("Array Arg - 1").isEqualTo(BAR);
    }

    @Test
    public void testGetHostArgsWithVarArg() {

        final GraalVMHostArguments arrayArguments = new GraalVMHostArguments(scriptEnvironment, 2);
        arrayArguments.addImmutableArg(0, FOO);
        arrayArguments.addImmutableArg(1, BAR);

        final GraalVMHostArguments hostArguments = new GraalVMHostArguments(scriptEnvironment, 3);
        hostArguments.addImmutableArg(0, FOO);
        hostArguments.addImmutableArg(1, BAR);
        hostArguments.addImmutableArg(2, arrayArguments);

        final Object[] hostArgs = hostArguments.getHostArgs();

        assertThat(hostArgs).as("Host Args").isNotNull();
        assertThat(hostArgs).as("Host Args Size").hasSize(3);
        assertThat(hostArgs[0]).as("Host Arg - 0").isEqualTo(FOO);
        assertThat(hostArgs[1]).as("Host Arg - 1").isEqualTo(BAR);

        final Object[] arrayArgs = (Object[]) hostArgs[2];

        assertThat(arrayArgs).as("Array Args").isNotNull();
        assertThat(arrayArgs).as("Array Args Size").hasSize(2);
        assertThat(arrayArgs[0]).as("Array Arg - 0").isEqualTo(FOO);
        assertThat(arrayArgs[1]).as("Array Arg - 1").isEqualTo(BAR);
    }

    @Test
    public void testGetHostArgTypes() {

        final GraalVMHostArguments arrayArguments = new GraalVMHostArguments(scriptEnvironment, 2);
        arrayArguments.addImmutableArg(0, FOO);
        arrayArguments.addImmutableArg(1, BAR);

        final Class<?> expectedArrayType = arrayArguments.getHostArgs().getClass().arrayType();

        final GraalVMHostArguments hostArguments = new GraalVMHostArguments(scriptEnvironment, 3);
        hostArguments.addImmutableArg(0, FOO);
        hostArguments.addImmutableArg(1, null);
        hostArguments.addImmutableArg(2, arrayArguments);

        final Class<?>[] hostArgTypes = hostArguments.getHostArgTypes();

        assertThat(hostArgTypes).as("Host Args").isNotNull();
        assertThat(hostArgTypes).as("Host Args Size").hasSize(3);
        assertThat(hostArgTypes[0]).as("Host Arg - 0").isEqualTo(String.class);
        assertThat(hostArgTypes[1]).as("Host Arg - 1").isNull();
        assertThat(hostArgTypes[2].isArray()).as("Host Arg - 2").isTrue();
        assertThat(hostArgTypes[2]).as("Host Arg - 0").isEqualTo(expectedArrayType);
    }

    @Test
    public void testTranslationMap() {

        final Map<String, Object> arg1 = new HashMap<>();
        final Value origArg1 = createValueMap(new HashMap<>());

        final Map<String, Object> arg2 = new HashMap<>();
        final Value origArg2 = createValueMap(new HashMap<>());

        final GraalVMHostArguments.TranslationMap transMap1 = new GraalVMHostArguments.TranslationMap(arg1, origArg1);
        final GraalVMHostArguments.TranslationMap transMap2 = new GraalVMHostArguments.TranslationMap(arg2, origArg2);

        assertThat(transMap1).as("Translation Maps").isNotEqualTo(transMap2);
        assertThat(transMap1.getArg()).as("Host Arg 1").isEqualTo(arg1);
        assertThat(transMap1.getValue()).as("Guest Arg 1").isEqualTo(origArg1);
        assertThat(transMap2.getArg()).as("Host Arg 2").isEqualTo(arg2);
        assertThat(transMap2.getValue()).as("Guest Arg 2").isEqualTo(origArg2);
    }

    @Test
    public void testValidateIndexValid() {

        final Map<String, Object> arg = new HashMap<>();

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnvironment, 1);
        hostArgs.validateIndex(0, arg);

        // This test is to show that no exceptions will be thrown if both the arg and original arg are null.
    }

    @Test
    public void testValidateIndexInvalid() {

        final Map<String, Object> arg = new HashMap<>();

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnvironment, 1);

        assertThrows(GraalVMRuntimeException.class, () -> hostArgs.validateIndex(1, arg));
    }

    @Test
    public void testAddImmutableArg() {

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnvironment, 1);
        hostArgs.addImmutableArg(0, FOO);

        final Object[] hostArg = hostArgs.getHostArgs();

        assertThat(hostArg).as("Host Args").isNotNull();
        assertThat(hostArg).as("Host Args").hasSize(1);
        assertThat(hostArg[0]).as("Host Args").isEqualTo(FOO);

        assertThat(hostArgs.getTranslations()).as("Translations").isEmpty();
    }

    @Test
    public void testAddMutableArg() {

        final String key = "name";
        final String origStringValue = "FooBar";

        final Map<String, Object> origMap = new HashMap<>();
        origMap.put(key, origStringValue);

        final Value origValueArg = createValueMap(origMap);

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnvironment, 1);
        hostArgs.addMutableArg(0, origMap, origValueArg);

        final Object[] hostArg = hostArgs.getHostArgs();

        assertThat(hostArg).as("Host Args").isNotNull();
        assertThat(hostArg).as("Host Args").hasSize(1);
        assertThat(hostArg[0]).as("Host Args").isEqualTo(origMap);

        assertThat(hostArgs.getTranslations()).as("Translations").hasSize(1);

        final GraalVMHostArguments.TranslationMap transMap = hostArgs.getTranslations().get(0);

        assertThat(transMap).as("Translation Map").isNotNull();
        assertThat(transMap.getArg()).as("Translation Map Arg").isEqualTo(origMap);
        assertThat(transMap.getValue()).as("Translation Map Value").isEqualTo(origValueArg);
    }

    @Test
    public void testTranslateFromHostToGuestWithMapWithStringKeyValue() {

        final String key = "name";
        final String origStringValue = "FooBar";
        final String newStringValue = "BarFoo";

        final Map<String, Object> origMap = new HashMap<>();
        origMap.put(key, origStringValue);

        final Value origArg = createValueMap(origMap);

        final Map<String, Object> arg = new HashMap<>();
        arg.put(key, newStringValue);

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnvironment, 1);
        hostArgs.translateFromHostToGuest(arg, origArg);

        final Value nameMember = origArg.getMember(key);

        assertThat(nameMember.isString()).as("Name Entry String").isTrue();
        assertThat(nameMember.asString()).as("Name Entry").isEqualTo(newStringValue);
    }

    @Test
    public void testTranslateFromHostToGuestWithMapWithStringKeyMapValue() {


        final String data = "data";

        final Map<String, Object> origDataMap = new HashMap<>();

        final Map<String, Object> origMapValue = new HashMap<>();
        origMapValue.put(FOO, BAR);
        origMapValue.put(data, origDataMap);

        final Value origArg = createValueMap(origMapValue);

        final Map<String, Object> newDataMap = new HashMap<>();
        newDataMap.put(FOO, FOO);
        newDataMap.put(BAR, BAR);

        final Map<String, Object> arg = new HashMap<>();
        arg.put(FOO, FOO);
        arg.put(BAR, BAR);
        arg.put(data, newDataMap);

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnvironment, 1);
        hostArgs.translateFromHostToGuest(arg, origArg);

        final Value fooMember = origArg.getMember(FOO);

        assertThat(fooMember).as("Foo Member").isNotNull();
        assertThat(fooMember.isString()).as("Foo Member is String").isTrue();
        assertThat(fooMember.asString()).as("Foo Member value").isEqualTo(FOO);

        final Value barMember = origArg.getMember(BAR);

        assertThat(barMember).as("Bar Member").isNotNull();
        assertThat(barMember.isString()).as("Bar Member is String").isTrue();
        assertThat(barMember.asString()).as("Bar Member value").isEqualTo(BAR);

        final Value dataMember = origArg.getMember(data);
        assertThat(dataMember).as("Data Member").isNotNull();
        assertThat(dataMember.hasMembers()).as("Data Member has members").isTrue();
        assertThat(dataMember.hasMember(FOO)).as("Data Member has foo").isTrue();
        assertThat(dataMember.hasMember(BAR)).as("Data Member has bar").isTrue();

        final Value newFooMember = dataMember.getMember(FOO);

        assertThat(newFooMember).as("Data Member - New Foo Member").isNotNull();
        assertThat(newFooMember.isString()).as("Data Member - New Foo Member is String").isTrue();
        assertThat(newFooMember.asString()).as("Data Member - New Foo Member value").isEqualTo(FOO);

        final Value newBarMember = dataMember.getMember(BAR);

        assertThat(newBarMember).as("Data Member - New Bar Member").isNotNull();
        assertThat(newBarMember.isString()).as("Data Member - New Bar Member is String").isTrue();
        assertThat(newBarMember.asString()).as("Data Member - New Bar Member value").isEqualTo(BAR);
    }

    @Test
    public void testTranslateFromHostToGuestWithArrayWithMap() {

        final String script = """
                (function() {
                    return ['foo', 'bar', {foo: 'foo', bar: 'bar'}, ['foo', 'bar']];
                })
                """;
        final Value scriptValue = scriptEnvironment.getContext().eval(GraalVMScriptConstants.SCRIPT_TYPE_JS, script);
        final Value origArg = scriptValue.execute();

        final String newFoo = "new foo";
        final String newBar = "new bar";

        final Map<String, Object> newArrayArgMap = new HashMap<>();
        newArrayArgMap.put(FOO, newFoo);
        newArrayArgMap.put(BAR, newBar);

        final Object[] arg = new Object[4];
        arg[0] = newFoo;
        arg[1] = newBar;
        arg[2] = newArrayArgMap;
        arg[3] = new String[]{newFoo, newBar};

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnvironment, 1);
        hostArgs.translateFromHostToGuest(arg, origArg);

        assertThat(origArg.hasArrayElements()).as("Original Arg still has Array Elements").isTrue();
        assertThat(origArg.getArraySize()).as("Original Arg Array Size").isEqualTo(4);

        Value arrayValue = origArg.getArrayElement(0);

        assertThat(arrayValue).as("Array Value @ index 0").isNotNull();
        assertThat(arrayValue.isString()).as("Array Value @ index 0 is String").isTrue();
        assertThat(arrayValue.asString()).as("Array Value @ index 0 value").isEqualTo(newFoo);

        arrayValue = origArg.getArrayElement(1);

        assertThat(arrayValue).as("Array Value @ index 1").isNotNull();
        assertThat(arrayValue.isString()).as("Array Value @ index 1 is String").isTrue();
        assertThat(arrayValue.asString()).as("Array Value @ index 1 value").isEqualTo(newBar);

        arrayValue = origArg.getArrayElement(2);

        assertThat(arrayValue).as("Array Value @ index 1").isNotNull();
        assertThat(arrayValue.hasMembers()).as("Array Value @ index 2 has members").isTrue();
        assertThat(arrayValue.hasMember(FOO)).as("Array Value @ index 2 has member - foo").isTrue();
        assertThat(arrayValue.getMember(FOO).isString()).as("Array Value @ index 2 has member - foo is a String").isTrue();
        assertThat(arrayValue.getMember(FOO).asString()).as("Array Value @ index 2 has member - foo value").isEqualTo(newFoo);
        assertThat(arrayValue.hasMember(BAR)).as("Array Value @ index 2 has member - bar").isTrue();
        assertThat(arrayValue.getMember(BAR).isString()).as("Array Value @ index 2 has member - bar is a String").isTrue();
        assertThat(arrayValue.getMember(BAR).asString()).as("Array Value @ index 2 has member - bar value").isEqualTo(newBar);

        arrayValue = origArg.getArrayElement(3);

        assertThat(arrayValue).as("Array Value @ index 3").isNotNull();
        assertThat(arrayValue.hasArrayElements()).as("Array Value @ index 3 has array elements").isTrue();
        assertThat(arrayValue.getArraySize()).as("Array Value @ index 3 has size").isEqualTo(2);

        Value subArrayValue = arrayValue.getArrayElement(0);

        assertThat(subArrayValue).as("Array Value @ index 1, sub index 0").isNotNull();
        assertThat(subArrayValue.isString()).as("Array Value @ index 1, sub index 0  is String").isTrue();
        assertThat(subArrayValue.asString()).as("Array Value @ index 1, sub index 0 value").isEqualTo(newFoo);

        subArrayValue = arrayValue.getArrayElement(1);

        assertThat(subArrayValue).as("Array Value @ index 1, sub index 1").isNotNull();
        assertThat(subArrayValue.isString()).as("Array Value @ index 1, sub index 1  is String").isTrue();
        assertThat(subArrayValue.asString()).as("Array Value @ index 1, sub index 1 value").isEqualTo(newBar);
    }

    @Test
    public void testTranslateWithNull() {

        final GraalVMHostArguments.TranslationMap transMap = new GraalVMHostArguments.TranslationMap(null, null);

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnvironment, 1);
        hostArgs.translate(transMap);

        // This test is to show that no exceptions will be thrown if both the arg and original arg are null.
    }

    @Test
    public void testTranslateWithNullArgAndNotNullOriginalArg() {

        final Value origArg = scriptEnvironment.getContext().asValue(FOO);

        final GraalVMHostArguments.TranslationMap transMap = new GraalVMHostArguments.TranslationMap(null, origArg);

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnvironment, 1);

        assertThrows(GraalVMRuntimeException.class, () -> hostArgs.translate(transMap));
    }

    @Test
    public void testTranslate() {

        final String key = "name";
        final String origStringValue = "FooBar";
        final String newStringValue = "BarFoo";

        final Map<String, Object> origMap = new HashMap<>();
        origMap.put(key, origStringValue);

        final Value origArg = createValueMap(origMap);

        final Map<String, Object> arg = new HashMap<>();
        arg.put(key, newStringValue);

        final GraalVMHostArguments.TranslationMap transMap = new GraalVMHostArguments.TranslationMap(arg, origArg);

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnvironment, 1);
        hostArgs.translate(transMap);

        final Value nameMember = origArg.getMember(key);

        assertThat(nameMember.isString()).as("Name Entry String").isTrue();
        assertThat(nameMember.asString()).as("Name Entry").isEqualTo(newStringValue);
    }


    @Test
    public void testTranslateWithVarArg() {

        final String key = "name";
        final String origStringValue = "FooBar";
        final String newStringValue = "BarFoo";

        final Map<String, Object> origMap = new HashMap<>();
        origMap.put(key, origStringValue);

        final Value origArg = createValueMap(origMap);

        final Map<String, Object> arg = new HashMap<>();
        arg.put(key, newStringValue);

        final GraalVMHostArguments hostVarArgs = new GraalVMHostArguments(scriptEnvironment, 1);
        hostVarArgs.addMutableArg(0, arg, origArg);

        final GraalVMHostArguments.TranslationMap transMap = new GraalVMHostArguments.TranslationMap(hostVarArgs, null);

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnvironment, 1);
        hostArgs.translate(transMap);

        final Value nameMember = origArg.getMember(key);

        assertThat(nameMember.isString()).as("Name Entry String").isTrue();
        assertThat(nameMember.asString()).as("Name Entry").isEqualTo(newStringValue);
    }

    @Test
    public void testDoTranslations() {

        final String key = "name";
        final String origStringValue = "FooBar";
        final String newStringValue = "BarFoo";

        final Map<String, Object> origMap = new HashMap<>();
        origMap.put(key, origStringValue);

        final Value origValueArg = createValueMap(origMap);

        final GraalVMHostArguments hostArgs = new GraalVMHostArguments(scriptEnvironment, 1);
        hostArgs.addMutableArg(0, origMap, origValueArg);

        origMap.put(key, newStringValue);

        hostArgs.doTranslations();

        final Value nameMember = origValueArg.getMember(key);

        assertThat(nameMember.isString()).as("Name Entry String").isTrue();
        assertThat(nameMember.asString()).as("Name Entry").isEqualTo(newStringValue);
    }

    private Value createValueMap(final Map<String, Object> map) {

        final Value objectPrototype = scriptEnvironment.getBindings().getMember(GraalVMScriptConstants.OBJECT_CLASS_NAME);
        final Value valueMap = objectPrototype.newInstance();

        for (Map.Entry<String, Object> entry : map.entrySet()) {
            valueMap.putMember(entry.getKey(), entry.getValue());
        }

        return valueMap;
    }


}