/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import static org.assertj.core.api.Assertions.*;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.graalvm.GraalVMWrappedObject;
import org.graalvm.dummy.DummyGraalVMScriptEnvironment;
import org.graalvm.dummy.DummyProxyObject;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.Proxy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@SuppressWarnings({"rawtypes", "unchecked"})
public class GraalVMGuestLanguageToHostUtilTest {

    private static final List<Integer> INT_LIST = new ArrayList<>();
    private final GraalVMGuestLanguageToHostUtil guestToHostUtil = new GraalVMGuestLanguageToHostUtil();
    private DummyGraalVMScriptEnvironment scriptEnvironment;

    @BeforeEach
    public void setup() {

        scriptEnvironment = new DummyGraalVMScriptEnvironment();
        scriptEnvironment.initialize();

        if (CollectionUtils.isEmpty(INT_LIST)) {
            INT_LIST.add(1);
            INT_LIST.add(42);
            INT_LIST.add(3);
        }

    }

    @Test
    public void testGetString() {

        final String expected = "FooBar";

        final String result = guestToHostUtil.getString(expected);

        assertThat(result).as("Get String").isEqualTo(expected);
    }

    @Test
    public void testGetStringEmptyString() {

        final String expected = "";

        final String result = guestToHostUtil.getString(expected);

        assertThat(result).as("Get String").isEqualTo(expected);
    }

    @Test
    public void testGetStringNullString() {

        final String result = guestToHostUtil.getString("null");

        assertThat(result).as("Get String").isNull();
    }

    @Test
    public void testGetStringNull() {

        final String result = guestToHostUtil.getString(null);

        assertThat(result).as("Get String").isNull();
    }

    @Test
    public void testAddGuestElementsFromIterator() {

        final Value value = scriptEnvironment.getContext().eval(JS, JS_ITERATOR);
        final ArrayList<Object> iteratorList = new ArrayList<>();

        guestToHostUtil.addGuestElementsFromIterator(scriptEnvironment, value, iteratorList);

        assertThat(iteratorList).as("Iterator List").hasSize(3);
        assertThat(iteratorList.get(0)).as("Element - 0").isEqualTo(1);
        assertThat(iteratorList.get(1)).as("Element - 0").isEqualTo(2);
        assertThat(iteratorList.get(2)).as("Element - 0").isEqualTo(3);
    }


    @Test
    public void testAddHostObjectsFromIterator() {

        final Value value = scriptEnvironment.getContext().asValue(INT_LIST.iterator());
        final ArrayList<Object> iteratorList = new ArrayList<>();

        guestToHostUtil.addHostObjectsFromIterator(scriptEnvironment, value, iteratorList);

        assertThat(iteratorList).as("Iterator List").hasSize(3);
        assertThat(iteratorList.get(0)).as("Element - 0").isEqualTo(1);
        assertThat(iteratorList.get(1)).as("Element - 0").isEqualTo(42);
        assertThat(iteratorList.get(2)).as("Element - 0").isEqualTo(3);
    }


    @Test
    public void testGetObjectIterator() {

        final List<Object> objList = new ArrayList<>();
        objList.add("1");
        objList.add(scriptEnvironment.getContext().asValue("2"));
        objList.add(3);

        final Iterator<Object> objIt = guestToHostUtil.getObjectIterator(scriptEnvironment, objList.iterator());

        assertThat(objIt.hasNext()).as("Iterator has Next").isTrue();
        int count = 0;

        while (objIt.hasNext()) {
            final Object val = objIt.next();

            if (val instanceof final String valString) {
                assertThat(valString).as("Host String").isIn("1", "2");
                count++;
            } else if (val instanceof final Integer valInt) {
                assertThat(valInt).as("Host Integer").isEqualTo(3);
                count++;
            }

        }

        assertThat(count).as("Iterator Size").isEqualTo(3);
    }

    @Test
    public void testGetObjectList() {

        final List<Object> objList = new ArrayList<>();
        objList.add("1");
        objList.add(scriptEnvironment.getContext().asValue("2"));
        objList.add(3);

        final List<Object> result = guestToHostUtil.getObjectList(scriptEnvironment, objList);

        assertThat(result).as("Iterator has Next").hasSize(3);

        for (final Object val : result) {

            if (val instanceof final String valString) {
                assertThat(valString).as("Host String").isIn("1", "2");
            } else if (val instanceof final Integer valInt) {
                assertThat(valInt).as("Host Integer").isEqualTo(3);
            }

        }

    }

    @Test
    public void testGetObjectMap() {

        final Map<Object, Object> objMap = new HashMap<>();
        objMap.put("#1", "1");
        objMap.put("#2", scriptEnvironment.getContext().asValue("2"));
        objMap.put("#3", 3);

        final Map<Object, Object> result = guestToHostUtil.getObjectMap(scriptEnvironment, objMap);

        assertThat(result).as("Iterator has Next").hasSize(3);

        for (final Object key : result.keySet()) {
            assertThat(key).as("Key").isIn("#1", "#2", "#3");

            final Object val = result.get(key);

            if (val instanceof final String valString) {
                assertThat(valString).as("Host String").isIn("1", "2");
            } else if (val instanceof final Integer valInt) {
                assertThat(valInt).as("Host Integer").isEqualTo(3);
            }

        }

    }

    @Test
    public void testGetHostObjMap() {

        final List<Object> hostObjectList = new ArrayList<>();
        hostObjectList.add(FOO);
        hostObjectList.add(scriptEnvironment.getContext().asValue(FOO));

        final Map<String, Object> hostObjectMap = new HashMap<>();
        hostObjectMap.put("text", "42");
        hostObjectMap.put("list", hostObjectList);

        final Object hostObj = guestToHostUtil.getHostObj(scriptEnvironment, hostObjectMap);

        assertThat(hostObj).as("Host Object Instance Of").isInstanceOf(Map.class);

        final Map<Object, Object> returnMap = (Map<Object, Object>) hostObj;

        assertThat(returnMap.size()).as("Host Object Map Size").isEqualTo(hostObjectMap.size());
        assertThat(returnMap.get("text")).as("Host Object Map - text").isEqualTo("42");

        final Object returnHostObjText = returnMap.get("list");

        assertThat(returnHostObjText).as("Host Object Map - list").isInstanceOf(List.class);

        final List<Object> returnHostObjList = (List<Object>) returnHostObjText;

        assertThat(returnHostObjList.size()).as("Host Object List Size").isEqualTo(hostObjectList.size());

        returnHostObjList.forEach(entry -> assertThat(entry).as("Return Host Object").isEqualTo(FOO));
    }

    @Test
    public void testGetHostObjList() {

        final List<Object> hostObjectList = new ArrayList<>();
        hostObjectList.add(FOO);
        hostObjectList.add(scriptEnvironment.getContext().asValue(FOO));

        final Object hostObj = guestToHostUtil.getHostObj(scriptEnvironment, hostObjectList);

        assertThat(hostObj).as("Host Object Instance Of").isInstanceOf(List.class);

        final List<Object> returnHostObjList = (List<Object>) hostObj;

        assertThat(returnHostObjList.size()).as("Host Object List Size").isEqualTo(hostObjectList.size());

        returnHostObjList.forEach(entry -> assertThat(entry).as("Return Host Object").isEqualTo(FOO));
    }

    @Test
    public void testGetHostObjIterator() {

        final List<Object> hostObjectList = new ArrayList<>();
        hostObjectList.add(FOO);
        hostObjectList.add(scriptEnvironment.getContext().asValue(FOO));

        final Object hostObj = guestToHostUtil.getHostObj(scriptEnvironment, hostObjectList.iterator());

        assertThat(hostObj).as("Host Object Instance Of").isInstanceOf(Iterator.class);

        final Iterator<Object> returnHostObjList = (Iterator<Object>) hostObj;

        assertThat(returnHostObjList.hasNext()).as("Host Object Iterator").isTrue();

        while (returnHostObjList.hasNext()) {
            assertThat(returnHostObjList.next()).as("Return Host Object").isEqualTo(FOO);
        }

    }

    @Test
    public void testGetHostObj() {

        final Object returnObj = guestToHostUtil.getHostObj(scriptEnvironment, FOO);

        assertThat(returnObj).as("Get Host Obj return existing").isEqualTo(FOO);
    }


    @Test
    public void testGetUnwrappedObject() {

        final Value value = scriptEnvironment.getContext().asValue(FOO);
        final GraalVMWrappedObject scriptObject = new GraalVMWrappedObject(scriptEnvironment, value);

        final Object result = guestToHostUtil.getUnwrappedObject(scriptEnvironment, scriptObject);

        assertThat(result).as("Unwrapped Object").isEqualTo(FOO);
    }

    @Test
    public void testGetUnwrappedObjectNotWrapped() {

        final Object result = guestToHostUtil.getUnwrappedObject(scriptEnvironment, FOO);

        assertThat(result).as("Unwrapped Object").isEqualTo(FOO);
    }

    @Test
    public void testGetUnwrappedObjectArray() {

        final String[] expected = new String[3];
        expected[0] = FOO;
        expected[1] = BAR;
        expected[2] = FOO_BAR;

        final GraalVMWrappedObject[] scriptObjectArray = new GraalVMWrappedObject[3];
        scriptObjectArray[0] = new GraalVMWrappedObject(scriptEnvironment, scriptEnvironment.getContext().asValue(FOO));
        scriptObjectArray[1] = new GraalVMWrappedObject(scriptEnvironment, scriptEnvironment.getContext().asValue(BAR));
        scriptObjectArray[2] = new GraalVMWrappedObject(scriptEnvironment, scriptEnvironment.getContext().asValue(FOO_BAR));

        final Object result = guestToHostUtil.getUnwrappedObject(scriptEnvironment, scriptObjectArray);

        assertThat(result).as("Unwrapped Object").isEqualTo(expected);
    }

    @Test
    public void testGetUnwrappedObjectArrayNotWrapped() {

        final String[] expected = new String[3];
        expected[0] = FOO;
        expected[1] = BAR;
        expected[2] = FOO_BAR;

        final Object result = guestToHostUtil.getUnwrappedObject(scriptEnvironment, expected);

        assertThat(result).as("Unwrapped Object").isEqualTo(expected);
    }

    @Test
    public void testGetUnwrappedObjectNullObject() {

        assertThat(guestToHostUtil.getUnwrappedObject(scriptEnvironment, null)).as("Unwrapped Object").isNull();
    }

    @Test
    public void testGetHostValueClass() {

        final Value value = scriptEnvironment.getContext().asValue(createMemberMap());

        final Class<?> clazz = guestToHostUtil.getHostValueClass(value);

        assertThat(clazz).as("Host Class").isNotNull();

        final Class<?>[] interfaces = clazz.getInterfaces();

        assertThat(interfaces).as("Host Value Class Interface").contains(Map.class);
        assertThat(clazz).as("Host Value Class").isEqualTo(HashMap.class);
    }

    @Test
    public void testGetHostValueClassFromGuestObject() {

        final Value value = scriptEnvironment.getContext().eval(JS, JS_MAP);

        final Class<?> clazz = guestToHostUtil.getHostValueClass(value);

        assertThat(clazz).as("Host Value Class").isNull();
    }

    @Test
    public void testGetHostValueClassFromNull() {

        final Class<?> clazz = guestToHostUtil.getHostValueClass(null);

        assertThat(clazz).as("Host Value Class").isNull();
    }

    @Test
    public void testSetMembersInMap() {

        final Map<String, Object> memberMap = new HashMap<>();

        final Value value = scriptEnvironment.getContext().eval(JS, JS_MAP);

        guestToHostUtil.setMembersInMap(scriptEnvironment, value, memberMap);

        assertThat(memberMap).as("Map").hasSize(3);
        assertThat(memberMap.get("id")).as("Map Entry - id").isEqualTo(42);
        assertThat(memberMap.get("text")).as("Map Entry - text").isEqualTo("42");
        assertThat(memberMap.get("arr")).as("Map Entry - arr").isEqualTo(INT_LIST);
    }

    @Test
    public void testSetMembersInMapNoMembers() {

        final Map<String, Object> memberMap = new HashMap<>();

        final Value value = scriptEnvironment.getContext().eval(JS, JS_ARRAY);

        guestToHostUtil.setMembersInMap(scriptEnvironment, value, memberMap);

        assertThat(memberMap).as("Map").isEmpty();
    }

    @Test
    public void testSetMembersInMapNullValue() {

        final Map<String, Object> memberMap = new HashMap<>();

        guestToHostUtil.setMembersInMap(scriptEnvironment, null, memberMap);

        assertThat(memberMap).as("Map").isEmpty();
    }

    @Test
    public void testSetHashEntriesInMap() {

        final Map<String, Object> hashEntryMap = new HashMap<>();
        final Map<String, Object> memberMap = createMemberMap();

        final Value value = scriptEnvironment.getContext().asValue(memberMap);

        guestToHostUtil.setHashEntriesInMap(scriptEnvironment, value, hashEntryMap);

        assertThat(hashEntryMap).as("Map").hasSize(3);
        assertThat(hashEntryMap.get("id")).as("Map Entry - id").isEqualTo(42);
        assertThat(hashEntryMap.get("text")).as("Map Entry - text").isEqualTo("42");
        assertThat(hashEntryMap.get("arr")).as("Map Entry - arr").isEqualTo(DEFAULT_INT_ARRAY);
    }

    @Test
    public void testSetHashEntriesInMapNoEntries() {

        final Map<String, Object> hashEntryMap = new HashMap<>();

        final Value value = scriptEnvironment.getContext().asValue("FooBar");

        guestToHostUtil.setHashEntriesInMap(scriptEnvironment, value, hashEntryMap);

        assertThat(hashEntryMap).as("Map").isEmpty();
    }

    @Test
    public void testSetHashEntriesInMapNullValue() {

        final Map<String, Object> hashEntryMap = new HashMap<>();

        guestToHostUtil.setHashEntriesInMap(scriptEnvironment, null, hashEntryMap);

        assertThat(hashEntryMap).as("Map").isEmpty();
    }

    @Test
    public void testGetMapNotAMap() {

        final Value value = scriptEnvironment.getContext().asValue(BAR);

        assertThat(guestToHostUtil.getMap(scriptEnvironment, value)).as("Map").isEmpty();
    }


    @Test
    public void testGetMapFromGuest() {

        final Value value = scriptEnvironment.getContext().eval(JS, JS_MAP);

        final Map<String, Object> memberMap = guestToHostUtil.getMap(scriptEnvironment, value);

        assertThat(memberMap).as("Map").hasSize(3);
        assertThat(memberMap.get("id")).as("Map Entry - id").isEqualTo(42);
        assertThat(memberMap.get("text")).as("Map Entry - text").isEqualTo("42");
        assertThat(memberMap.get("arr")).as("Map Entry - arr").isEqualTo(INT_LIST);
    }

    @Test
    public void testGetMapFromHost() {

        final Map<String, Object> valueMap = createMemberMap();
        final Value value = scriptEnvironment.getContext().asValue(valueMap);

        final Map<String, Object> memberMap = guestToHostUtil.getMap(scriptEnvironment, value);

        assertThat(memberMap).as("Map").hasSize(3);
        assertThat(memberMap.get("id")).as("Map Entry - id").isEqualTo(42);
        assertThat(memberMap.get("text")).as("Map Entry - text").isEqualTo("42");
        assertThat(memberMap.get("arr")).as("Map Entry - arr").isEqualTo(DEFAULT_INT_ARRAY);
    }

    @Test
    public void testGetMapFromNullValue() {

        final Map<String, Object> memberMap = guestToHostUtil.getMap(scriptEnvironment, null);

        assertThat(memberMap).as("Map").isEmpty();
    }

    @Test
    public void testGetGuestLanguageObjectMetaObject() {

        final Map<String, Object> memberMap = createMemberMap();

        final Value value = scriptEnvironment.getContext().asValue(memberMap);
        final Value metaObject = value.getMetaObject();

        assertThat(guestToHostUtil.getGuestLanguageObject(scriptEnvironment, metaObject)).as("Meta Object").isEqualTo(memberMap.getClass());
    }

    @Test
    public void testGetGuestLanguageObjectFromGuest() {

        final Value value = scriptEnvironment.getContext().eval(JS, JS_MAP);
        final Object obj = guestToHostUtil.getGuestLanguageObject(scriptEnvironment, value);

        assertThat(obj).as("Object").isNotNull();
        assertThat(obj).as("Object").isInstanceOf(HashMap.class);

        final Map memberMap = Collections.unmodifiableMap((Map) obj);

        assertThat(memberMap).as("Map").hasSize(3);
        assertThat(memberMap.get("id")).as("Map Entry - id").isEqualTo(42);
        assertThat(memberMap.get("text")).as("Map Entry - text").isEqualTo("42");
        assertThat(memberMap.get("arr")).as("Map Entry - arr").isEqualTo(INT_LIST);
    }

    @Test
    public void testGetGuestLanguageObjectFromHost() {

        final Map<String, Object> valueMap = createMemberMap();

        final Value value = scriptEnvironment.getContext().asValue(valueMap);
        final Object obj = guestToHostUtil.getGuestLanguageObject(scriptEnvironment, value);

        assertThat(obj).as("Object").isNotNull();
        assertThat(obj).as("Object").isInstanceOf(HashMap.class);

        final Map memberMap = (Map) obj;

        assertThat(memberMap).as("Map").hasSize(3);
        assertThat(memberMap.get("id")).as("Map Entry - id").isEqualTo(42);
        assertThat(memberMap.get("text")).as("Map Entry - text").isEqualTo("42");
        assertThat(memberMap.get("arr")).as("Map Entry - arr").isEqualTo(DEFAULT_INT_ARRAY);
    }

    @Test
    public void testGetGuestLanguageObjectNullValue() {

        assertThat(guestToHostUtil.getGuestLanguageObject(scriptEnvironment, null)).as("Object").isNull();
    }

    @Test
    public void testGetArrayFromGuest() {

        final Value value = scriptEnvironment.getContext().eval(JS, JS_ARRAY);

        assertThat(guestToHostUtil.getArray(scriptEnvironment, value)).as("Array").isEqualTo(DEFAULT_INT_ARRAY);
    }

    @Test
    public void testGetArrayFromHost() {

        final Value value = scriptEnvironment.getContext().asValue(DEFAULT_INT_ARRAY);

        assertThat(guestToHostUtil.getArray(scriptEnvironment, value)).as("Array").isEqualTo(DEFAULT_INT_ARRAY);
    }

    @Test
    public void testGetArrayNonArray() {

        final Value value = scriptEnvironment.getContext().asValue(createMemberMap());

        assertThat(guestToHostUtil.getArray(scriptEnvironment, value)).as("Array").isNull();
    }

    @Test
    public void testGetArrayNullValue() {

        assertThat(guestToHostUtil.getArray(scriptEnvironment, null)).as("Array").isNull();
    }

    @Test
    public void testGetIteratorFromGuest() {

        final Value value = scriptEnvironment.getContext().eval(JS, JS_ITERATOR);

        final Iterator<?> iterator = guestToHostUtil.getIterator(scriptEnvironment, value);

        assertThat(iterator.hasNext()).as("Iterator").isTrue();
        assertThat(iterator.next()).as("Iterator").isEqualTo(1);
        assertThat(iterator.hasNext()).as("Iterator").isTrue();
        assertThat(iterator.next()).as("Iterator").isEqualTo(2);
        assertThat(iterator.hasNext()).as("Iterator").isTrue();
        assertThat(iterator.next()).as("Iterator").isEqualTo(3);
        assertThat(iterator.hasNext()).as("Iterator").isFalse();
    }

    @Test
    public void testGetIteratorFromHost() {

        final Value value = scriptEnvironment.getContext().asValue(INT_LIST.iterator());

        final Iterator<?> iterator = guestToHostUtil.getIterator(scriptEnvironment, value);

        assertThat(iterator.hasNext()).as("Iterator").isTrue();
        assertThat(iterator.next()).as("Iterator").isEqualTo(1);
        assertThat(iterator.hasNext()).as("Iterator").isTrue();
        assertThat(iterator.next()).as("Iterator").isEqualTo(42);
        assertThat(iterator.hasNext()).as("Iterator").isTrue();
        assertThat(iterator.next()).as("Iterator").isEqualTo(3);
        assertThat(iterator.hasNext()).as("Iterator").isFalse();
    }

    @Test
    public void testGetIteratorNonArray() {

        final Value value = scriptEnvironment.getContext().asValue(createMemberMap());

        final Iterator<?> iterator = guestToHostUtil.getIterator(scriptEnvironment, value);

        assertThat(iterator.hasNext()).as("Iterator").isFalse();
    }

    @Test
    public void testGetIteratorNullValue() {

        final Iterator<?> iterator = guestToHostUtil.getIterator(scriptEnvironment, null);

        assertThat(iterator.hasNext()).as("Iterator").isFalse();
    }

    @Test
    public void testGetIterableFromGuest() {

        final Value value = scriptEnvironment.getContext().eval(JS, JS_ARRAY);

        final Iterable<?> iterable = guestToHostUtil.getIterable(scriptEnvironment, value);

        iterable.forEach(entry -> assertThat(entry).as("Iterable Entry").isIn(1, 42, 3));

    }

    @Test
    public void testGetIterableFromHost() {

        final Value value = scriptEnvironment.getContext().asValue(INT_LIST);

        final Iterable<?> iterable = guestToHostUtil.getIterable(scriptEnvironment, value);

        iterable.forEach(entry -> assertThat(entry).as("Iterable Entry").isIn(1, 42, 3));
    }

    @Test
    public void testGetIterableNonIterableValue() {

        final Value value = scriptEnvironment.getContext().asValue("(var text = 'foobar'");

        final Iterable<?> iterable = guestToHostUtil.getIterable(scriptEnvironment, value);

        assertThat(iterable).as("Iterable").isEmpty();
    }

    @Test
    public void testGetIterableNullValue() {

        final Iterable<?> iterable = guestToHostUtil.getIterable(scriptEnvironment, null);

        assertThat(iterable).as("Iterable").isEmpty();
    }

    @Test
    public void testGetDefaultNumberInteger() {

        final String integerString = "42";
        final Integer stringToInt = Integer.valueOf(integerString);

        final Value value = scriptEnvironment.getContext().eval(JS, integerString);

        final Number result = guestToHostUtil.getDefaultNumber(value);

        assertThat(result).as("Default Number").isInstanceOf(Integer.class).isEqualTo(stringToInt);
    }

    @Test
    public void testGetDefaultNumberIntegerWithDecimalAndZeros() {

        final String integerString = "42.00";
        final Integer expected = 42;

        final Value value = scriptEnvironment.getContext().eval(JS, integerString);

        final Number result = guestToHostUtil.getDefaultNumber(value);

        assertThat(result).as("Default Number").isInstanceOf(Integer.class).isEqualTo(expected);
    }

    @Test
    public void testGetDefaultNumberDouble() {

        final String doubleString = "42.42";
        final Double stringToDouble = Double.valueOf(doubleString);

        final Value value = scriptEnvironment.getContext().eval(JS, doubleString);

        final Number result = guestToHostUtil.getDefaultNumber(value);

        assertThat(result).as("Default Number").isInstanceOf(Double.class).isEqualTo(stringToDouble);
    }

    @Test
    public void testGetDefaultNumberTooLargeForInteger() {

        final long maxLong = Long.MAX_VALUE;
        final String longString = Long.toString(maxLong);
        final Double longToDouble = Double.valueOf(longString);

        final Value value = scriptEnvironment.getContext().eval(JS, longString);

        final Number result = guestToHostUtil.getDefaultNumber(value);

        assertThat(result).as("Default Number").isInstanceOf(Double.class).isEqualTo(longToDouble);
    }

    @Test
    public void testGetDefaultNumberNotANumber() {

        final String expected = "FooBar";

        final Value value = Value.asValue(expected);

        final Object result = guestToHostUtil.getDefaultNumber(value);

        assertThat(result).as("Default Number").isNull();
    }

    @Test
    public void testGetDefaultNumberNullValue() {

        final Object result = guestToHostUtil.getDefaultNumber(null);

        assertThat(result).as("Default Number").isNull();
    }

    @Test
    public void testGetNumberNoMetaData() {

        final Integer expected = 42;

        final Value value = Value.asValue(expected);

        final Object result = guestToHostUtil.getNumber(value);

        assertThat(result)
                .as("Number")
                .isNotNull()
                .isInstanceOf(Integer.class)
                .isEqualTo(expected);
    }

    @Test
    public void testGetNumberMetaDataNoHostClass() {

        final Integer expected = 42;

        final Value value = scriptEnvironment.getContext().eval(JS, expected.toString());

        final Object result = guestToHostUtil.getNumber(value);

        assertThat(result)
                .as("Number")
                .isNotNull()
                .isInstanceOf(Integer.class)
                .isEqualTo(expected);
    }

    @Test
    public void testGetNumberByte() {

        final Byte expected = Byte.parseByte("1");

        final Value value = scriptEnvironment.getContext().asValue(expected);

        final Object result = guestToHostUtil.getNumber(value);

        assertThat(result)
                .as("Number")
                .isNotNull()
                .isInstanceOf(Byte.class)
                .isEqualTo(expected);
    }

    @Test
    public void testGetNumberShort() {

        final Short expected = (short) 42;

        final Value value = scriptEnvironment.getContext().asValue(expected);

        final Object result = guestToHostUtil.getNumber(value);

        assertThat(result)
                .as("Number")
                .isNotNull()
                .isInstanceOf(Short.class)
                .isEqualTo(expected);
    }

    @Test
    public void testGetNumberInteger() {

        final Integer expected = 42;

        final Value value = scriptEnvironment.getContext().asValue(expected);

        final Object result = guestToHostUtil.getNumber(value);

        assertThat(result)
                .as("Number")
                .isNotNull()
                .isInstanceOf(Integer.class)
                .isEqualTo(expected);
    }

    @Test
    public void testGetNumberLong() {

        final Long expected = 42L;

        final Value value = scriptEnvironment.getContext().asValue(expected);

        final Object result = guestToHostUtil.getNumber(value);

        assertThat(result)
                .as("Number")
                .isNotNull()
                .isInstanceOf(Long.class)
                .isEqualTo(expected);
    }

    @Test
    public void testGetNumberFloat() {

        final Float expected = 42.42f;

        final Value value = scriptEnvironment.getContext().asValue(expected);

        final Object result = guestToHostUtil.getNumber(value);

        assertThat(result)
                .as("Number")
                .isNotNull()
                .isInstanceOf(Float.class)
                .isEqualTo(expected);
    }

    @Test
    public void testGetNumberDouble() {

        final Double expected = 42.42;

        final Value value = scriptEnvironment.getContext().asValue(expected);

        final Object result = guestToHostUtil.getNumber(value);

        assertThat(result)
                .as("Number")
                .isNotNull()
                .isInstanceOf(Double.class)
                .isEqualTo(expected);
    }

    @Test
    public void testGetHostObjectFromValueNullObject() {

        assertThat(guestToHostUtil.getHostObjectFromValue(scriptEnvironment, null)).as("Host Value").isNull();
    }

    @Test
    public void testGetHostObjectNullValue() {

        assertThat(guestToHostUtil.getHostObjectFromValue(scriptEnvironment, scriptEnvironment.getContext().asValue(null))).as("Host Value").isNull();
    }

    @Test
    public void testGetHostObjectFromValueMap() {

        final List<String> objList = new ArrayList<>();
        objList.add("1");
        objList.add("2");
        objList.add("3");

        final Value value = scriptEnvironment.getContext().asValue(objList);

        final Object result = guestToHostUtil.getHostObjectFromValue(scriptEnvironment, value);

        assertThat(result).as("Host Value").isNotNull();
        assertThat(result).as("Host Value Type").isInstanceOf(List.class);

        final List<?> resultList = (List<?>) result;

        assertThat(resultList).as("Result List").hasSize(3);
    }

    @Test
    public void testGetHostValueProxyObject() {

        final DummyProxyObject proxyObject = new DummyProxyObject();

        final Value value = scriptEnvironment.getContext().asValue(proxyObject);

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(result).as("Host Value - Proxy").isInstanceOf(Proxy.class);
    }

    @Test
    public void testGetHostValueHostObject() {

        final Value value = scriptEnvironment.getContext().asValue(BigDecimal.ONE);

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(result).as("Host Value - BigDecimal").isInstanceOf(BigDecimal.class);
        assertThat(result).as("Host Value - BigDecimal").isEqualTo(BigDecimal.ONE);
    }

    @Test
    public void testGetHostValueBoolean() {

        final Value value = scriptEnvironment.getContext().asValue(Boolean.TRUE);

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(result).as("Host Value - Boolean").isInstanceOf(Boolean.class);
        assertThat((Boolean) result).as("Host Value - Boolean").isTrue();
    }

    @Test
    public void testGetHostValueInteger() {

        final Value value = scriptEnvironment.getContext().asValue(42);

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(result).as("Host Value - Integer").isInstanceOf(Integer.class);
        assertThat(result).as("Host Value - Integer").isEqualTo(42);
    }

    @Test
    public void testGetHostValueString() {

        final Value value = scriptEnvironment.getContext().asValue(FOO);

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(result).as("Host Value - String").isInstanceOf(String.class);
        assertThat(result).as("Host Value - String").isEqualTo(FOO);
    }

    @Test
    public void testGetHostValueDate() {

        final LocalDate date = LocalDate.now();

        final Value value = scriptEnvironment.getContext().asValue(date);

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(result).as("Host Value - Date").isInstanceOf(LocalDate.class);
        assertThat(result).as("Host Value - Date").isEqualTo(date);
    }

    @Test
    public void testGetHostValueDateTime() {

        final LocalTime date = LocalTime.now();

        final Value value = scriptEnvironment.getContext().asValue(date);

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(result).as("Host Value - Time").isInstanceOf(LocalTime.class);
        assertThat(result).as("Host Value - Time").isEqualTo(date);
    }

    @Test
    public void testGetHostValueDuration() {

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime fiveMinutesBehind = now.minusMinutes(5);

        Duration duration = Duration.between(now, fiveMinutesBehind);

        final Value value = scriptEnvironment.getContext().asValue(duration);

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(result).as("Host Value - Duration").isInstanceOf(Duration.class);
        assertThat(result).as("Host Value - Duration").isEqualTo(duration);
    }

    @Test
    public void testGetHostValueInstant() {

        final Instant timestamp = Instant.now();

        final Value value = scriptEnvironment.getContext().asValue(timestamp);

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(result).as("Host Value - Instant").isInstanceOf(Instant.class);
        assertThat(result).as("Host Value - Instant").isEqualTo(timestamp);
    }

    @Test
    public void testGetHostValueTimezone() {

        final ZoneId pst = ZoneId.of("America/Los_Angeles");

        final Value value = scriptEnvironment.getContext().asValue(pst);

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(result).as("Host Value - Timezone").isInstanceOf(ZoneId.class);
        assertThat(result).as("Host Value - Timezone").isEqualTo(pst);
    }

    @Test
    public void testGetHostValueIterator() {

        final Value value = scriptEnvironment.getContext().asValue(INT_LIST.iterator());

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(result).as("Host Vale - Iterator").isNotNull();
        assertThat(result).as("Host Value - Iterator").isInstanceOf(Iterator.class);

        final Iterator<?> iterator = (Iterator<?>) result;

        assertThat(iterator.hasNext()).as("Iterator").isTrue();
        assertThat(iterator.next()).as("Iterator").isEqualTo(1);
        assertThat(iterator.hasNext()).as("Iterator").isTrue();
        assertThat(iterator.next()).as("Iterator").isEqualTo(42);
        assertThat(iterator.hasNext()).as("Iterator").isTrue();
        assertThat(iterator.next()).as("Iterator").isEqualTo(3);
        assertThat(iterator.hasNext()).as("Iterator").isFalse();
    }

    @Test
    public void testGetHostValueIterable() {

        final Value value = scriptEnvironment.getContext().asValue(INT_LIST);

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(result).as("Host Value - Iterator").isNotNull();
        assertThat(result).as("Host Value - Iterator").isInstanceOf(Iterable.class);

        final Iterable<?> iterable = (Iterable<?>) result;

        iterable.forEach(entry -> assertThat(entry).as("Iterable Entry").isIn(1, 42, 3));
    }

    @Test
    public void testGetHostValueArray() {

        final Value value = scriptEnvironment.getContext().asValue(INT_LIST.toArray());

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(result).as("Host Value - Array").isNotNull();
        assertThat(result).as("Host Value - Array").isInstanceOf(Object[].class);

        final Object[] intArray = (Object[]) result;

        assertThat(intArray[0]).as("Host Value - Array").isEqualTo(1);
        assertThat(intArray[1]).as("Host Value - Array").isEqualTo(42);
        assertThat(intArray[2]).as("Host Value - Array").isEqualTo(3);
    }

    @Test
    public void testGetHostValueMap() {

        final Map<String, Object> memberMap = createMemberMap();
        final Value value = scriptEnvironment.getContext().asValue(memberMap);

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(result).as("Host Value - Array").isNotNull();
        assertThat(result).as("Host Value - Array").isInstanceOf(Map.class);
        assertThat(result).as("Host Value - Map").isEqualTo(memberMap);
    }

    @Test
    public void testGetHostValueObject() {

        final Value value = scriptEnvironment.getContext().eval(JS, JS_MAP);
        final Object obj = guestToHostUtil.getHostValue(scriptEnvironment, value);

        assertThat(obj).as("Object").isNotNull();
        assertThat(obj).as("Object").isInstanceOf(HashMap.class);

        final Map memberMap = Collections.unmodifiableMap((Map) obj);

        assertThat(memberMap).as("Map").hasSize(3);
        assertThat(memberMap.get("id")).as("Map Entry - id").isEqualTo(42);
        assertThat(memberMap.get("text")).as("Map Entry - text").isEqualTo("42");
        assertThat(memberMap.get("arr")).as("Map Entry - arr").isEqualTo(INT_LIST);
    }

    @Test
    public void testGetHostValueMetaObject() {

        final Map<String, Object> memberMap = createMemberMap();
        final Value value = scriptEnvironment.getContext().asValue(memberMap);
        final Value metaObject = value.getMetaObject();

        final Object result = guestToHostUtil.getHostValue(scriptEnvironment, metaObject);

        assertThat(result).as("Meta Object - Class").isNotNull();
        assertThat(result).as("Meta Object - Class").isInstanceOf(Class.class);
        assertThat(result).as("Meta Object - Class").isEqualTo(memberMap.getClass());
    }

    @Test
    public void testGetHostValueReturnNull() {

        assertThat(guestToHostUtil.getHostValue(scriptEnvironment, null)).as("Host Value").isNull();
    }

    @Test
    public void testGetHostbject() {

        final Object result = guestToHostUtil.getHostObject(scriptEnvironment, FOO);

        assertThat(result).as("Host Value").isEqualTo(FOO);
    }

    @Test
    public void testGetHostObjectFromValue() {

        final Object value = scriptEnvironment.getContext().asValue(FOO);

        final Object result = guestToHostUtil.getHostObject(scriptEnvironment, value);

        assertThat(result).as("Host Value").isEqualTo(FOO);
    }

    @Test
    public void testGetHostObjectFromWrappedObject() {

        final Value value = scriptEnvironment.getContext().asValue(FOO);
        final GraalVMWrappedObject scriptObject = new GraalVMWrappedObject(scriptEnvironment, value);

        final Object result = guestToHostUtil.getHostObject(scriptEnvironment, scriptObject);

        assertThat(result).as("Host Value").isEqualTo(FOO);
    }

    @Test
    public void testGetHostObjects() {

        final Value value1 = scriptEnvironment.getContext().asValue(FOO);
        final Value value2 = scriptEnvironment.getContext().asValue(BAR);
        final Value value3 = scriptEnvironment.getContext().asValue(FOO_BAR);

        final Object[] result = guestToHostUtil.getHostObjects(scriptEnvironment, value1, value2, value3);

        assertThat(result).as("Result").isNotNull();
        assertThat(result).as("Result Size").hasSize(3);
        assertThat(result[0]).as("Host Value").isEqualTo(FOO);
        assertThat(result[1]).as("Host Value").isEqualTo(BAR);
        assertThat(result[2]).as("Host Value").isEqualTo(FOO_BAR);
    }

    @Test
    public void testGetReturnObjectFromValue() {

        final Value value = scriptEnvironment.getContext().asValue(FOO);
        final String defaultValue = "1";

        final Object result = guestToHostUtil.getReturnObject(scriptEnvironment, value, defaultValue);

        assertThat(result).as("Result").isNotNull();
        assertThat(result).as("Result Type").isInstanceOf(String.class);
        assertThat(result).as("Return Object").isEqualTo(FOO);
    }

    @Test
    public void testGetReturnObjectFromWrappedObject() {

        final Value value = scriptEnvironment.getContext().asValue(FOO);
        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(scriptEnvironment, value);
        final String defaultValue = "1";

        final Object result = guestToHostUtil.getReturnObject(scriptEnvironment, wrappedObject, defaultValue);

        assertThat(result).as("Result").isNotNull();
        assertThat(result).as("Result Type").isInstanceOf(String.class);
        assertThat(result).as("Return Object").isEqualTo(FOO);
    }

    @Test
    public void testGetReturnObjectFromDefaultValue() {

        final String defaultValue = "1";

        final Object result = guestToHostUtil.getReturnObject(scriptEnvironment, FOO, defaultValue);

        assertThat(result).as("Result").isNotNull();
        assertThat(result).as("Result Type").isInstanceOf(String.class);
        assertThat(result).as("Return Object").isEqualTo("1");
    }

    @Test
    public void testGetReturnObjects() {

        final Value[] valueArray = {scriptEnvironment.getContext().asValue(FOO), scriptEnvironment.getContext().asValue(BAR)};

        final Object[] result = guestToHostUtil.getReturnObjects(scriptEnvironment, valueArray);

        assertThat(result).as("Result").isNotNull();
        assertThat(result).as("Result Type").hasSize(2);
        assertThat(result[0]).as("Return Object").isEqualTo(FOO);
        assertThat(result[1]).as("Return Object").isEqualTo(BAR);
    }

    private Map<String, Object> createMemberMap() {

        final Map<String, Object> memberMap = new HashMap<>();
        memberMap.put("id", 42);
        memberMap.put("text", "42");
        memberMap.put("arr", DEFAULT_INT_ARRAY);

        return memberMap;
    }

    private static final String JS = "js";
    public static final String FOO = "Foo";
    public static final String BAR = "Bar";
    public static final String FOO_BAR = FOO + BAR;
    private static final Integer[] DEFAULT_INT_ARRAY = {1, 42, 3};
    private static final String JS_ARRAY = "([1,42,3])";

    private static final String JS_MAP = """
            ({
             id   : 42,\s
            text : '42',
             arr  : [1,42,3]\s
            })""";

    private static final String JS_ITERATOR = """
            class Sequence {
                constructor( start = 0, end = Infinity, interval = 1 ) {
                    this.start = start;
                    this.end = end;
                    this.interval = interval;
                }
                [Symbol.iterator]() {
                    let counter = 0;
                    let nextIndex = this.start;
                    return  {
                        next: () => {
                            if ( nextIndex <= this.end ) {
                                let result = { value: nextIndex,  done: false }
                                nextIndex += this.interval;
                                counter++;
                                return result;
                            }
                            return { value: counter, done: true };
                        }
                    }
                }
            };
                        
            let evenNumbers = new Sequence(1, 3, 1);
            evenNumbers[Symbol.iterator]();
            """;


}