/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.graalvm.polyglot.Value;

public class GraalVMDebugTestUtil {

    /*
     * *************************************************************************************************************** *
     * *************************************************************************************************************** *
     * *************************************************************************************************************** *
     * *************************************************************************************************************** *
     *                                                                                                                 *
     *            ONLY USE THIS DURING DEVELOPMENT FOR DEBUGGING TO FIGURE OUT WHAT IS IN A VALUE OBJECT!!             *
     *                            DO NOT USE THIS IN PRODUCTION OR TEST ENVIRONMENTS!!                                 *
     *                                         DEVELOPER ENVIRONMENTS ONLY!!                                           *
     *                                                                                                                 *
     * *************************************************************************************************************** *
     * *************************************************************************************************************** *
     * *************************************************************************************************************** *
     */

    public static void showKeys(final Value value) {

        if (value.hasMembers()) {
            final Set<String> keys = value.getMemberKeys();

            if (CollectionUtils.isEmpty(keys)) {
                System.out.println("Value has no members");
            } else {
                value.getMemberKeys().forEach(key -> System.out.println("Member Key = " + key));
            }

        } else {
            System.out.println("Value has no members");
        }

        if (value.hasArrayElements()) {
            System.out.println("Value does have Array Elements");
        } else {
            System.out.println("Value does not have Array Elements");
        }

        if (value.hasHashEntries()) {
            value.getHashKeysIterator().getMemberKeys().forEach(key -> System.out.println("Hash Entry Key = " + key));
        } else {
            System.out.println("Value does not have Hash Entries");
        }

    }

    public static void debug(final Value value) {

        debug(value, StringUtils.EMPTY);
    }

    public static void debug(final Value value, final String prefix) {

        String newPrefix = prefix + "--";

        System.out.println(prefix + " Displaying Type");
        displayType(value, newPrefix);

        System.out.println(prefix + " Displaying Array Elements");
        displayArrayElements(value, prefix);

        System.out.println(prefix + " Displaying Hash Entries");
        displayHashEntries(value, newPrefix);

        System.out.println(prefix + " Displaying Members");
        displayMembers(value, newPrefix);

        System.out.println(prefix + " Displaying Meta Data");
        displayMetaObject(value, newPrefix);

        System.out.println(prefix + " Value toString --");
        System.out.println(prefix + "-----------------");
        System.out.println(value);
        System.out.println(prefix + "-----------------");
        System.out.println(prefix + "-- Value toString");
    }

    /*
     * ONLY USE THIS DURING DEVELOPMENT FOR DEBUGGING TO FIGURE OUT WHAT IS IN A VALUE OBJECT!!
     * DO NOT USE THIS IN PRODUCTION OR TEST ENVIRONMENTS!!
     * DEVELOPER ENVIRONMENTS ONLY!!
     */
    public static void displayMetaObject(final Value value, final String prefix) {

        if (value == null) {
            System.out.println(prefix + " No Meta Object");
            return;
        }

        final String newPrefix = prefix + "--";

        if (value.isMetaObject()) {
            displayMembers(value, newPrefix);
            return;
        }

        try {
            final Value metaValue = value.getMetaObject();

            if (metaValue == null) {
                System.out.println(prefix + " No Meta Object");
            } else {
                debug(metaValue, newPrefix);
            }

        } catch (final Exception e) {
            System.out.println(prefix + " Exception Message: " + e.getMessage());
        }

    }

    /*
     * ONLY USE THIS DURING DEVELOPMENT FOR DEBUGGING TO FIGURE OUT WHAT IS IN A VALUE OBJECT!!
     * DO NOT USE THIS IN PRODUCTION OR TEST ENVIRONMENTS!!
     * DEVELOPER ENVIRONMENTS ONLY!!
     */
    public static void displayMembers(final Value value, final String prefix) {

        if (value == null) {
            System.out.println(prefix + " Value passed in is NULL");
            return;
        }

        if (!value.hasMembers()) {
            System.out.println(prefix + " Value has no Members");
            return;
        }

        final String newPrefix = prefix + "--";

        if (CollectionUtils.isEmpty(value.getMemberKeys())) {
            System.out.println(prefix + " Value has no Member Keys.");
        } else {

            for (String key : value.getMemberKeys()) {
                System.out.println(prefix + " KEY NAME = " + key);

                final Value memberValue = value.getMember(key);
                debug(memberValue, newPrefix);
            }

        }

    }

    /*
     * ONLY USE THIS DURING DEVELOPMENT FOR DEBUGGING TO FIGURE OUT WHAT IS IN A VALUE OBJECT!!
     * DO NOT USE THIS IN PRODUCTION OR TEST ENVIRONMENTS!!
     * DEVELOPER ENVIRONMENTS ONLY!!
     */
    static void displayArrayElements(final Value value, final String prefix) {

        if (value == null || !value.hasArrayElements()) {
            System.out.println(prefix + " Value does not have Array Elements");
            return;
        }

        final String newPrefix = prefix + "--";
        final long arrSize = value.getArraySize();

        for (long index = 0; index < arrSize; index++) {
            final Value arrValue = value.getArrayElement(index);
            debug(arrValue, newPrefix);
        }

    }

    /*
     * ONLY USE THIS DURING DEVELOPMENT FOR DEBUGGING TO FIGURE OUT WHAT IS IN A VALUE OBJECT!!
     * DO NOT USE THIS IN PRODUCTION OR TEST ENVIRONMENTS!!
     * DEVELOPER ENVIRONMENTS ONLY!!
     */
    static void displayHashEntries(final Value value, final String prefix) {

        if (value == null || !value.hasHashEntries()) {
            System.out.println(prefix + " Value does not have Hash Entries");
            return;
        }

        final String newPrefix = prefix + "--";
        final Value keysValue = value.getHashKeysIterator();

        while (keysValue.hasIteratorNextElement()) {
            final Value hashKey = keysValue.getIteratorNextElement();
            final Value hashValue = value.getHashValue(hashKey);

            final String key = hashKey.toString();
            System.out.println(prefix + " HASH KEY = " + key);
            displayType(hashKey, newPrefix);

            final Object keyValue = hashValue.toString();
            System.out.println(prefix + " HASH VALUE = " + keyValue);
            displayType(hashValue, newPrefix);
        }

    }

    /*
     * ONLY USE THIS DURING DEVELOPMENT FOR DEBUGGING TO FIGURE OUT WHAT IS IN A VALUE OBJECT!!
     * DO NOT USE THIS IN PRODUCTION OR TEST ENVIRONMENTS!!
     * DEVELOPER ENVIRONMENTS ONLY!!
     */
    public static void displayType(final Value value, final String prefix) {

        if (value == null) {
            System.out.println(prefix + " Value is NULL");
            return;
        }

        if (value.isProxyObject()) {
            System.out.println(prefix + " Value is a Proxy Object");
        }

        if (value.isHostObject()) {
            System.out.println(prefix + " Value is a Host Object");
        }

        if (value.isBoolean()) {
            System.out.println(prefix + " Value is a Boolean");
        }

        if (value.isNumber()) {
            System.out.println(prefix + " Value is a Number");
        }

        if (value.isString()) {
            System.out.println(prefix + " Value is a String");
        }

        if (value.isDate()) {
            System.out.println(prefix + " Value is a Date");
        }

        if (value.isDuration()) {
            System.out.println(prefix + " Value is a Duration");
        }

        if (value.isInstant()) {
            System.out.println(prefix + " Value is an Instant");
        }

        if (value.isTime()) {
            System.out.println(prefix + " Value is a Time");
        }

        if (value.isTimeZone()) {
            System.out.println(prefix + " Value is a TimeZone");
        }

        if (value.isNativePointer()) {
            System.out.println(prefix + " Value is a Native Pointer");
        }

        if (value.hasIterator()) {
            System.out.println(prefix + " Value has an Object that implements Iterable");
        }

        if (value.isIterator()) {
            System.out.println(prefix + " Value is an Iterator");
        }

        if (value.hasArrayElements()) {
            System.out.println(prefix + " Value has Array Elements");
        }

        if (value.hasHashEntries()) {
            System.out.println(prefix + " Value has Hash Entries");
        }

        if (value.hasMembers()) {
            System.out.println(prefix + " Value has Members");
        }

        if (value.hasBufferElements()) {
            System.out.println(prefix + " Value has Buffer Elements");

            if (value.isBufferWritable()) {
                System.out.println(prefix + " Value is a Buffer Writable");
            }

        }

        if (value.isException()) {
            System.out.println(prefix + " Value is an Exception");
        }

        if (value.isMetaObject()) {
            System.out.println(prefix + " Value is a Meta Object");
        }

        if (value.canInstantiate()) {
            System.out.println(prefix + " Value can be Instantiated");
        }

        if (value.canExecute()) {
            System.out.println(prefix + " Value can be Executed");
        }

    }

}