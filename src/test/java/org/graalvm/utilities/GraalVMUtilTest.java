/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.utilities;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.graalvm.GraalVMRuntimeException;
import org.graalvm.GraalVMScriptEnvironment;
import org.graalvm.GraalVMWrappedObject;
import org.graalvm.dummy.DummyGraalVMScriptEnvironment;
import org.graalvm.dummy.DummyObject;
import org.graalvm.dummy.DummyScriptObject;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


@SuppressWarnings({"ConfusingArgumentToVarargsMethod"})
public class GraalVMUtilTest {

    private static final List<Integer> INT_LIST = new ArrayList<>();
    private final GraalVMUtil graalVmUtil = new GraalVMUtil();
    private DummyGraalVMScriptEnvironment scriptEnvironment;

    @BeforeEach
    public void setup() {


        scriptEnvironment = new DummyGraalVMScriptEnvironment();
        scriptEnvironment.initialize();

        if (CollectionUtils.isEmpty(INT_LIST)) {
            INT_LIST.add(1);
            INT_LIST.add(42);
            INT_LIST.add(3);
        }

    }

    @Test
    public void testIsScriptObjectWrappedObject() {

        final Value value = scriptEnvironment.getContext().asValue(FOO);
        final GraalVMWrappedObject scriptObject = new GraalVMWrappedObject(scriptEnvironment, value);

        assertThat(graalVmUtil.isScriptObject(scriptObject)).as("Script Object").isTrue();
    }

    @Test
    public void testIsScriptObjectValue() {

        final Value value = scriptEnvironment.getContext().asValue(FOO);

        assertThat(graalVmUtil.isScriptObject(value)).as("Script Object").isTrue();
    }

    @Test
    public void testIsScriptObjectPorxyObject() {

        final ProxyObject proxyObj = ProxyObject.fromMap(new HashMap<>());

        assertThat(graalVmUtil.isScriptObject(proxyObj)).as("Script Object").isTrue();
    }

    @Test
    public void testIsScriptObjectValueFalse() {

        assertThat(graalVmUtil.isScriptObject(FOO)).as("Script Object").isFalse();
    }


    @Test
    public void testIsNullOrUndefined() {

        assertThat(graalVmUtil.isNullOrUndefined(null)).as("Object Null or Undefined").isTrue();
    }

    @Test
    public void testIsNullOrUndefinedWithNullValue() {

        final Value value = scriptEnvironment.getContext().asValue(null);

        assertThat(graalVmUtil.isNullOrUndefined(value)).as("Object Null or Undefined").isTrue();
    }

    @Test
    public void testIsNullOrUndefinedWithNonValue() {

        assertThat(graalVmUtil.isNullOrUndefined(FOO)).as("Object Null or Undefined").isFalse();
    }

    @Test
    public void testIsNullOrUndefinedWithNonNullValue() {

        final Value value = scriptEnvironment.getContext().asValue(FOO);

        assertThat(graalVmUtil.isNullOrUndefined(value)).as("Object Null or Undefined").isFalse();
    }

    @Test
    public void testIsNullOrUndefinedWithScriptObject() {

        final DummyScriptObject diso = new DummyScriptObject(scriptEnvironment, FOO);

        assertThat(graalVmUtil.isNullOrUndefined(diso)).as("Object Null or Undefined").isFalse();
    }

    @Test
    public void testIsNullOrUndefinedWithScriptObjectNullValue() {

        final DummyScriptObject diso = new DummyScriptObject(scriptEnvironment);

        assertThat(graalVmUtil.isNullOrUndefined(diso)).as("Object Null or Undefined").isFalse();
    }

    @Test
    public void testIsArrayNull() {

        assertThat(graalVmUtil.isArray(scriptEnvironment, null)).as("Array").isFalse();
    }

    @Test
    public void testIsArray() {

        assertThat(graalVmUtil.isArray(scriptEnvironment, DEFAULT_INT_ARRAY)).as("Array").isTrue();
    }

    @Test
    public void testIsArrayNonArray() {

        assertThat(graalVmUtil.isArray(scriptEnvironment, FOO)).as("Array").isFalse();
    }

    @Test
    public void testIsArrayValueWrappedArray() {

        final Value value = scriptEnvironment.getContext().asValue(DEFAULT_INT_ARRAY);
        final GraalVMWrappedObject scriptObject = new GraalVMWrappedObject(scriptEnvironment, value);

        assertThat(graalVmUtil.isArray(scriptEnvironment, scriptObject)).as("Array").isTrue();
    }

    @Test
    public void testIsArrayValue() {

        final Value value = scriptEnvironment.getContext().asValue(DEFAULT_INT_ARRAY);

        assertThat(graalVmUtil.isArray(scriptEnvironment, value)).as("Array").isTrue();
    }

    @Test
    public void testIsArrayValueNonArray() {

        final Value value = scriptEnvironment.getContext().asValue(FOO);
        final GraalVMWrappedObject scriptObject = new GraalVMWrappedObject(scriptEnvironment, value);

        assertThat(graalVmUtil.isArray(scriptEnvironment, scriptObject)).as("Array").isFalse();
    }

    @Test
    public void testHasFunctionInValue() {

        final Value value = scriptEnvironment.getContext().asValue(createMemberMap());

        assertThat(graalVmUtil.hasFunction(value, "isEmpty")).as("Has Function").isTrue();
    }

    @Test
    public void testHasFunctionHostObject() {

        assertThat(graalVmUtil.hasFunction(createMemberMap(), "isEmpty")).as("Has Function").isFalse();
    }

    @Test
    public void testHasFunctionNullObject() {

        assertThat(graalVmUtil.hasFunction(null, "isEmpty")).as("Has Function").isFalse();
    }

    @Test
    public void testHasFunctionBlankFunctionName() {

        assertThat(graalVmUtil.hasFunction(createMemberMap(), "")).as("Has Function").isFalse();
    }

    @Test
    public void testIsFunctionNonFunction() {

        assertThat(graalVmUtil.isFunction(createMemberMap())).as("Is Function").isFalse();
    }

    @Test
    public void testIsFunctionValue() {

        final Value value = scriptEnvironment.getContext().eval(GraalVMScriptConstants.SCRIPT_TYPE_JS, JS_FUNCTION);

        assertThat(graalVmUtil.isFunction(value)).as("Is Function").isTrue();
    }

    @Test
    public void testIsFunctionWrappedObject() {

        final Value value = scriptEnvironment.getContext().eval(GraalVMScriptConstants.SCRIPT_TYPE_JS, JS_FUNCTION);
        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(scriptEnvironment, value);

        assertThat(graalVmUtil.isFunction(wrappedObject)).as("Is Function").isTrue();
    }

    @Test
    public void testIsObjectValue() {

        final Value value = scriptEnvironment.getContext().asValue(createMemberMap());

        assertThat(graalVmUtil.isObject(value)).as("Is Object").isTrue();
    }

    @Test
    public void testIsObjectWrappedObject() {

        final Value value = scriptEnvironment.getContext().asValue(createMemberMap());

        final GraalVMScriptEnvironment scriptEnvironment = mock(GraalVMScriptEnvironment.class);
        final GraalVMWrappedObject scriptObject = new GraalVMWrappedObject(scriptEnvironment, value);

        assertThat(graalVmUtil.isObject(scriptObject)).as("Is Object").isTrue();
    }

    @Test
    public void testIsObject() {

        assertThat(graalVmUtil.isObject(createMemberMap())).as("Is Object").isTrue();
    }

    @Test
    public void testIsObjectWithArray() {

        final Value value = scriptEnvironment.getContext().asValue(DEFAULT_INT_ARRAY);

        assertThat(graalVmUtil.isObject(value)).as("Is Object").isFalse();
    }

    @Test
    public void testIsObjectNull() {

        assertThat(graalVmUtil.isObject(null)).as("Is Object").isFalse();
    }

    @Test
    public void testHasMemberNonValue() {

        assertThat(graalVmUtil.hasMember(FOO, "isEmpty")).as("").isFalse();
    }

    @Test
    public void testHasMemberTrue() {

        final Value value = scriptEnvironment.getContext().asValue(new DummyObject());

        System.out.println("Member Keys: " + value.getMemberKeys());

        assertThat(graalVmUtil.hasMember(value, "getValue")).as("").isTrue();
    }

    @Test
    public void testSetHasMemberNonValue() {

        graalVmUtil.setMember(FOO, "isEmpty", BAR);

        assertThat(graalVmUtil.hasMember(FOO, "isEmpty")).as("").isFalse();
    }

    @Test
    public void testGetMemberNonValue() {

        final Value expected = Value.asValue(null);

        assertThat(graalVmUtil.getMember(FOO, "isEmpty")).as("").isEqualTo(expected);
    }

    @Test
    public void testSetHasGetMemberValue() {

        final Value expected = scriptEnvironment.getContext().asValue(BAR);

        final Value value = scriptEnvironment.getContext().asValue(createMemberMap());

        graalVmUtil.setMember(value, FOO, BAR);

        assertThat(graalVmUtil.hasMember(value, FOO)).as("Member - Foo").isTrue();
        assertThat(graalVmUtil.getMember(value, FOO)).as("Member - Foo").isEqualTo(expected);
    }

    @Test
    public void testCallFunctionNullObject() {

        assertThrows(GraalVMRuntimeException.class, () -> graalVmUtil.callFunction(scriptEnvironment, null, null));
    }

    @Test
    public void testCallFunctionNotExecutable() {

        final Object result = graalVmUtil.callFunction(scriptEnvironment, createMemberMap(), null);

        assertThat(result).as("Result").isInstanceOf(Value.class);

        final Value value = (Value) result;

        assertThat(value.isNull()).as("Result is Null").isTrue();
    }

    @Test
    public void testCallFunctionString() {

        final Object result = graalVmUtil.callFunction(scriptEnvironment, JS_FUNCTION, new Object[]{"Foo", 2});

        assertThat(result).as("Call Function Result Class").isInstanceOf(Value.class);

        final Value value = (Value) result;

        assertThat(value.toString()).as("Call Function with String result").isEqualTo("undefined");
    }

    @Test
    public void testModuleWrapWithNoArgs() {

        final String expected = "(function(module, exports) {return 42;})";

        final String script = "return 42;";

        final String function = graalVmUtil.moduleWrap(script);

        assertThat(function).as("Module Wrapped Function").isEqualTo(expected);
    }

    @Test
    public void testModuleWrapWithArgs() {

        final String expected = "(function(module, exports, arg1, arg2) {return arg1 + arg2;})";

        final String script = "return arg1 + arg2;";

        final String function = graalVmUtil.moduleWrap(script, "arg1", "arg2");

        assertThat(function).as("Module Wrapped Function").isEqualTo(expected);
    }

    @Test
    public void testModuleWrapWithEmptyMap() {

        final String expected = "(function(module, exports) {return 42;})";

        final String script = "return 42;";

        final String function = graalVmUtil.moduleWrap(script, new HashMap<>());

        assertThat(function).as("Module Wrapped Function").isEqualTo(expected);
    }

    @Test
    public void testModuleWrapWithMap() {

        final String expected = "(function(module, exports, arg2, arg1) {return arg1 + arg2;})";

        final String script = "return arg1 + arg2;";

        final Map<String, Object> argMap = new HashMap<>();
        argMap.put("arg1", FOO);
        argMap.put("arg2", BAR);

        final String function = graalVmUtil.moduleWrap(script, argMap);

        assertThat(function).as("Module Wrapped Function").isEqualTo(expected);
    }

    private Map<String, Object> createMemberMap() {

        final Map<String, Object> memberMap = new HashMap<>();
        memberMap.put("id", 42);
        memberMap.put("text", "42");
        memberMap.put("arr", DEFAULT_INT_ARRAY);

        return memberMap;
    }

    public static final String FOO = "Foo";

    public static final String BAR = "Bar";


    private static final Integer[] DEFAULT_INT_ARRAY = {1, 42, 3};
    private static final String JS_FUNCTION = """
            (function showValue(value, numberOfTimes) {

                if ((!value || /^\\s*$/.test(value))) {
                    throw 'Value is Blank';
                }

                if (isNaN(numberOfTimes)) {
                    throw 'Number of Times is Not A Number';
                }

                if (numberOfTimes === undefined || numberOfTimes == 0) {
                    console.log(value);
                }

                for (let i = 0; i < numberOfTimes; i++) {
                    console.log(value);
                }

            })

            """;

}