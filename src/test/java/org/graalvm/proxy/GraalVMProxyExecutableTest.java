/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.proxy;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Method;
import java.util.List;

import org.graalvm.GraalVMRuntimeException;
import org.graalvm.dummy.DummyGraalVMScriptEnvironment;
import org.graalvm.polyglot.Value;
import org.graalvm.utilities.GraalVMHostArguments;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class GraalVMProxyExecutableTest {

    private static final String FOOBAR = "FooBar";

    private DummyGraalVMScriptEnvironment scriptEnv;

    private Method defaultMethod;

    @BeforeEach
    public void setup() throws Exception {

        scriptEnv = new DummyGraalVMScriptEnvironment();
        scriptEnv.initialize();


        defaultMethod = FOOBAR.getClass().getMethod("toString");
    }

    @Test
    public void testReturnGuestLanguageObjectTestNullReturn() {

        final GraalVMProxyExecutable proxy = new GraalVMProxyExecutable(scriptEnv, FOOBAR, defaultMethod);
        final Object guestObject = proxy.returnGuestLanguageObject(null);

        assertThat(guestObject).as("Guest Object").isNotNull();
        assertThat(guestObject).as("Guest Object Type").isInstanceOf(Value.class);

        final Value value = (Value) guestObject;

        assertThat(value.isNull()).as("Guest Object is Undefined").isTrue();
    }

    @Test
    public void testReturnGuestLanguageObjectTest() {

        final GraalVMProxyExecutable proxy = new GraalVMProxyExecutable(scriptEnv, FOOBAR, defaultMethod);
        final Object guestObject = proxy.returnGuestLanguageObject(FOOBAR);

        assertThat(guestObject).as("Guest Object").isNotNull();
        assertThat(guestObject).as("Guest Object Type").isInstanceOf(String.class);
        assertThat(guestObject).as("Guest Object").isEqualTo(FOOBAR);
    }

    @Test
    public void testValidateParameterTypesInvalid() throws Exception {

        final Method method = FOOBAR.getClass().getMethod("split", String.class, Integer.TYPE);
        final GraalVMHostArguments arguments = new GraalVMHostArguments(scriptEnv, 1);
        arguments.addImmutableArg(0, ",");

        final GraalVMProxyExecutable proxy = new GraalVMProxyExecutable(scriptEnv, FOOBAR, method);
        final boolean isValid = proxy.validateParameterTypes(method, arguments);

        assertThat(isValid).as("Is Valid Parameter Types").isFalse();
    }

    @Test
    public void testValidateParameterTypesValid() throws Exception {

        final Method method = FOOBAR.getClass().getMethod("split", String.class);
        final GraalVMHostArguments arguments = new GraalVMHostArguments(scriptEnv, 1);
        arguments.addImmutableArg(0, ",");

        final GraalVMProxyExecutable proxy = new GraalVMProxyExecutable(scriptEnv, FOOBAR, method);
        final boolean isValid = proxy.validateParameterTypes(method, arguments);

        assertThat(isValid).as("Is Valid Parameter Types").isTrue();
    }

    @Test
    public void testValidateParameterTypesValidWithPrimitive() throws Exception {

        final Method method = FOOBAR.getClass().getMethod("split", String.class, Integer.TYPE);
        final GraalVMProxyExecutable proxy = new GraalVMProxyExecutable(scriptEnv, FOOBAR, method);
        final GraalVMHostArguments arguments = new GraalVMHostArguments(scriptEnv, 2);
        arguments.addImmutableArg(0, ",");
        arguments.addImmutableArg(1, 1);

        final boolean isValid = proxy.validateParameterTypes(method, arguments);

        assertThat(isValid).as("Is Valid Parameter Types").isTrue();
    }

    @Test
    public void testValidateParameterTypesValidWithTooManyParameters() throws Exception {

        final Method method = FOOBAR.getClass().getMethod("split", String.class, Integer.TYPE);
        final GraalVMHostArguments arguments = new GraalVMHostArguments(scriptEnv, 3);
        arguments.addImmutableArg(0, ",");
        arguments.addImmutableArg(1, 1);
        arguments.addImmutableArg(2, "Extra Param");

        final GraalVMProxyExecutable proxy = new GraalVMProxyExecutable(scriptEnv, FOOBAR, method);
        final boolean isValid = proxy.validateParameterTypes(method, arguments);

        assertThat(isValid).as("Is Valid Parameter Types").isFalse();
    }

    @Test
    public void testValidateParameterTypesValidWithVarArgs() throws Exception {

        final Method method = FOOBAR.getClass().getMethod("format", String.class, Object[].class);
        final GraalVMHostArguments arguments = new GraalVMHostArguments(scriptEnv, 2);
        arguments.addImmutableArg(0, ",");
        arguments.addImmutableArg(1, new String[]{"Var Arg1", "Var Arg 2"});

        final GraalVMProxyExecutable proxy = new GraalVMProxyExecutable(scriptEnv, FOOBAR, method);
        final boolean isValid = proxy.validateParameterTypes(method, arguments);

        assertThat(isValid).as("Is Valid Parameter Types").isFalse();
    }

    @Test
    public void testFindMethodWithSameName() throws Exception {

        final Method method = FOOBAR.getClass().getMethod("split", String.class, Integer.TYPE);

        final GraalVMProxyExecutable proxy = new GraalVMProxyExecutable(scriptEnv, FOOBAR, method);
        final List<Method> methods = proxy.findMethodsWithTheSameName();

        assertThat(methods).as("Method List size").hasSize(2);
        Method resultMethod = methods.get(0);

        assertThat(resultMethod.getName()).as("Method Name").isEqualTo(method.getName());
        assertSplitParamTypes(resultMethod);

        resultMethod = methods.get(1);

        assertThat(resultMethod.getName()).as("Method Name").isEqualTo(method.getName());
        assertSplitParamTypes(resultMethod);
    }

    @Test
    public void testFindMethodBasedOnPassedInArguments() throws Exception {

        final Method expected = FOOBAR.getClass().getMethod("split", String.class);
        final Method method = FOOBAR.getClass().getMethod("split", String.class, Integer.TYPE);

        final GraalVMHostArguments arguments = new GraalVMHostArguments(scriptEnv, 1);
        arguments.addImmutableArg(0, ",");

        final GraalVMProxyExecutable proxy = new GraalVMProxyExecutable(scriptEnv, FOOBAR, method);
        final boolean foundMethod = proxy.findMethodBasedOnPassedInArguments(arguments);

        assertThat(foundMethod).as("Found Method").isTrue();
        assertThat(proxy.getMethod()).as("Method").isEqualTo(expected);
    }

    @Test
    public void testFindMethod() throws Exception {

        final Method expected = FOOBAR.getClass().getMethod("split", String.class);
        final Method method = FOOBAR.getClass().getMethod("split", String.class, Integer.TYPE);
        final GraalVMProxyExecutable proxy = new GraalVMProxyExecutable(scriptEnv, FOOBAR, method);
        final GraalVMHostArguments arguments = new GraalVMHostArguments(scriptEnv, 1);
        arguments.addImmutableArg(0, ",");

        proxy.findMethod(arguments);

        final Method newMethod = proxy.getMethod();

        assertThat(newMethod).as("New Method is not null").isNotNull();
        assertThat(newMethod).as("New Method not equal to Old Method").isNotEqualTo(method);
        assertThat(newMethod).as("New Method").isEqualTo(expected);
    }

    @Test
    public void testFindMethodWithIncorrectMethodName() throws Exception {

        final Integer id = 1;
        final Method method = FOOBAR.getClass().getMethod("split", String.class, Integer.TYPE);
        final GraalVMHostArguments arguments = new GraalVMHostArguments(scriptEnv, 1);
        arguments.addImmutableArg(0, ",");

        final GraalVMProxyExecutable proxy = new GraalVMProxyExecutable(scriptEnv, id, method);

        assertThrows(GraalVMRuntimeException.class, () -> proxy.findMethod(arguments));
    }

    @Test
    public void testValidateCorrectMethodWasPassedIn() throws Exception {

        final Method expected = FOOBAR.getClass().getMethod("split", String.class);
        final Method method = FOOBAR.getClass().getMethod("split", String.class, Integer.TYPE);

        final Value value = scriptEnv.getContext().asValue(",");
        final Value[] arguments = new Value[1];
        arguments[0] = value;

        final GraalVMProxyExecutable proxy = new GraalVMProxyExecutable(scriptEnv, FOOBAR, method);
        proxy.validateCorrectMethodWasPassedIn(arguments);

        final Method newMethod = proxy.getMethod();

        assertThat(newMethod).as("New Method is not null").isNotNull();
        assertThat(newMethod).as("New Method not equal to Old Method").isNotEqualTo(method);
        assertThat(newMethod).as("New Method").isEqualTo(expected);
    }

    private void assertSplitParamTypes(final Method method) {

        final Class<?>[] paramTypes = method.getParameterTypes();
        Class<?> paramType = paramTypes[0];

        assertThat(paramType.getName()).as("Param Type - String").isEqualTo("java.lang.String");

        if (paramTypes.length > 1) {
            paramType = paramTypes[1];
            assertThat(paramType).as("Param Type - int").isEqualTo(Integer.TYPE);
        }

    }

}