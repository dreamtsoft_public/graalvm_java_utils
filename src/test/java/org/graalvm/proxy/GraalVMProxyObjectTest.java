/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.proxy;

import static org.assertj.core.api.Assertions.*;

import org.graalvm.dummy.DummyGraalVMProxyObject;
import org.graalvm.dummy.DummyGraalVMScriptEnvironment;
import org.graalvm.polyglot.Value;
import org.junit.jupiter.api.Test;

public class GraalVMProxyObjectTest {

    @Test
    public void testSetGetRemoveMemberKeys() {

        final String foo = "Foo";
        final String bar = "bar";

        final DummyGraalVMScriptEnvironment scriptEnvironment = new DummyGraalVMScriptEnvironment();
        scriptEnvironment.initialize();

        final DummyGraalVMProxyObject dummyProxyObject = new DummyGraalVMProxyObject();

        final GraalVMProxyObject proxyObject = new GraalVMProxyObject(scriptEnvironment, dummyProxyObject);

        proxyObject.putMember(foo, scriptEnvironment.getContext().asValue(bar));

        assertThat(proxyObject.hasMember(foo)).as("Has Member - Foo").isTrue();
        assertThat(proxyObject.hasMember("getProxyName")).as("Has Member - getProxyName").isTrue();

        final Object fooMember = proxyObject.getMember(foo);

        assertThat(fooMember).as("Foo Member").isInstanceOf(Value.class);
        assertThat(fooMember.toString()).as("Foo Member").isEqualTo(bar);

        final Object getProxyNameMember = proxyObject.getMember("getProxyName");

        assertThat(getProxyNameMember).as("Get Proxy Name Member").isInstanceOf(GraalVMProxyExecutable.class);

        assertThat(proxyObject.removeMember(foo)).as("Removing Member - Foo").isTrue();
        assertThat(proxyObject.hasMember(foo)).as("Has Member - Foo").isFalse();
    }

}