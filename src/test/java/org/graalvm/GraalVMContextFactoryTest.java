/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.logging.Handler;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Engine;
import org.graalvm.polyglot.EnvironmentAccess;
import org.graalvm.polyglot.HostAccess;
import org.graalvm.polyglot.PolyglotAccess;
import org.graalvm.polyglot.ResourceLimits;
import org.graalvm.polyglot.io.FileSystem;
import org.graalvm.polyglot.io.MessageTransport;
import org.graalvm.polyglot.io.ProcessHandler;
import org.graalvm.utilities.GraalVMScriptConstants;
import org.junit.jupiter.api.Test;

public class GraalVMContextFactoryTest {

    @Test
    public void testBuildContextDynamically() {

        final GraalVMContextFactory factory = new GraalVMContextFactory();
        final Context context = factory.buildContext(GraalVMScriptConstants.SCRIPT_TYPE_JS, createContextConfigMap1());

        assertThat(context).as("Context").isNotNull();
    }

    @Test
    public void testBuildContextStatically() {

        final GraalVMContextFactory factory = new GraalVMContextFactory(createContextConfigMap2());
        final Context context = factory.buildContext(GraalVMScriptConstants.SCRIPT_TYPE_JS);

        assertThat(context).as("Context").isNotNull();
    }

    private Map<String, Object> createContextConfigMap1() {

        final Map<String, Object> configMap = new HashMap<>();

        configMap.put(GraalVMConfigConstants.ALLOW_ALL_ACCESS, true);
        configMap.put(GraalVMConfigConstants.ALLOW_CREATE_PROCESS, true);
        configMap.put(GraalVMConfigConstants.ALLOW_CREATE_THREAD, true);
        configMap.put(GraalVMConfigConstants.ALLOW_ENVIRONMENT_ACCESS, EnvironmentAccess.INHERIT);
        configMap.put(GraalVMConfigConstants.OPTIONS, new HashMap<>());
        configMap.put(GraalVMConfigConstants.ALLOW_HOST_ACCESS, HostAccess.ALL);
        configMap.put(GraalVMConfigConstants.HOST_CLASS_LOADER, mock(ClassLoader.class));
        configMap.put(GraalVMConfigConstants.ALLOW_HOST_CLASS_LOOKUP, mock(Predicate.class));
        configMap.put(GraalVMConfigConstants.ALLOW_IO, true);
        configMap.put(GraalVMConfigConstants.ALLOW_NATIVE_ACCESS, true);
        configMap.put(GraalVMConfigConstants.ALLOW_POLUGLOT_ACCESS, mock(PolyglotAccess.class));
        configMap.put(GraalVMConfigConstants.ALLOW_VALUE_SHARING, true);
        configMap.put(GraalVMConfigConstants.ARGUMENTS, new String[0]);

        final File currentWorkingDir = mock(File.class);
        when(currentWorkingDir.getAbsolutePath()).thenReturn("/home/foo/bar");

        configMap.put(GraalVMConfigConstants.CURRENT_WORKING_DIRECTORY, currentWorkingDir);
        configMap.put(GraalVMConfigConstants.ENGINE, Engine.newBuilder().build());
        configMap.put(GraalVMConfigConstants.ENVIRONMENT, new HashMap<>());
        configMap.put(GraalVMConfigConstants.STD_ERR, mock(OutputStream.class));
        configMap.put(GraalVMConfigConstants.FILE_SYSTEM, mock(FileSystem.class));
        configMap.put(GraalVMConfigConstants.STD_IN, mock(InputStream.class));
        configMap.put(GraalVMConfigConstants.LOG_HANDLER, mock(Handler.class));
        configMap.put(GraalVMConfigConstants.STD_OUT, mock(OutputStream.class));
        configMap.put(GraalVMConfigConstants.PROCESS_HANDLER, mock(ProcessHandler.class));
        configMap.put(GraalVMConfigConstants.RESOURCE_LIMITS, mock(ResourceLimits.class));
        configMap.put(GraalVMConfigConstants.TIME_ZONE, ZoneId.systemDefault());
        configMap.put(GraalVMConfigConstants.USER_SYSTEM_EXIT, true);

        return configMap;
    }

    private Map<String, Object> createContextConfigMap2() {

        final Map<String, Object> configMap = new HashMap<>();

        configMap.put(GraalVMConfigConstants.ALLOW_ALL_ACCESS, true);
        configMap.put(GraalVMConfigConstants.ALLOW_CREATE_PROCESS, true);
        configMap.put(GraalVMConfigConstants.ALLOW_CREATE_THREAD, true);
        configMap.put(GraalVMConfigConstants.ALLOW_ENVIRONMENT_ACCESS, EnvironmentAccess.INHERIT);
        configMap.put(GraalVMConfigConstants.OPTIONS, new HashMap<>());
        configMap.put(GraalVMConfigConstants.ALLOW_HOST_ACCESS, HostAccess.ALL);
        configMap.put(GraalVMConfigConstants.HOST_CLASS_LOADER, mock(ClassLoader.class));
        configMap.put(GraalVMConfigConstants.ALLOW_HOST_CLASS_LOOKUP, mock(Predicate.class));
        configMap.put(GraalVMConfigConstants.ALLOW_IO, true);
        configMap.put(GraalVMConfigConstants.ALLOW_NATIVE_ACCESS, true);
        configMap.put(GraalVMConfigConstants.ALLOW_POLUGLOT_ACCESS, mock(PolyglotAccess.class));
        configMap.put(GraalVMConfigConstants.ALLOW_VALUE_SHARING, true);
        configMap.put(GraalVMConfigConstants.ARGUMENTS, new String[0]);

        final File currentWorkingDir = mock(File.class);
        when(currentWorkingDir.getAbsolutePath()).thenReturn("/home/foo/bar");

        configMap.put(GraalVMConfigConstants.CURRENT_WORKING_DIRECTORY, currentWorkingDir);

        configMap.put(GraalVMConfigConstants.ENVIRONMENT, new HashMap<>());
        configMap.put(GraalVMConfigConstants.STD_ERR, mock(OutputStream.class));
        configMap.put(GraalVMConfigConstants.FILE_SYSTEM, mock(FileSystem.class));
        configMap.put(GraalVMConfigConstants.STD_IN, mock(InputStream.class));
        configMap.put(GraalVMConfigConstants.LOG_HANDLER, mock(Handler.class));
        configMap.put(GraalVMConfigConstants.STD_OUT, mock(OutputStream.class));
        configMap.put(GraalVMConfigConstants.PROCESS_HANDLER, mock(ProcessHandler.class));
        configMap.put(GraalVMConfigConstants.RESOURCE_LIMITS, mock(ResourceLimits.class));
        configMap.put(GraalVMConfigConstants.SERVER_TRANSPORT, mock(MessageTransport.class));
        configMap.put(GraalVMConfigConstants.TIME_ZONE, ZoneId.systemDefault());
        configMap.put(GraalVMConfigConstants.USER_SYSTEM_EXIT, true);

        return configMap;
    }

}