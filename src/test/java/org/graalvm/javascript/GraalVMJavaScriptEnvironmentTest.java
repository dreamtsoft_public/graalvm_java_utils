/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm.javascript;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.graalvm.GraalVMRuntimeException;
import org.graalvm.GraalVMScriptEnvironment;
import org.graalvm.GraalVMWrappedObject;
import org.graalvm.dummy.DummyObject;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;
import org.graalvm.utilities.GraalVMGuestLanguageToHostUtil;
import org.graalvm.utilities.GraalVMScriptConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GraalVMJavaScriptEnvironmentTest {

    private static final List<Integer> INT_LIST = new ArrayList<>();

    private final GraalVMGuestLanguageToHostUtil gGuestToHostUtil = new GraalVMGuestLanguageToHostUtil();

    @BeforeEach
    public void setup() {

        if (CollectionUtils.isEmpty(INT_LIST)) {
            INT_LIST.add(1);
            INT_LIST.add(42);
            INT_LIST.add(3);
        }

    }

    @Test
    public void testGetContext() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        assertThat(env.getContext()).as("Script Environment Context").isNotNull();
    }

    @Test
    public void testFindGlobalObject() {

        final Map<String, Object> memberMap = createMemberMap();
        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final Value bindings = env.getContext().getBindings(JS);
        bindings.putMember("Map", memberMap);

        final GraalVMWrappedObject globalObject = env.findGlobalObject("Map");

        assertThat(globalObject).as("New Map Class").isNotNull();
        assertThat(gGuestToHostUtil.getHostValue(env, globalObject.getValue())).as("New Map").isEqualTo(memberMap);
    }

    @Test
    public void testFindGlobalObjectNotFound() {

        final Map<String, Object> memberMap = createMemberMap();
        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final Value bindings = env.getContext().getBindings(JS);
        bindings.putMember("Map", memberMap);

        assertThrows(GraalVMRuntimeException.class, () -> env.findGlobalObject("Mapper"));
    }

    @Test
    public void testCreateFunctionClassWithArgs() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        final GraalVMWrappedObject wrappedObject = env.createFunctionClass(GraalVMScriptConstants.FUNCTION_CLASS_NAME, "foo", "bar", "console.log(foo + '=' + bar);");

        assertThat(wrappedObject).as("Member Class Wrapped Object").isNotNull();

        final Value value = wrappedObject.getValue();

        assertThat(value).as("Member Class Value").isNotNull();
        assertThat(value.canExecute()).as("Member Class Value can execute").isTrue();
        assertThat(value.canInstantiate()).as("Member Class Value can instantiate").isTrue();
        assertThat(value.toString()).as("Member Class Value to string").isEqualTo(ANON_FUNCTION_WITH_ARGS);
    }

    @Test
    public void testCreateFunctionClassWithNoArgs() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        final GraalVMWrappedObject wrappedObject = env.createFunctionClass(GraalVMScriptConstants.FUNCTION_CLASS_NAME);

        assertThat(wrappedObject).as("Member Class Wrapped Object").isNotNull();

        final Value value = wrappedObject.getValue();

        assertThat(value).as("Member Class Value").isNotNull();
        assertThat(value.canExecute()).as("Member Class Value can execute").isTrue();
        assertThat(value.canInstantiate()).as("Member Class Value can instantiate").isTrue();
        assertThat(value.toString()).as("Member Class Value to string").isEqualTo(ANON_FUNCTION_NO_ARGS);
    }

    @Test
    public void testHasPrototypeFunctionWithNullObject() {

        final String functionName = "replace";

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final boolean hasFunc = env.hasPrototypeFunction(null, functionName);

        assertThat(hasFunc).as("Has Function").isFalse();
    }

    @Test
    public void testHasPrototypeFunctionWithWrappedObject() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        final DummyObject dummy = new DummyObject();
        final Value value = env.getContext().asValue(dummy);
        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(env, value);

        final String functionName = "getValue";

        final boolean hasFunc = env.hasPrototypeFunction(wrappedObject, functionName);

        assertThat(hasFunc).as("Has Function").isTrue();
    }

    @Test
    public void testHasPrototypeFunctionFromGlobal() {

        final String functionName = "replace";

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        final Object globalObj = env.getGlobalObject(GraalVMScriptConstants.STRING_CLASS_NAME);

        final boolean hasFunc = env.hasPrototypeFunction(globalObj, functionName);

        assertThat(hasFunc).as("Has Function").isTrue();
    }

    @Test
    public void testHasPrototypeFunctionWithWrappedJavaString() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        final Value value = env.getContext().asValue(FOO);
        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(env, value);

        final String functionName = "replace";

        final boolean hasFunc = env.hasPrototypeFunction(wrappedObject, functionName);

        assertThat(hasFunc).as("Has Function").isFalse();
    }

    @Test
    public void testHasPrototypeFunctionWithGuestLanguageString() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final Value strProto = env.getContext().getBindings(GraalVMScriptConstants.SCRIPT_TYPE_JS).getMember(GraalVMScriptConstants.STRING_CLASS_NAME);
        final Value guestObj = strProto.newInstance(FOO);

        final String functionName = "replace";

        final boolean hasFunc = env.hasPrototypeFunction(guestObj, functionName);

        assertThat(hasFunc).as("Has Function").isTrue();
    }

    @Test
    public void testHasPrototypeFunctionWithWrappedGuestLanguageString() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final Object guestObj = env.createNewString(FOO);

        final String functionName = "replace";

        final boolean hasFunc = env.hasPrototypeFunction(guestObj, functionName);

        assertThat(hasFunc).as("Has Function").isTrue();
    }

    @Test
    public void testSetGlobalWrappedObject() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final Value barValue = env.getContext().asValue(BAR);

        final GraalVMWrappedObject wrappedObject = new GraalVMWrappedObject(env, barValue);
        env.setGlobal(FOO, wrappedObject);

        final GraalVMWrappedObject globalObject = env.findGlobalObject(FOO);

        assertThat(globalObject).as("Global Object - %s", FOO).isNotNull();
        assertThat(globalObject.toString()).as("Global Object %s Value", FOO).isEqualTo(BAR);
    }


    @Test
    public void testSetGlobals() {

        final Map<String, Object> memberMap = createMemberMap();
        final GraalVMScriptEnvironment env = createScriptEnvironment();
        env.setGlobals(memberMap);

        for (String key : memberMap.keySet()) {
            final GraalVMWrappedObject globalObject = env.findGlobalObject(key);

            assertThat(globalObject).as("Global Object - $%s", key).isNotNull();
        }

    }

    @Test
    public void testGetTypedClass() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        final GraalVMWrappedObject object = env.getTypedClass("Map");

        assertThat(object).as("Type Class").isNotNull();
        assertThat(object).as("Typed Class class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = object.getValue();

        assertThat(value.canInstantiate()).as("Typed Class Instatiatable").isTrue();
        assertThat(value.canExecute()).as("Typed Class Executable").isTrue();
        assertThat(value.toString()).as("Typed Class Function").isEqualTo("function Map() { [native code] }");
    }

    @Test
    public void testGetTypedClassNotValid() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        assertThrows(GraalVMRuntimeException.class, () -> env.getTypedClass("java.Map)"));
    }

    @Test
    public void testGetTypedClassBlankName() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        final Object object = env.getTypedClass("");

        assertThat(object).as("Type Class").isNull();
    }

    @Test
    public void testGetTypedClassNullName() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        final Object object = env.getTypedClass(null);

        assertThat(object).as("Type Class").isNull();
    }

    @Test
    public void testCreateUndefined() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        final GraalVMWrappedObject object = env.createUndefined();

        assertThat(object).as("Undefined Object").isNotNull();
        assertThat(object).as("Undefined Object class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = object.getValue();

        assertThat(value).as("Undefined Object Value").isNotNull();

        final Object hostObject = value.asHostObject();

        assertThat(value.isNull()).as("Undefined Object Value is null").isTrue();
        assertThat(hostObject).as("Undefined Object Value Host Value").isNull();
        assertThat(value.toString()).as("Undefined Object Value To String - undefined").isEqualTo("null");
    }

    @Test
    public void testGetStringClass() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final GraalVMWrappedObject stringClass = env.getStringClass();

        assertThat(stringClass).as("String Class").isNotNull();
        assertThat(stringClass).as("String Class class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = stringClass.getValue();

        assertThat(value).as("String Class Value").isNotNull();
        assertThat(value.canExecute()).as("String Class Value can execute").isTrue();
        assertThat(value.canInstantiate()).as("String Class Value can instantiate").isTrue();
        assertThat(value.toString()).as("String Class Value to string").isEqualTo("function String() { [native code] }");
    }

    @Test
    public void testGetFunctionClass() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final GraalVMWrappedObject functionClass = env.getFunctionClass();

        assertThat(functionClass).as("Function Class").isNotNull();
        assertThat(functionClass).as("Function Class class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = functionClass.getValue();

        assertThat(value).as("Function Class Value").isNotNull();
        assertThat(value.canExecute()).as("Function Class Value can execute").isTrue();
        assertThat(value.canInstantiate()).as("Function Class Value can instantiate").isTrue();
        assertThat(value.toString()).as("Function Class Value to string").isEqualTo("function Function() { [native code] }");
    }

    @Test
    public void testGetObjectClass() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final GraalVMWrappedObject objectClass = env.getObjectClass();

        assertThat(objectClass).as("Object Class").isNotNull();
        assertThat(objectClass).as("Object Class class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = objectClass.getValue();

        assertThat(value).as("Object Class Value").isNotNull();
        assertThat(value.canExecute()).as("Object Class Value can execute").isTrue();
        assertThat(value.canInstantiate()).as("Object Class Value can instantiate").isTrue();
        assertThat(value.toString()).as(" Object Class Value to string").isEqualTo("function Object() { [native code] }");
    }

    @Test
    public void testGetArrayClass() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final GraalVMWrappedObject arrayClass = env.getArrayClass();

        assertThat(arrayClass).as("Array Class").isNotNull();
        assertThat(arrayClass).as("Array Class class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = arrayClass.getValue();

        assertThat(value).as("Array Class Value").isNotNull();
        assertThat(value.canExecute()).as("Array Class Value can execute").isTrue();
        assertThat(value.canInstantiate()).as("Array Class Value can instantiate").isTrue();
        assertThat(value.toString()).as(" Array Class Value to string").isEqualTo("function Array() { [native code] }");
    }

    @Test
    public void testCreateNewFunctionWithArgs() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final GraalVMWrappedObject functionClass = env.createNewFunction("foo", "bar", "console.log(foo + '=' + bar);");

        assertThat(functionClass).as("Function Class").isNotNull();
        assertThat(functionClass).as("Function Class class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = functionClass.getValue();

        assertThat(value).as("Function Class Value").isNotNull();
        assertThat(value.canExecute()).as("Function Class Value can execute").isTrue();
        assertThat(value.canInstantiate()).as("Function Class Value can instantiate").isTrue();
        assertThat(value.toString()).as("Function Class Value to string").isEqualTo(ANON_FUNCTION_WITH_ARGS);
    }

    @Test
    public void testCreateNewStringWithArgs() {

        final String expected = "String{[[PrimitiveValue]]: \"Foo\"}";

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final GraalVMWrappedObject functionClass = env.createNewString(FOO);

        assertThat(functionClass).as("String Class").isNotNull();
        assertThat(functionClass).as("String Class class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = functionClass.getValue();

        assertThat(value).as("String Value").isNotNull();
        assertThat(value.canExecute()).as("String Value can execute").isFalse();
        assertThat(value.canInstantiate()).as("String Value can instantiate").isFalse();
        assertThat(value.toString()).as("String Value to string").isEqualTo(expected);
    }

    @Test
    public void testCreateNewStringWithNoArgs() {

        final String expected = "String{[[PrimitiveValue]]: \"\"}";

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final GraalVMWrappedObject functionClass = env.createNewString();

        assertThat(functionClass).as("String Class").isNotNull();
        assertThat(functionClass).as("String Class class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = functionClass.getValue();

        assertThat(value).as("String Value").isNotNull();
        assertThat(value.canExecute()).as("String Value can execute").isFalse();
        assertThat(value.canInstantiate()).as("String Value can instantiate").isFalse();
        assertThat(value.toString()).as("String Value to string").isEqualTo(expected);
    }

    @Test
    public void testCreateNewObjectWithArgs() {

        final String expected = "{id=42, text=42}";

        final Map<String, Object> memberMap = new HashMap<>();
        memberMap.put("id", 42);
        memberMap.put("text", "42");

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final GraalVMWrappedObject functionClass = env.createNewObject(memberMap);

        assertThat(functionClass).as("Object Class").isNotNull();
        assertThat(functionClass).as("Object Class class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = functionClass.getValue();

        assertThat(value).as("Object Value").isNotNull();
        assertThat(value.canExecute()).as("Object Value can execute").isFalse();
        assertThat(value.canInstantiate()).as("Object Value can instantiate").isFalse();
        assertThat(value.toString()).as("Object Value to string").isEqualTo(expected);
    }

    @Test
    public void testCreateNewObjectWithNoArgs() {

        final String expected = "{}";

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final GraalVMWrappedObject functionClass = env.createNewObject();

        assertThat(functionClass).as("Object Class").isNotNull();
        assertThat(functionClass).as("Object Class class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = functionClass.getValue();

        assertThat(value).as("Object Value").isNotNull();
        assertThat(value.canExecute()).as("Object Value can execute").isFalse();
        assertThat(value.canInstantiate()).as("Object Value can instantiate").isFalse();
        assertThat(value.toString()).as("Object Value to string").isEqualTo(expected);
    }

    @SuppressWarnings("ConfusingArgumentToVarargsMethod")
    @Test
    public void testCreateNewArrayWithArgs() {

        final String expected = "(3)[1, 42, 3]";

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final GraalVMWrappedObject functionClass = env.createNewArray(DEFAULT_INT_ARRAY);

        assertThat(functionClass).as("Array Class").isNotNull();
        assertThat(functionClass).as("Array Class class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = functionClass.getValue();

        assertThat(value).as("Array Value").isNotNull();
        assertThat(value.canExecute()).as("Array Value can execute").isFalse();
        assertThat(value.canInstantiate()).as("Array Value can instantiate").isFalse();
        assertThat(value.toString()).as("Array Value to string").isEqualTo(expected);
    }

    @Test
    public void testCreateNewArrayWithNoArgs() {

        final String expected = "[]";

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final GraalVMWrappedObject functionClass = env.createNewArray();

        assertThat(functionClass).as("Object Array").isNotNull();
        assertThat(functionClass).as("Object Array class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = functionClass.getValue();

        assertThat(value).as("Array Value").isNotNull();
        assertThat(value.canExecute()).as("Array Value can execute").isFalse();
        assertThat(value.canInstantiate()).as("Array Value can instantiate").isFalse();
        assertThat(value.toString()).as("Array Value to string").isEqualTo(expected);
    }

    @Test
    public void testCreateNewFunctionWithNoArgs() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        final GraalVMWrappedObject functionClass = env.createNewFunction();

        assertThat(functionClass).as("Function Class").isNotNull();
        assertThat(functionClass).as("Function Class class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = functionClass.getValue();

        assertThat(value).as("Function Class Value").isNotNull();
        assertThat(value.canExecute()).as("Function Class Value can execute").isTrue();
        assertThat(value.canInstantiate()).as("Function Class Value can instantiate").isTrue();
        assertThat(value.toString()).as("Function Class Value to string").isEqualTo(ANON_FUNCTION_NO_ARGS);
    }


    @Test
    public void testWrapNull() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        final GraalVMWrappedObject scriptObject = env.wrap(null);

        assertThat(scriptObject).as("Script Object").isNotNull();
        assertThat(scriptObject).as("Script Object class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = scriptObject.getValue();

        assertThat(value).as("Script Object Value").isNotNull();
        assertThat(value.isNull()).as("Script Object Value").isTrue();
    }

    @Test
    public void testWrapHostObject() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        final GraalVMWrappedObject scriptObject = env.wrap(BigDecimal.ONE);

        assertThat(scriptObject).as("Script Object").isNotNull();
        assertThat(scriptObject).as("Script Object class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = scriptObject.getValue();

        assertThat(value).as("Script Object Value").isNotNull();
        assertThat(value.isHostObject()).as("Script Object Host Object").isTrue();

        final Object hostObject = value.asHostObject();

        assertThat(hostObject).as("Script Object Host Object class").isInstanceOf(BigDecimal.class);
        assertThat(hostObject).as("Script Object Host Object value").isEqualTo(BigDecimal.ONE);
    }

    @Test
    public void testWrapValue() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        final Value wrapValue = env.getContext().asValue(BigDecimal.ONE);

        final GraalVMWrappedObject scriptObject = env.wrap(wrapValue);

        assertThat(scriptObject).as("Script Object").isNotNull();
        assertThat(scriptObject).as("Script Object class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = scriptObject.getValue();

        assertThat(value).as("Script Object Value").isNotNull();
        assertThat(value.isHostObject()).as("Script Object Host Object").isTrue();

        final Object hostObject = value.asHostObject();

        assertThat(hostObject).as("Script Object Host Object class").isInstanceOf(BigDecimal.class);
        assertThat(hostObject).as("Script Object Host Object value").isEqualTo(BigDecimal.ONE);
    }

    @Test
    public void testWrapIScriptObject() {

        final GraalVMScriptEnvironment env = createScriptEnvironment();

        final Value wrapValue = env.getContext().asValue(BigDecimal.ONE);
        final GraalVMWrappedObject wrappedScriptObject = new GraalVMWrappedObject(env, wrapValue);

        final GraalVMWrappedObject scriptObject = env.wrap(wrappedScriptObject);

        assertThat(scriptObject).as("Script Object").isNotNull();
        assertThat(scriptObject).as("Script Object class").isInstanceOf(GraalVMWrappedObject.class);

        final Value value = scriptObject.getValue();

        assertThat(value).as("Script Object Value").isNotNull();
        assertThat(value.isHostObject()).as("Script Object Host Object").isTrue();

        final Object hostObject = value.asHostObject();

        assertThat(hostObject).as("Script Object Host Object class").isInstanceOf(BigDecimal.class);
        assertThat(hostObject).as("Script Object Host Object value").isEqualTo(BigDecimal.ONE);
    }

    @Test
    public void testScriptEvalulate() {

        // Success for this test is no Exception is thrown out of the method.

        final String scriptContents = """
                var key = "Foo";
                var value = "Bar";
                var result = "";
                                
                if (key == value) {
                    result = "Key and Value equal";
                } else {
                    result = "Key and Value do not equal";
                }
                """;

        final GraalVMScriptEnvironment env = createScriptEnvironment();
        env.scriptEvaluate(scriptContents);
    }

    private GraalVMScriptEnvironment createScriptEnvironment() {

        final Context context = Context.newBuilder().allowAllAccess(true).build();

        return new GraalVMJavaScriptEnvironment(context);
    }

    private Map<String, Object> createMemberMap() {

        final Map<String, Object> memberMap = new HashMap<>();
        memberMap.put("id", 42);
        memberMap.put("text", "42");
        memberMap.put("arr", DEFAULT_INT_ARRAY);

        return memberMap;
    }

    private static final String JS = "js";

    private static final String FOO = "Foo";

    private static final String BAR = "Bar";

    private static final Integer[] DEFAULT_INT_ARRAY = {1, 42, 3};

    private static final String ANON_FUNCTION_WITH_ARGS = """
            function anonymous(foo,bar
            ) {
            console.log(foo + '=' + bar);
            }""";

    private static final String ANON_FUNCTION_NO_ARGS = """
            function anonymous(
            ) {
                        
            }""";

}