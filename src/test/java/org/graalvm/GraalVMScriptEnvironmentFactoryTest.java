/*
Copyright 2022 Dreamtsoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package org.graalvm;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;

import org.graalvm.javascript.GraalVMJavaScriptEnvironment;
import org.graalvm.polyglot.Context;
import org.graalvm.utilities.GraalVMScriptConstants;
import org.junit.jupiter.api.Test;

public class GraalVMScriptEnvironmentFactoryTest {

    @Test
    public void testCreateScriptEnvironmentFromReflection() {

        final Context context = Context.newBuilder().build();

        final GraalVMScriptEnvironmentFactory factory = new GraalVMScriptEnvironmentFactory();
        final GraalVMScriptEnvironment scriptEnvironment = factory.createScriptEnvironment(GraalVMJavaScriptEnvironment.class, context);

        assertThat(scriptEnvironment).as("Script Envrionment").isNotNull();
        assertThat(scriptEnvironment).as("Script Envrionment").isInstanceOf(GraalVMJavaScriptEnvironment.class);
    }

    @Test
    public void testCreateScriptEnvironmentFromScriptType() {

        final Context context = Context.newBuilder().build();

        final GraalVMScriptEnvironmentFactory factory = new GraalVMScriptEnvironmentFactory();
        final GraalVMScriptEnvironment scriptEnvironment = factory.createScriptEnvironment(context, GraalVMScriptConstants.SCRIPT_TYPE_JS);

        assertThat(scriptEnvironment).as("Script Envrionment").isNotNull();
        assertThat(scriptEnvironment).as("Script Envrionment").isInstanceOf(GraalVMJavaScriptEnvironment.class);
    }

    @Test
    public void testCreateScriptEnvironmentFromConfigMap() {

        final GraalVMScriptEnvironmentFactory factory = new GraalVMScriptEnvironmentFactory();
        final GraalVMScriptEnvironment scriptEnvironment = factory.createScriptEnvironment(GraalVMScriptConstants.SCRIPT_TYPE_JS, new HashMap<>());

        assertThat(scriptEnvironment).as("Script Envrionment").isNotNull();
        assertThat(scriptEnvironment).as("Script Envrionment").isInstanceOf(GraalVMJavaScriptEnvironment.class);
    }

    @Test
    public void testCreateScriptEnvironmentNoLanguageSupported() {

        final GraalVMScriptEnvironmentFactory factory = new GraalVMScriptEnvironmentFactory(new HashMap<>());
        assertThrows(GraalVMRuntimeException.class, () -> factory.createScriptEnvironment(GraalVMScriptConstants.SCRIPT_TYPE_JS, new HashMap<>()));
    }

}