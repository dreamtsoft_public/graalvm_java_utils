# GraalVM Java Utils

This project was born out of a transition from JDK 9 and Nashorn to GraalVM 22.2 with JDK 17. There was a need to be
able to bounce back and forth between JavaScript and Java during a single transaction. This created a lot of complexity
when it came to making sure the Inputs into JavaScript were of a JavaScript Object type and the Outputs from JavaScript
were of a Java type.

This library is meant to be a thin layer between GraalVM and any application that uses the Guest Language support.
There are some complexities in bouncing back and forth between Java and the Guest Language. This library will
help simplify the complexities and hopefully make it easier to execute Guest Languages and translate inputs and outputs.

This library is also extensible to allow developers to customize their own needs.

# Good to know...

This library was built on GraalVM 22.2.0 using JDK 17. There are a new features that are utilized (enhanced switch
statements, as an example).

Also, the library uses Apache License 2.0.

# Usage

The way to use this library is to first create a GraalVMContextFactory using configurations. The configurations can be
hard coded, or they can come from a file. The GraalVMContextFactory is only concerned with a Map<String, Object>.
The Configuration Keys can be found in GraalVMConfigConstants.

After creating the Contex, create a GraalVMScriptEnvironment passing in the Context and type of Script Environment.
It is possible to build a script environment without settting up the script type. The only caveat is you will not be
able to set any
script [arguments](https://www.graalvm.org/sdk/javadoc/org/graalvm/polyglot/Context.Builder.html#arguments-java.lang.String-java.lang.String:A-)
.

```java
final Map<String, Object> contextConfigMap=getContextConfigurations();

// EITHER - Use the same config map for all Context's
final GraalVMContextFactory contextFactory=new GraalVMContextFactory(contextConfigMap);
final Context context=contextFactory.buildContext(GraalVMScriptConstants.SCRIPT_TYPE_JS);

final GraalVMScriptEnvironmentFactory scriptEnvironmentFactory=new GraalVMScriptEnvironmentFactory();
final GraalVMScriptEnvironment scriptEnvironment=scriptEnvironmentFactory.createScriptEnvironment(context,GraalVMScriptConstants.SCRIPT_TYPE_JS);

// OR - Use the same config map for each Context created.
final GraalVMContextFactory contextFactory=new GraalVMContextFactory(contextConfigMap);
final Context context=contextFactory.buildContext();

final GraalVMScriptEnvironmentFactory scriptEnvironmentFactory=new GraalVMScriptEnvironmentFactory();
final GraalVMScriptEnvironment scriptEnvironment=scriptEnvironmentFactory.createScriptEnvironment(context,GraalVMScriptConstants.SCRIPT_TYPE_JS);

// OR - Use a different config map for each Context based on the script environment
final GraalVMContextFactory contextFactory=new GraalVMContextFactory();
final Context context=contextFactory.buildContext(GraalVMScriptConstants.SCRIPT_TYPE_JS,contextConfigMap);

final GraalVMScriptEnvironmentFactory scriptEnvironmentFactory=new GraalVMScriptEnvironmentFactory();
final GraalVMScriptEnvironment scriptEnvironment=scriptEnvironmentFactory.createScriptEnvironment(context,GraalVMScriptConstants.SCRIPT_TYPE_JS);

// OR - Use a different config map for each Context created.
final GraalVMContextFactory contextFactory=new GraalVMContextFactory();
final Context context=contextFactory.buildContext(contextConfigMap);

final GraalVMScriptEnvironmentFactory scriptEnvironmentFactory=new GraalVMScriptEnvironmentFactory();
final GraalVMScriptEnvironment scriptEnvironment=scriptEnvironmentFactory.createScriptEnvironment(context,GraalVMScriptConstants.SCRIPT_TYPE_JS);
```

Once the script environment is created, then it is a matte of choosing how the guest language should be executed.

Use the scriptEvaluate() method to either create a guest language object to be created.
This will return a GraalVMWrappedObject which will allow the host language to either call a function,
or retrieve a member, or retrieve the host object.
When setting up the script, there are some helper methods in GraalVMUtil to either wrap the script as a function,
or set up the arguments to be passed in, etc.

```java
final String script="""
    // some script
""";

final GraalVMWrappedObject wrappedObject=scriptEnvironment.scriptEvaluate(wrappedScript);
final Object result=wrapppedObject.toObject();
```

There are a lot of utilities under `org.graalvm.utilities` to explore. These are the workhorses in this library.

# Examples

There are 2 examples under `src/it/java` that show how this library can be used.

# GraalVM Options

---

## Option Categories:

* **EXPERT:** An option only relevant in corner cases and for fine-tuning.
* **INTERNAL:** An option only relevant when debugging a language implementation or an instrument.
* **USER:** An option common for users to apply.

---

## Options

#### engine.DisableCodeSharing

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Option to force disable code sharing for this engine, even if the context was created with an explicit
  engine. This option is intended for testing purposes only.

#### engine.ForceCodeSharing

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Option to force enable code sharing for this engine, even if the context was created with a bound
  engine. This option is intended for testing purposes only.

#### engine.InstrumentExceptionsAreThrown

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      INTERNAL
* Usage:         true|false
* Help:          Propagates exceptions thrown by instruments. (default: true)

#### engine.PreinitializeContexts

* Option Key:
    * Option Type:    String
    * Option Default:
* Is Deprecated: true
* Category:      EXPERT
* Usage:         N/A
* Help:          Preinitialize language contexts for given languages.

#### engine.RelaxStaticObjectSafetyChecks

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      EXPERT
* Usage:         N/A
* Help:          On property accesses, the Static Object Model does not perform shape checks and uses unsafe casts

#### engine.SafepointALot

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Repeadly submits thread local actions and collects statistics about safepoint intervals in the process.
  Prints event and interval statistics when the context is closed for each thread. This option significantly slows down
  execution and is therefore intended for testing purposes only.

#### engine.ShowInternalStackFrames

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Show internal frames specific to the language implementation in stack traces.

#### engine.SpecializationStatistics

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Enables specialization statistics for nodes generated with Truffle DSL and prints the result on exit.
  In order for this flag to be functional -Atruffle.dsl.GenerateSpecializationStatistics=true needs to be set at build
  time. Enabling this flag and the compiler option has major implications on the performance and footprint of the
  interpreter. Do not use in production environments.

#### engine.StaticObjectStorageStrategy

* Option Key:
    * Option Type:    strategy
    * Option Default: DEFAULT
* Category:      INTERNAL
* Usage:         default|array-based|field-based
* Help:          Set the storage strategy used by the Static Object Model. Accepted values
  are: ['default', 'array-based', 'field-based']

#### engine.TraceCodeSharing

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Enables printing of code sharing related information to the logger. This option is intended to support
  debugging language implementations.

#### engine.TraceStackTraceInterval

* Option Key:
    * Option Type:    Long
    * Option Default: 0
* Category:      EXPERT
* Usage:         [1, inf)
* Help:          Prints the stack trace for all threads for a time interval. By default 0, which disables the output.

#### engine.TraceThreadLocalActions

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Traces thread local events and when they are processed on the individual threads.Prints messages with
  the [engine] [tl] prefix.

#### engine.TriggerUncaughtExceptionHandlerForCancel

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Propagates cancel execution exception into UncaughtExceptionHandler. For testing purposes only.

#### engine.UseConservativeContextReferences

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Is Deprecated: true
* Category:      INTERNAL
* Usage:         N/A
* Help:          Enables conservative context references. This allows invalid sharing between contexts. For testing
  purposes only.

#### engine.UsePreInitializedContext

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      INTERNAL
* Usage:         true|false
* Help:          Use pre-initialized context when it's available (default: true).

#### engine.WarnInterpreterOnly

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      USER
* Usage:         true|false
* Help:          Print warning when the engine is using a default Truffle runtime (default: true).

#### engine.ArgumentTypeSpeculation

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      INTERNAL
* Usage:         true|false
* Help:          Speculate on arguments types at call sites (default: true)

#### engine.BackgroundCompilation

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      EXPERT
* Usage:         true|false
* Help:          Enable asynchronous truffle compilation in background threads (default: true)

#### engine.Compilation

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      EXPERT
* Usage:         true|false
* Help:          Enable or disable Truffle compilation.

#### engine.CompilationExceptionsAreFatal

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Is Deprecated: true
* Category:      INTERNAL
* Usage:         N/A
* Help:          Treat compilation exceptions as fatal exceptions that will exit the application

#### engine.CompilationExceptionsArePrinted

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Is Deprecated: true
* Category:      INTERNAL
* Usage:         N/A
* Help:          Prints the exception stack trace for compilation exceptions

#### engine.CompilationExceptionsAreThrown

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Is Deprecated: true
* Category:      INTERNAL
* Usage:         N/A
* Help:          Treat compilation exceptions as thrown runtime exceptions

#### engine.CompilationFailureAction

* Option Key:
    * Option Type:    ExceptionAction
    * Option Default: Silent
* Category:      INTERNAL
* Usage:         Silent|Print|Throw|Diagnose|ExitVM
* Help:          Specifies the action to take when Truffle compilation fails.
    * The accepted values are:
        * Silent - Print nothing to the console.
        * Print - Print the exception to the console.
        * Throw - Throw the exception to caller.
    * Diagnose - Retry compilation with extra diagnostics enabled.
        * ExitVM - Exit the VM process.

#### engine.CompilationStatisticDetails

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Print additional more verbose Truffle compilation statistics at the end of a run.

#### engine.CompilationStatistics

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Print Truffle compilation statistics at the end of a run.

#### engine.CompileAOTOnCreate

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Compiles created call targets immediately with last tier. Disables background compilation if enabled.

#### engine.CompileImmediately

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Compile immediately to test Truffle compilation

#### engine.CompileOnly

* Option Key:
    * Option Type:    String
    * Option Default: null
* Category:      INTERNAL
* Usage:         \<name>,\<name>,...
* Help:          Restrict compilation to ','-separated list of includes (or excludes prefixed with '~'). No restriction
  by default.

#### engine.CompilerIdleDelay

* Option Key:
    * Option Type:    Long
    * Option Default: 10000
* Category:      EXPERT
* Usage:         \<ms>
* Help:          Set the time in milliseconds an idle Truffle compiler thread will wait for new tasks before
  terminating. New compiler threads will be started once new compilation tasks are submitted. Select '0' to never
  terminate the Truffle compiler thread. The option is not supported by all Truffle runtimes. On the runtime which
  doesn't support it the option has no effect. default: 10000

#### engine.CompilerThreads

* Option Key:
    * Option Type:    Integer
    * Option Default: -1
* Category:      EXPERT
* Usage:         [1, inf)
* Help:          Manually set the number of compiler threads. By default, the number of compiler threads is scaled with
  the number of available cores on the CPU.

#### engine.DynamicCompilationThresholds

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      INTERNAL
* Usage:         true|false
* Help:          Reduce or increase the compilation threshold depending on the size of the compilation queue (default:
  true).

#### engine.DynamicCompilationThresholdsMaxNormalLoad

* Option Key:
    * Option Type:    Integer
    * Option Default: 90
* Category:      INTERNAL
* Usage:         [1, inf)
* Help:          The desired maximum compilation queue load. When the load rises above this value, the compilation
  thresholds are increased. The load is scaled by the number of compiler threads.  (default: 10)

#### engine.DynamicCompilationThresholdsMinNormalLoad

* Option Key:
    * Option Type:    Integer
    * Option Default: 10
* Category:      INTERNAL
* Usage:         [1, inf)
* Help:          The desired minimum compilation queue load. When the load falls bellow this value, the compilation
  thresholds are decreased. The load is scaled by the number of compiler threads (default: 10).

#### engine.DynamicCompilationThresholdsMinScale

* Option Key:
    * Option Type:    Double
    * Option Default: 0.1
* Category:      INTERNAL
* Usage:         [0.0, inf)
* Help:          The minimal scale the compilation thresholds can be reduced to (default: 0.1).

#### engine.EncodedGraphCacheCapacity

* Option Key:
    * Option Type:    Integer
    * Option Default: 0
* Category:      EXPERT
* Usage:         [-1, inf)
* Help:          Maximum number of entries in the encoded graph cache (< 0 unbounded, 0 disabled) (default: 0).

#### engine.EncodedGraphCachePurgeDelay

* Option Key:
    * Option Type:    Integer
    * Option Default: 10000
* Category:      EXPERT
* Usage:         \<ms>
* Help:          Delay, in milliseconds, after which the encoded graph cache is dropped when the compile queue becomes
  idle.The option is only supported on the HotSpot (non-libgraal) Truffle runtime.On runtimes which doesn't support it
  the option has no effect (default: 10000).

#### engine.ExcludeAssertions

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      INTERNAL
* Usage:         true|false
* Help:          Exclude assertion code from Truffle compilations (default: true)

#### engine.FirstTierBackedgeCounts

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      EXPERT
* Usage:         true|false
* Help:          Whether to emit look-back-edge counters in the first-tier compilations. (default: true)

#### engine.FirstTierCompilationThreshold

* Option Key:
    * Option Type:    Integer
    * Option Default: 400
* Category:      EXPERT
* Usage:         [1, inf)
* Help:          Minimum number of invocations or loop iterations needed to compile a guest language root in first tier
  under normal compilation load (default: 400).

#### engine.FirstTierInliningPolicy

* Option Key:
    * Option Type:    String
    * Option Default:
* Category:      INTERNAL
* Usage:         \<policy>
* Help:          Explicitly pick a first tier inlining policy by name (None, TrivialOnly). If empty (default) the lowest
  priority policy (TrivialOnly) is chosen.

#### engine.FirstTierMinInvokeThreshold

* Option Key:
    * Option Type:    Integer
    * Option Default: 1
* Category:      EXPERT
* Usage:         [1, inf)
* Help:          Minimum number of calls before a call target is compiled in the first tier (default: 1)

#### engine.FirstTierUseEconomy

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      EXPERT
* Usage:         true|false
* Help:          Whether to use the economy configuration in the first-tier compilations. (default: true)

#### engine.ForceFrameLivenessAnalysis

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Is Deprecated: true
* Category:      EXPERT
* Usage:         N/A
* Help:          Forces the frame clearing mechanism to be executed, even if Frame.clear() is not used.

#### engine.InlineAcrossTruffleBoundary

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Enable inlining across Truffle boundary

#### engine.InlineOnly

* Option Key:
    * Option Type:    String
    * Option Default: null
* Category:      INTERNAL
* Usage:         \<name>,\<name>,...
* Help:          Restrict inlined methods to ','-separated list of includes (or excludes prefixed with '~'). No
  restriction by default.

#### engine.Inlining

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      EXPERT
* Usage:         true|false
* Help:          Enable automatic inlining of guest language call targets (default: true).

#### engine.InliningExpansionBudget

* Option Key:
    * Option Type:    Integer
    * Option Default: 12000
* Category:      EXPERT
* Usage:         [1, inf)
* Help:          The base expansion budget for language-agnostic inlining (default: 12000).

#### engine.InliningInliningBudget

* Option Key:
    * Option Type:    Integer
    * Option Default: 12000
* Category:      EXPERT
* Usage:         [1, inf)
* Help:          The base inlining budget for language-agnostic inlining (default: 12000)

#### engine.InliningPolicy

* Option Key:
    * Option Type:    String
    * Option Default:
* Category:      INTERNAL
* Usage:         \<policy>
* Help:          Explicitly pick a inlining policy by name. If empty (default) the highest priority chosen by default.

#### engine.InliningRecursionDepth

* Option Key:
    * Option Type:    Integer
    * Option Default: 2
* Category:      EXPERT
* Usage:         [0, inf)
* Help:          Maximum depth for recursive inlining (default: 2).

#### engine.InstrumentBoundaries

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Instrument Truffle boundaries and output profiling information to the standard output.

#### engine.InstrumentBoundariesPerInlineSite

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Instrument Truffle boundaries by considering different inlining sites as different branches.

#### engine.InstrumentBranches

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Instrument branches and output profiling information to the standard output.

#### engine.InstrumentBranchesPerInlineSite

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Instrument branches by considering different inlining sites as different branches.

#### engine.InstrumentFilter

* Option Key:
    * Option Type:    String
    * Option Default: *.*.*
* Category:      INTERNAL
* Usage:         \<method>,\<method>,...
* Help:          Method filter for host methods in which to add instrumentation.

#### engine.InstrumentationTableSize

* Option Key:
    * Option Type:    Integer
    * Option Default: 10000
* Category:      INTERNAL
* Usage:         [1, inf)
* Help:          Maximum number of instrumentation counters available (default: 10000).

#### engine.InvalidationReprofileCount

* Option Key:
    * Option Type:    Integer
    * Option Default: 3
* Is Deprecated: true
* Category:      EXPERT
* Usage:         N/A
* Help:          Delay compilation after an invalidation to allow for reprofiling. Deprecated: no longer has any effect.

#### engine.IterativePartialEscape

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Run the partial escape analysis iteratively in Truffle compilation.

#### engine.LastTierCompilationThreshold

* Option Key:
    * Option Type:    Integer
    * Option Default: 10000
* Category:      EXPERT
* Usage:         [1, inf)
* Help:          Minimum number of invocations or loop iterations needed to compile a guest language root in last tier
  under normal compilation load (default: 10000).

#### engine.MaximumGraalNodeCount

* Option Key:
    * Option Type:    Integer
    * Option Default: 400000
* Category:      INTERNAL
* Usage:         [1, inf)
* Help:          Stop partial evaluation when the graph exceeded this many nodes (default: 40000).

#### engine.MaximumInlineNodeCount

* Option Key:
    * Option Type:    Integer
    * Option Default: 150000
* Category:      INTERNAL
* Usage:         [1, inf)
* Help:          Ignore further truffle inlining decisions when the graph exceeded this many nodes (default: 150000).

#### engine.MethodExpansionStatistics

* Option Key:
    * Option Type:    Tier
    * Option Default: []
* Category:      INTERNAL
* Usage:         true|false|peTier|truffleTier|lowTier|\<tier>,\<tier>,...
* Help:          Print statistics on expanded Java methods during partial evaluation at the end of a run.Accepted values
  are:
    * true - Collect data for the default tier 'truffleTier'.
    * false - No data will be collected.
    * Or one or multiple tiers separated by comma (e.g. truffleTier,lowTier):
        * peTier - After partial evaluation without additional phases applied.
        * truffleTier - After partial evaluation with additional phases applied.
        * lowTier - After low tier phases were applied.

#### engine.MinInvokeThreshold

* Option Key:
    * Option Type:    Integer
    * Option Default: 3
* Category:      EXPERT
* Usage:         [1, inf)
* Help:          Minimum number of calls before a call target is compiled (default: 3).

#### engine.Mode

* Option Key:
    * Option Type:    EngineMode
    * Option Default: default
* Category:      EXPERT
* Usage:         latency|throughput
* Help:          Configures the execution mode of the engine. Available modes are 'latency' and 'throughput'. The
  default value balances between the two.

#### engine.MultiTier

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      EXPERT
* Usage:         true|false
* Help:          Whether to use multiple Truffle compilation tiers by default. (default: true)

#### engine.NodeExpansionStatistics

* Option Key:
    * Option Type:    Tier
    * Option Default: []
* Category:      INTERNAL
* Usage:         true|false|peTier|truffleTier|lowTier|\<tier>,\<tier>,...
* Help:          Print statistics on expanded Truffle nodes during partial evaluation at the end of a run.Accepted
  values are:
    * true - Collect data for the default tier 'truffleTier'.
    * false - No data will be collected.
    * Or one or multiple tiers separated by comma (e.g. truffleTier,lowTier):
        * peTier - After partial evaluation without additional phases applied.
        * truffleTier - After partial evaluation with additional phases applied.
        * lowTier - After low tier phases were applied.

#### engine.NodeSourcePositions

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Enable node source positions in truffle partial evaluations.

#### engine.OSR

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      EXPERT
* Usage:         true|false
* Help:          Enable automatic on-stack-replacement of loops (default: true).

#### engine.OSRCompilationThreshold

* Option Key:
    * Option Type:    Integer
    * Option Default: 100352
* Category:      INTERNAL
* Usage:         [1, inf)
* Help:          Number of loop iterations until on-stack-replacement compilation is triggered (default 100352).

#### engine.PartialBlockCompilation

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      EXPERT
* Usage:         true|false
* Help:          Enable partial compilation for BlockNode (default: true).

#### engine.PartialBlockCompilationSize

* Option Key:
    * Option Type:    Integer
    * Option Default: 3000
* Category:      EXPERT
* Usage:         [1, inf)
* Help:          Sets the target non-trivial Truffle node size for partial compilation of BlockNode nodes (default:
  3000).

#### engine.PartialBlockMaximumSize

* Option Key:
    * Option Type:    Integer
    * Option Default: 10000
* Category:      EXPERT
* Usage:         [1, inf)
* Help:          Sets the maximum non-trivial Truffle node size for partial compilation of BlockNode nodes (default:
  10000).

#### engine.PerformanceWarningsAreFatal

* Option Key:
    * Option Type:    PerformanceWarningKind
    * Option Default: []
* Is Deprecated: true
* Category:      INTERNAL
* Usage:         N/A
* Help:          Treat performance warnings as fatal occurrences that will exit the applications

#### engine.PrintExpansionHistogram

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Is Deprecated: true
* Category:      INTERNAL
* Usage:         N/A
* Help:          Prints a histogram of all expanded Java methods.

#### engine.PriorityQueue

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      INTERNAL
* Usage:         true|false
* Help:          Use the priority of compilation jobs in the compilation queue (default: true).

#### engine.Profiling

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      INTERNAL
* Usage:         true|false
* Help:          Enable/disable builtin profiles in com.oracle.truffle.api.profiles. (default: true)

#### engine.ReplaceReprofileCount

* Option Key:
    * Option Type:    Integer
    * Option Default: 3
* Is Deprecated: true
* Category:      EXPERT
* Usage:         N/A
* Help:          Delay compilation after a node replacement. Deprecated: no longer has any effect.

#### engine.ReturnTypeSpeculation

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      INTERNAL
* Usage:         true|false
* Help:          Speculate on return types at call sites (default: true)

#### engine.SingleTierCompilationThreshold

* Option Key:
    * Option Type:    Integer
    * Option Default: 1000
* Category:      EXPERT
* Usage:         [1, inf)
* Help:          Minimum number of invocations or loop iterations needed to compile a guest language root when not using
  multi tier (default: 1000).

#### engine.Splitting

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      EXPERT
* Usage:         true|false
* Help:          Enable automatic duplication of compilation profiles (splitting) (default: true).

#### engine.SplittingAllowForcedSplits

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      INTERNAL
* Usage:         true|false
* Help:          Should forced splits be allowed (default: true)

#### engine.SplittingDumpDecisions

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Dumps to IGV information on polymorphic events

#### engine.SplittingGrowthLimit

* Option Key:
    * Option Type:    Double
    * Option Default: 1.5
* Category:      INTERNAL
* Usage:         [0.0, inf)
* Help:          Disable call target splitting if the number of nodes created by splitting exceeds this factor times
  node count (default: 1.5).

#### engine.SplittingMaxCalleeSize

* Option Key:
    * Option Type:    Integer
    * Option Default: 100
* Category:      INTERNAL
* Usage:         [1, inf)
* Help:          Disable call target splitting if tree size exceeds this limit (default: 100)

#### engine.SplittingMaxPropagationDepth

* Option Key:
    * Option Type:    Integer
    * Option Default: 5
* Category:      INTERNAL
* Usage:         [1, inf)
* Help:          Propagate info about a polymorphic specialize through maximum this many call targets (default: 5)

#### engine.SplittingTraceEvents

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Trace details of splitting events and decisions.

#### engine.TraceAssumptions

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Print stack trace on assumption invalidation

#### engine.TraceCompilation

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      EXPERT
* Usage:         N/A
* Help:          Print information for compilation results.

#### engine.TraceCompilationAST

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Print the entire AST after each compilation

#### engine.TraceCompilationDetails

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Print information for compilation queuing.

#### engine.TraceCompilationPolymorphism

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Print all polymorphic and generic nodes after each compilation

#### engine.TraceDeoptimizeFrame

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Print stack trace when deoptimizing a frame from the stack
  with `FrameInstance#getFrame(READ_WRITE|MATERIALIZE)`.

#### engine.TraceInlining

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Print information for inlining decisions.

#### engine.TraceInliningDetails

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Print detailed information for inlining (i.e. the entire explored call tree).

#### engine.TraceMethodExpansion

* Option Key:
    * Option Type:    Tier
    * Option Default: []
* Category:      INTERNAL
* Usage:         true|false|peTier|truffleTier|lowTier|\<tier>,\<tier>,...
* Help:          Print a tree of all expanded Java methods with statistics after each compilation. Accepted values are:
    * true - Collect data for the default tier 'truffleTier'.
    * false - No data will be collected.
    * Or one or multiple tiers separated by comma (e.g. truffleTier,lowTier):
        * peTier - After partial evaluation without additional phases applied.
        * truffleTier - After partial evaluation with additional phases applied.
        * lowTier - After low tier phases were applied.

#### engine.TraceNodeExpansion

* Option Key:
    * Option Type:    Tier
    * Option Default: []
* Category:      INTERNAL
* Usage:         true|false|peTier|truffleTier|lowTier|\<tier>,\<tier>,...
* Help:          Print a tree of all expanded Truffle nodes with statistics after each compilation. Accepted values are:
    * true - Collect data for the default tier 'truffleTier'.
    * false - No data will be collected.
    * Or one or multiple tiers separated by comma (e.g. truffleTier,lowTier):
        * peTier - After partial evaluation without additional phases applied.
        * truffleTier - After partial evaluation with additional phases applied.
        * lowTier - After low tier phases were applied.

#### engine.TracePerformanceWarnings

* Option Key:
    * Option Type:    PerformanceWarningKind
    * Option Default: []
* Category:      INTERNAL
* Usage:         none|all|\<perfWarning>,\<perfWarning>,...
* Help:          Print potential performance problems, Performance warnings are: call, instanceof, store, frame_merge,
  trivial.

#### engine.TraceSplitting

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Print information for splitting decisions.

#### engine.TraceSplittingSummary

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Used for debugging the splitting implementation. Prints splitting summary directly to stdout on
  shutdown

#### engine.TraceStackTraceLimit

* Option Key:
    * Option Type:    Integer
    * Option Default: 20
* Category:      INTERNAL
* Usage:         [1, inf)
* Help:          Number of stack trace elements printed by TraceTruffleTransferToInterpreter, TraceTruffleAssumptions
  and TraceDeoptimizeFrame (default: 20).

#### engine.TraceTransferToInterpreter

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Print stack trace on transfer to interpreter.

#### engine.TraversingCompilationQueue

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      INTERNAL
* Usage:         true|false
* Help:          Use a traversing compilation queue. (default: true)

#### engine.TraversingQueueFirstTierBonus

* Option Key:
    * Option Type:    Double
    * Option Default: 15.0
* Category:      INTERNAL
* Usage:         [0.0, inf)
* Help:          Controls how much of a priority should be given to first tier compilations (default 15.0).

#### engine.TraversingQueueFirstTierPriority

* Option Key:
    * Option Type:    Boolean
    * Option Default: false
* Category:      INTERNAL
* Usage:         N/A
* Help:          Traversing queue gives first tier compilations priority.

#### engine.TraversingQueueWeightingBothTiers

* Option Key:
    * Option Type:    Boolean
    * Option Default: true
* Category:      INTERNAL
* Usage:         true|false
* Help:          Traversing queue uses rate as priority for both tier. (default: true)

#### engine.TreatPerformanceWarningsAsErrors

* Option Key:
    * Option Type:    PerformanceWarningKind
    * Option Default: []
* Category:      INTERNAL
* Usage:         none|all|\<perfWarning>,\<perfWarning>,...
* Help:          Treat performance warnings as error. Handling of the error depends on the CompilationFailureAction
  option value. Performance warnings are: call, instanceof, store, frame_merge, trivial.